# Add, Delete, Update
 + add_exam                                POST    /exams/add
 + delete_exam                             ANY     /exams/{exam_id}/delete
 + update_exam                             POST    /exams/{exam_id}/update
          
 + add_school                              POST    /schools/add
 + delete_school                           ANY     /schools/{school_id}/delete
 + update_school                           POST    /schools/{school_id}/update
          
# Listes des routes pour stats          
     
## Exams     
 + exam_stats                              GET     /exams/{exam_id}/stats
 + exam_student_stats                      GET     /exams/{exam_id}/student/{student_id}
 + exam_parts_stats                        GET     /exams/{exam_id}/parts_stats
 + exam_part                               GET     /exams/{exam_id}/{part_id}
 + exam_part_questions_old                 GET     /exams/{exam_id}/{part_id}/questions_stats_old
 + exam_part_questions_old2                GET     /exams/{exam_id}/{part_id}/questions_stats_old2
 + exam_part_questions                     GET     /exams/{exam_id}/{part_id}/questions_stats
     
## Students     
 + student_stats                           GET     /exams/student/{student_id}
 + student_stats_year                      GET     /exams/student/{student_id}/year/{year_id}
 + student_stats_subject_year              GET     /exams/student/{student_id}/subject/{subject_id}/year/{year_id}
 + student_stats_category_year             GET     /exams/student/{student_id}/exam_category/{exam_category_id}/year/{year_id}
     
## Schools     
 + school_students_exams_stats             GET     /exams/school/{school_id}/students_stats
      
 + schools_info                            GET     /schools/
 + schools_stats                           GET     /schools/stats
 + school_stats                            GET     /schools/{school_id}/stats
 + school                                  GET     /schools/{school_id}
 + school_info                             GET     /schools/{school_id}/{info}

## Classes
 + class_exams_stats_years                 GET     /exams/class/{class_id}/year/{year_id}
 + class_exams_stats_subject_years         GET     /exams/class/{class_id}/subject/{subject_id}/year/{year_id}
 + class_exams_stats_exam_category_years   GET     /exams/class/{class_id}/exam_category/{exam_category_id}/year/{year_id}
 + class_exams_stats_prof_years            GET     /exams/class/{class_id}/prof/{prof_id}/year/{year_id}


## Profs
 + prof_stats_category_year                GET     /exams/prof/{prof_id}/exam_category/{exam_category_id}/year/{year_id}
 + prof_stats_subject_year                 GET     /exams/prof/{prof_id}/subject/{subject_id}/year/{year_id}
 + prof_stats_education_year_year          GET     /exams/prof/{prof_id}/education_year/{education_year_id}/year/{year_id}
     
## Autres     
 + school_exams_stats_years                GET     /exams/school/{school_id}/year/{year_id}
 + prof_exams_stats_years                  GET     /exams/prof/{prof_id}/year/{year_id}
 + subject_exams_stats_years               GET     /exams/subject/{subject_id}/year/{year_id}
 + category_exams_stats_years              GET     /exams/category/{category_id}/year/{year_id}
 + education_year_exams_stats_years        GET     /exams/education_year/{education_year_id}/year/{year_id}
     
 + school_exams_stats                      GET     /exams/school/{school_id}
 + prof_exams_stats                        GET     /exams/prof/{prof_id}
 + subject_exams_stats                     GET     /exams/subject/{subject_id}
 + category_exams_stats                    GET     /exams/category/{category_id}
 + education_year_exams_stats              GET     /exams/education_year/{education_year_id}
 

# Stats sur les exams
### Stats d'un exam : /exams/{exam_id}/stats
 + "exam_id"
 + "exam_name"
 + "nb_students"        : Nombre d'étudiants
 + "avg"                : Moyenne de l'exam
 + "min"                : Note la plus basse
 + "max"                : Note la plus haute
 + "scale"              : Barème 
 + "nb_parts"           : Nombre de parties dans l'exam

### Stats d'un étudiant sur un examen : /exams/{exam_id}/student/{student_id}
 + "id"
 + "username"
 + "exam_grade" : Note à l'examen de l'étudiant sur 20
 + "parts"      : Liste des parties de l'examen avec la note de l'étudiant
        - "id"
        - "type_part"  : Type de la partie
        - "part_grade" : Note de la partie
        - "scale"      : Barême de la partie
        - "questions"  : Liste des questions 

### Stats des parties d'un exam : /exams/{exam_id}/parts_stats
 + "exam_id"
 + "exam_name"
 + "nb_students" : Nombre d'étudiants
 + "nb_parts"    : Nombre de parties
 + "parts"       : Listes des parties avec leur stats
        - "part_id"
        - "type_part"
        - "avg"       : Moyenne de la partie
        - "min"       : Note la plus basse de la partie
        - "max"       : Note la plus haute de la partie
        - "scale"     : Barème

### Stats des questions pour une partie : /exams/{exam_id}/{part_id}/questions_stats
 + "exam_id" 
 + "exam_name" 
 + "part_id" 
 + "type" 
 + "nb_students"  : Nombre d'étudiants
 + "nb_questions" : Nombre de questions dans la partie
 + "questions"    : Listes des questions avec leur stats
        - "question_id" 
        - "avg"         : Moyenne des notes sur la question
        - "min"         : Note la plus basse  
        - "max"         : Note la plus haute
        - "scale"       : Barème
    
### Stats des exams d'un étudiant : /exams/student/{student_id}
 + "student_id"
 + "student_username"
 + "nb_exams"         : Nombre d'examens passés
 + "min"              : Note la plus basse obtenue
 + "max"              : Note la plus haute obtenue
 + "avg"              : Moyenne des notes
 + "exams"            : Listes des examens passés avec les notes 
        - "exam_id"   
        - "grade"       : la note 
        - "coefficient" : Coefficient de l'examen


/exams/student/{student_id}/year/{year_id}
/exams/student/{student_id}/subject/{subject_id}/year/{year_id}

### Stats des examens d'un étudiant par année : /exams/student/{student_id}/year          /{year_id}
 + "student_id"
 + "student_username"
 + "years" : 
        - "year_id"      
        - "nb_exams" : Nombres d'examens passés
        - "min"      : Note la plus basse
        - "max"      : Note la plus élevée
        - "avg"      : Moyenne des notes obtenues
        - "exams"    : Listes des examens passés
                + "exam_id"     
                + "grade"       : Note de l'examen
                + "coefficient" : Coefficient de l'examen

### Stats des examens par matière par année d'un étudiant : /exams/student/{student_id}/subject/0/year          /{year_id}
 + "student_id"
 + "student_username"
 + "nb_years"         : Nombre d'années scolaire
 + "years"            : Listes des années scolaire
        - "year_id"
        - "min"         : Moyenne la plus basse
        - "max"         : Moyenne la plus haute
        - "avg"         : Moyenne générale sur l'année
        - "nb_exams"    : Nombre d'examen passés sur l'année
        - "nb_subjects" : Nombre de matières
        - "subjects"    : Liste des matières
                + "subject_id" 
                + "nb_exams"   : Nombre d'examens dans la matière
                + "min"        : Note la plus basse de la matière
                + "max"        : Note la plus haute de la matière
                + "avg"        : Moyenne des notes de la matière
                + "exams"      : Liste des examens
                        - "exam_id" 
                        - "grade"       : Note de l'examen
                        - "coefficient" : Coefficient de l'examen

Pareil pour stats des examens par catégorie d'examen d'un étudiant : /exams/student/{student_id}/exam_category/{exam_category_id}/year/{year_id}

### Stats sur les étudiants d'une école : /exams/school/{school_id}/students_stats
 + "school_id"          
 + "school_name"         
 + "nb_students" : Nombre d'étudiants de l'école 
 + "avg"         : Moyenne des moyennes des étudiants    
 + "min"         : Moyenne la plus basse
 + "max"         : Moyenne la plus haute
 + "students"    : Listes des étudiants avec leur moyenne sur tous les examens
        - "student_id"  
        - "student_username"
        - "avg_grade"        : La moyenne de l'étudiant

### Stats des examens d'une école par année : /exams/school/{school_id}/year       /{year_id}
 + "school_id"
 + "school_name"
 + "nb_years" : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "nb_exams" : Nombre d'examens
        - "avg"      : Moyenne de tous les examens
        - "min"      : Moyenne d'examen la plus basse
        - "max"      : Moyenne d'examen la plus haute
        - "exams"    : Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min"         : Note la plus basse
                + "max"         : Note la plus haute
                + "avg"         : Moyenne de l'examen
                + "coefficient" : Coefficient de l'examen
    
Pareil pour les profs, sujets, catégorie d'examen, années d'enseignement

### Stats des examens d'une école sur toutes les années : /exams/school/{school_id}
 + "school_id"
 + "school_name"
 + "nb_exams"    : Nombre d'examens total de l'école
 + "min"         : Moyenne d'examen la plus basse
 + "max"         : Moyenne d'examen la plus haute
 + "avg"         : Moyenne de tous les examens
 + "exams"       : Liste des examens
        - "exam_id"     
        - "nb_students" : Nombre d'étudiants ayant passés l'examen
        - "min"         : Note la plus basse
        - "max"         : Note la plus haute
        - "avg          : Moyenne des notes sur l'examen

Pareil pour les profs, sujets, catégorie d'examen, années d'enseignement

### Stats des examens pour une classe par année : /exams/class/{class_id}/year     /{year_id}
 + "class_id"
 + "class_name"
 + "nb_years"   : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "avg"      : Moyenne de tous les examens
        - "min"      : Note la plus basse
        - "max"      : Note la plus haute
        - "nb_exams" : Nombre d'examens
        - "exams"    : Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min" 
                + "max" 
                + "avg" 
                + "coefficient" : Coefficient de l'examen

### Stats des examens par matières pour une classe sur une année : /exams/class/{class_id}/subject/0/year      /{year_id}
 + "class_id" 
 + "nb_years" : Nombre d'années
 + "years"    : Liste des stats sur les années
        - "year_id"    
        - "min"        : Moyenne la plus basse des moyennes de matières sur l'année
        - "max"        : Moyenne la plus haute des moyennes de matières sur l'année
        - "avg"        : Moyenne des moyennes de matières sur l'année
        - "nb_exams"   : Nombre d'examens en tout
        - "nb_subject" : Nombre de catégories d'examens
        - "subject"    : 
                + "subject_id" : 8,
                + "min"        : Moyenne d'examen min 
                + "max"        : Moyenne d'examen max
                + "avg"        : Moyenne des moyennes d'examens
                + "nb_exams"   : Nombre d'examens dans la catégorie d'examen
                + "exams"      : Liste des examens
                        - "exam_id"     : 549,
                        - "avg"         : 14.52,
                        - "coefficient" : 50

Par catégorie d'examens : /exams/class/{class_id}/exam_category/{exam_category_id}/year/{year_id}
Par professeurs         : /exams/class/{class_id}/prof/{prof_id}/year/{year_id}



### Stats des examens d'un prof par catégorie d'examen et par année : /exams/prof/{prof_id}/exam_category/0/year         /{year_id}
 + "prof_id"            
 + "nb_years" : Nombre d'années
 + "years"    : Liste des stats sur les années
        - "year_id"                 
        - "min"              : Moyenne la plus basse sur les catégories d'examens
        - "max"              : Moyenne la plus haute sur les catégories d'examens
        - "avg"              : Moyenne sur les moyennes des catégories d'examens
        - "nb_exams"         : Nombre d'examens en tout
        - "nb_exam_category" : Nombre de catégories d'examens
        - "exam_category"    : 
                + "exam_category_id"
                + "min"              : Moyenne d'examen min 
                + "max"              : Moyenne d'examen max
                + "avg"              : Moyenne des moyennes d'examens
                + "nb_exams"         : Nombre d'examens dans la catégorie d'examen
                + "exams"            : Liste des examens
                        - "exam_id"      
                        - "avg"         : Moyenne de l'examen
                        - "coefficient" : Coefficient de l'examen

Par matières              : /exams/prof/{prof_id}/subject/0/year/{year_id}
Par années d'enseignement : /exams/prof/{prof_id}/education_year/0/year/{year_id}

# Stats sur les écoles
### Stats d'une école : /schools/{school_id}/stats
 + "id"                    
 + "name"                  
 + "nb_exams"           : Nombre d'examens 
 + "nb_students"        : Nombre d'étudiants 
 + "nb_profs"           : Nombre de professeurs 
 + "nb_classes"         : Nombre de classes 
 + "nb_educationYears"  : Nombre d'années scolaires
 + "nb_subjects"        : Nombre de matières


