# Journal

#### 06/06
 + Initialisation de la base de l'application
 + Ajout du bundle FOSElasticaBundle

#### 07/06
 + Génération des entités à partir de la BD (Changement de noms pour attributs de la table exams_professors)
 + Indexation pour un type 
 + 

#### 07/06
 + Génération des entités à partir de la BD (Changement de noms pour attributs de la table exams_professors)
 + Indexation pour un type 
 + 

#### 08/06
 + Modifications des fichiers entités pour la cohérence avec la BD 
 (php bin/console doctrine:schema:update --dump-sql ) 
 + Début de déclaration des index et types dans fos_elastica.yaml  
 + Indexation des données 

#### 11/06
 + Documentation sur les queries dans ES (JSON et Elastica)
 + Requetes retourne des résultats 
 + Lister les examens d'une école (avec l'id)

#### 12/06
 + Documentation sur les queries dans ES (JSON et Elastica)
 + Test de quelques requêtes
 + Liste examens par école dans un premier temps pour tester
 + histSchools Histogramme des examens par école
 + nombres d'étudiants par école
 + nombres de prof par école
 + nombres de classes par école

#### 13/06
 + Recherche pour regrouper les queries similaires en une seule
 + Documentation sur Complex datatypes -> Nested datatype
 + Indexation depuis elastica

#### 14/06
 + Création de Nested Datatype depuis Elastica 
 + Nouveau mapping test
 + fonction pour Insertion de données dans ES (school_info)
 + Documentation sur requête de nested datatype
 + Récupération de toute l'index school_info en une seule requête
 + Nouveau SchoolInfo

#### 15/06
 + Reduction du nombres de requêtes
 + Création des repository de Users et Schools
 + Modification du README (Guide d'installation)
 + Documentation sur Daemon pour faire les scripts
 + Création d'une commande symfony pour l'indexation des données 

#### 18/06
 + Documentation sur Planificateur de tâches sur Windows
 + Pour créer une tâche planifiée, dans un terminal windows : schtasks /Create /XML  "IndexationES.xml" /TN "IndexationES"
 + Route pour add document en JSON
 + Ajout d'un service qui définie le mapping

#### 19/06
 + Corriger le service mapping.infos
 + Middleware -> EventSubscriber
 + Test des données reçues (entier, clés ...)

#### 20/06
 + Suppression de document de l'index schools_info avec l'id
 + Récupération de stats en JSON
 + Création d'un ParamConverter : ElasticWeConverter
 + Ajout de nouvelles routes (pour exams, profs ...)

#### 21/06
 + Documentation sur Kibana et installation du service
 + Création du ExamsStatsController
 + Exams_info Mapping
 + Création de la commande pour indexer les exams

#### 22/06
 + Documentation sur nested object aggregation 
 + Et test de requête sur exams_info
 + Nouveau mapping pour inclure les parts et questions (avec les notes aussi)
 + Modification de la commande pour indexer les exams

#### 25/06
 + Ajout de nouveaux champs dans exams_info (classes)
 + Requêtes Elasticsearch pour récupérer les stats de parts, questions ... (nested agrégations)
 + Traduction en php des requêtes ES sur Elastica 

#### 26/06
 + Premières stats parts et questions
 + Création de fonctions pour stats des parties et questions
 + addExam et rajout de vérifications dans l'EventSubscriber

#### 27/06
 + Moyenne d'un étudiant pour une année (requêste ES et controller)
 + Réduction du nombre de requêtes (concaténation des agrégations pour stats sur questions)
 + Documentation sur Script dans requête ES (pour calculer directement la note sur 20)

#### 28/06
 + Documentation sur script dans requête ES
 + Moyenne générale d'un étudiant et ses notes (controller)
 + Stats de tous les étudiants d'une école donnée

#### 29/06
 + Update Schools_info
 + Update Exams_info
 + Réduction du nombres de requêtes pour examParts
 + Exams Stats par année scolaire d'une école

#### 02/07
 + Création d'un ESQueryController pour pouvoir envoyer nos propres requêtes
 + Stats des examens d'une année pour une école
 + Stats des examens par année d'une école
 + Stats des examens par année d'un prof
 + Stats des examens pour un sujet (par année)

#### 03/07
 + Ajout d'information dans routes précédentes
 + Exam Student Stats -> part -> question
 + Optimisation d'ancienne requête
 + Relecture du code pour optimiser (refactoring ?)
 
#### 04/07
 + Ajout de commentaires 
 + Réécriture de requêtes (ExamParts) 
 + Route pour stats des exams d'un étudiant par années

#### 05/07
 + Stats des examens d'une classe par année
 + Stats des examens d'une catégorie d’évaluation par année
 + Education Years

#### 06/07
 + Stats des examens par matière par année pour un étudiant
 + Correction de Stats par classe et ajout du champ class_id pour chaque étudiant

#### 09/07
 + Ajout de stats dans anciennes routes
 + Stats des exams par (school|prof|educationYear| ...) non groupé par année
 + Stats des examens par classe et matière
 + Correction du problèmes des max, min, grade NULL 
 + Correction de la liste des stats

#### 10/07
 + Ajout des coefficients dans les calculs de moyennes générales (par année, par sujet, par étudiant, par type)
 + Service pour récupérer : stats examens d'un étudiant groupés par (catégorie, subject) et année

#### 11/07
 + les notes sur 20 prennent la valeur 0 si le scale est nul dans les scripts -> Correction. 
 + Stats des examens d'un prof par (catégorie d'examen|sujet) et année
 + Reindexation des examens validés seulement

### 12/07
 + Maj de liste de stats
 + Même chose pour les classes (par prof, par matière, par catégorie d'examens) et par année
 + 

### A venir
 + Controller pour les autres stats (Zeplin)
 + Optimiser les requêtes
 
 + Faire les routes qui semblent pertinentes
 + Manager -> Prof -> Correcteur
 
### A corriger
 + les notes sur 20 prennent la valeur 0 si le scale est nul dans les scripts -> Corriger ça. 

### Informations importantes
 + Par professeur
 + Par matière
 + Par année d’enseignement
 + Par classe
 + Par catégorie d’évaluation
 + Par année scolaire

### Routes à venir
 + Par prof et par année d'enseignement et année ?

