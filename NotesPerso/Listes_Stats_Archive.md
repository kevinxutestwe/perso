### Stats des examens d'un prof par année : /exams/prof/{prof_id}/year/{year_id}
 + "author_id"
 + "author_name"
 + "nb_years"           : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "nb_exams"    : Nombre d'examens
        - "avg"         : Moyenne de tous les examens
        - "min"         : Note la plus basse
        - "max"         : Note la plus haute
        - "exams": Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min" 
                + "max" 
                + "avg" 

### Stats des examens pour un sujet sur une année : /exams/subject/{subject_id}/year/{year_id}
 + "subject_id"
 + "subject_name"
 + "nb_years"           : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "nb_exams"    : Nombre d'examens
        - "avg"         : Moyenne de tous les examens
        - "min"         : Note la plus basse
        - "max"         : Note la plus haute
        - "exams": Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min" 
                + "max" 
                + "avg" 

### Stats des examens pour une catégorie d'examen sur une année : /exams/category/{category_id}/year/{year_id}
 + "exam_category_id"
 + "exam_category_name"
 + "nb_years"           : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "nb_exams"    : Nombre d'examens
        - "avg"         : Moyenne de tous les examens
        - "min"         : Note la plus basse
        - "max"         : Note la plus haute
        - "exams": Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min" 
                + "max" 
                + "avg" 

### Stats des examens pour une année d'enseignement d'examen sur une année : /exams/education_year/{education_year_id}/year/{year_id}
 + "education_year_id"
 + "education_year_name"
 + "nb_years"           : Nombre d'années scolaire
 + "years": 
        - "year_id" 
        - "nb_exams"    : Nombre d'examens
        - "avg"         : Moyenne de tous les examens
        - "min"         : Note la plus basse
        - "max"         : Note la plus haute
        - "exams": Listes des examens
                + "exam_id"
                + "nb_students" : Nombre d'étudiants
                + "min" 
                + "max" 
                + "avg" 