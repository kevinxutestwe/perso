AVANT : 


{
    "class_id": 140,
    "class_name": "CEBS - INT471 - S8 2016",
    "nb_years": 1,
    "years": [
        {
            "year_id": 12,
            "nb_exams": 8,
            "exams": [
                {
                    "exam_id": 392,
                    "nb_students": 19,
                    "min": 12,
                    "max": 18.27,
                    "avg": 14.6
                },
                {
                    "exam_id": 409,
                    "nb_students": 0,
                    "min": null,
                    "max": null,
                    "avg": null
                },
                {
                    "exam_id": 411,
                    "nb_students": 19,
                    "min": 15,
                    "max": 15,
                    "avg": 15
                },
                {
                    "exam_id": 415,
                    "nb_students": 0,
                    "min": null,
                    "max": null,
                    "avg": null
                },
                {
                    "exam_id": 417,
                    "nb_students": 19,
                    "min": 9.5,
                    "max": 18.5,
                    "avg": 14.99
                },
                {
                    "exam_id": 454,
                    "nb_students": 0,
                    "min": null,
                    "max": null,
                    "avg": null
                },
                {
                    "exam_id": 455,
                    "nb_students": 0,
                    "min": null,
                    "max": null,
                    "avg": null
                },
                {
                    "exam_id": 549,
                    "nb_students": 33,
                    "min": 7.73,
                    "max": 18.18,
                    "avg": 13.51
                }
            ]
        }
    ]
}

GET exams_info/_search
{
  "size": 0,
  "query": {
    "nested": {
      "path": "classes",
      "query": {
        "bool": {
          "must": [
            {
              "match": {
                "classes.id": 140
              }
            }
          ]
        }
      }
    }
  },
  "aggs": {
    "yearAgg": {
      "terms": {
        "field": "year_id",
        "size": 10000,
        "order": {
          "_key": "asc"
        }
      },
      "aggs": {
        "examAgg": {
          "terms": {
            "field": "id",
            "size": 10000,
            "order": {
              "_key": "asc"
            }
          },
          "aggs": {
            "studentNested": {
              "nested": {
                "path": "students"
              },
              "aggs": {
                "studentClass": {
                  "terms": {
                    "field": "students.class_id",
                    "include": [
                      140
                    ]
                  },
                  "aggs": {
                    "studentAgg": {
                      "terms": {
                        "field": "students.id",
                        "size": 1
                      },
                      "aggs": {
                        "partsNested": {
                          "nested": {
                            "path": "students.parts"
                          },
                          "aggs": {
                            "scale": {
                              "sum": {
                                "field": "students.parts.scale"
                              }
                            }
                          }
                        }
                      }
                    },
                    "examStats": {
                      "stats": {
                        "field": "students.exam_grade"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

{"size":0,"query":{"nested":{"path":"classes","query":{"bool":{"must":[{"match":{"classes.id":140}}]}}}},"aggs":{"yearAgg":{"terms":{"field":"year_id","size":10000,"order":{"_key":"asc"}},"aggs":{"examAgg":{"terms":{"field":"id","size":10000,"order":{"_key":"asc"}},"aggs":{"studentNested":{"nested":{"path":"students"},"aggs":{"studentClass":{"terms":{"field":"student.class_id","include":[140]},"aggs":{"studentAgg":{"terms":{"field":"students.id","size":1},"aggs":{"partsNested":{"nested":{"path":"students.parts"},"aggs":{"scale":{"sum":{"field":"students.parts.scale"}}}}}},"examStats":{"stats":{"field":"students.exam_grade"}}}}}}}}}}}}