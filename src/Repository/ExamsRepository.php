<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Schools;
use App\Entity\ExamPartGradings;

class ExamsRepository extends EntityRepository
{
    /**
     * @param int $exam_id
     * @return array 
     */
    public function getStudentsByExam($exam_id)
    {
        $query = $this->_em->createQueryBuilder();
        $query
             ->select('s.id as student_id', 's.username as student_username', 'sum(epg.grade) as grade')
             ->from('App\Entity\ExamPartGradings', 'epg')
             ->leftJoin('epg.examPartCompletion', 'epc')
             ->leftJoin('epc.examCompletion', 'ec')
             ->leftJoin('ec.exam', 'e')
             ->leftJoin('ec.student', 's')
             ->where('ec.exam = :exam')
             ->groupBy('ec.exam, ec.student')
             ->orderBy('ec.student', 'ASC')
             ->setParameter('exam', $exam_id);
        
        return $query->getQuery()->getResult();
    }
    
    /**
     * @param int $exam_id
     * @return array 
     */
    public function getGradesByParts($exam_id, $student_id)
    {
        $query = $this->_em->createQueryBuilder();
        $query
             ->select('ep.id', 'ep.type', 'epg.grade as partGrade', 'ep.quizMaxGrade as maxGrade')
             ->from('App\Entity\ExamPartGradings', 'epg')
             ->leftJoin('epg.examPartCompletion', 'epc')
             ->leftJoin('epc.examPart', 'ep')
             ->leftJoin('epc.examCompletion', 'ec')
             ->leftJoin('ec.student', 's')
             ->leftJoin('ec.exam', 'e')
             ->where('ec.exam = :exam AND ec.student = :student')
             ->orderBy('ec.student', 'ASC')
             ->setParameter('exam', $exam_id)
             ->setParameter('student', $student_id);
        
        return $query->getQuery()->getResult();
    }
    
    /**
     * @param int $exam_id
     * @param int $student_id
     * @param int $part_id
     * @return array 
     */
    public function getGradesByQuestions($exam_id, $student_id, $part_id)
    {
        $query = $this->_em->createQueryBuilder();
        $query
             ->select('eq.id', 'eq.name', 'eqg.grade as questionGrade', 'eq.maxGrade', 'eq.otherPoints')
             ->from('App\Entity\ExamQuestionGradings', 'eqg')
             ->leftJoin('eqg.examPartGrading', 'epg')
             ->leftJoin('eqg.question', 'eq')
             ->leftJoin('epg.examPartCompletion', 'epc')
             ->leftJoin('epc.examCompletion', 'ec')
             ->where('ec.exam = :exam AND ec.student = :student AND epc.examPart=:part')
             ->orderBy('ec.student', 'ASC')
             ->setParameter('exam', $exam_id)
             ->setParameter('student', $student_id)
             ->setParameter('part', $part_id);
        
        return $query->getQuery()->getResult();
    }
    
    /**
     * @param int $exam_id
     * @return array 
     */
    public function getClassesByExam($exam_id)
    {
        $query = $this->_em->createQueryBuilder();
        $query
             ->select('sc.id', 'sc.name')
             ->from('App\Entity\Exams', 'e')
             ->leftJoin('e.schoolclass', 'sc')
             ->where('e.id=:exam')
             ->orderBy('sc.id', 'ASC')
             ->setParameter('exam', $exam_id);
        
        return $query->getQuery()->getResult();
    }
    
    /**
     * @param int $exam_category_id
     * @param int $school_subject_id
     * @return int
     */
    public function getExamCoef($exam_category_id, $school_subject_id)
    {
        $query = $this->_em->createQueryBuilder();
        $query
             ->select('ecss.coefficient')
             ->from('App\Entity\ExamCategoriesSchoolSubjects', 'ecss')
             ->leftJoin('ecss.examCategory', 'ec')
             ->leftJoin('ecss.schoolSubject', 'ss')
             ->where('ec.id=:ec AND ss.id=:ss')
             ->setParameter('ec', $exam_category_id)
             ->setParameter('ss', $school_subject_id);
             
        $res = $query->getQuery()->getResult();
        
        if (empty($res)) { 
            return 1; // TODO : A CHANGER
        }
        return $res[0]['coefficient'];
    }
}