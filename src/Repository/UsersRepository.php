<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Users;

class UsersRepository extends EntityRepository
{
    /**
     * @param int $id
     * @return Users[]
     */
    public function getProfsBySchool($id) {
        $query = $this->_em->createQuery(
                                'SELECT u.id, u.username
                                FROM App\Entity\Users u
                                WHERE u.school = :school 
                                AND u.isProfessor = TRUE'
                            )
                           ->setParameter('school', $id);    
        return $query->getResult();
    }

    /**
     * @param int $id
     * @return Users[]
     */
    public function getStudentsBySchool($id) {
        $query = $this->_em->createQuery(
                                'SELECT u.id, u.username
                                FROM App\Entity\Users u
                                WHERE u.school = :school 
                                AND u.isStudent = TRUE'
                            )
                           ->setParameter('school', $id);    
        return $query->getResult();
    }
}
