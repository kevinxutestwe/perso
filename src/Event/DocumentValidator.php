<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

class DocumentValidator extends Event
{
    /*
     * var array string
     */
    private $content;
    
    /*
     * var array string
     */
    private $mapping;

    public function __construct($content, array $mapping = []) {
        $this->content = $content;
        $this->mapping = $mapping;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function getMapping() {
        return $this->mapping;
    }

    public function setMapping($mapping) {
        $this->mapping = $mapping;
    }
}