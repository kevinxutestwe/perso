<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use App\Event\DocumentValidator;

class DocumentPreSubscriber implements EventSubscriberInterface
{
    /*
     * @var $id mixed
     * @return bool
     */
    public function is_id($id) {
        $test = intval($id);
        /*if ($test <= 0 || !is_int($test) || is_array($id)) { // Si $eventContent['id'] est un array, intval renvoie 1
            return FALSE;
        }
        return TRUE;*/
        return !($test <= 0 || !is_int($test) || is_array($id));
    }

    public function testMappingNestedDataField($mapping, $doc, &$error) {
        if (!is_array($mapping)) {
            return $doc;
        }
        foreach ($doc as $key => $elt) {
            if (!empty(array_diff_key($mapping['properties'], $elt))) {
                $error = $error.' - Malformed request syntax : wrong array keys';
                return $doc;
            }
            if (array_key_exists('id', $elt) && !$this->is_id($elt['id'])) {
                $error = $error.' - Malformed request syntax : id must be an positive integer and cannot equal to zero';
                return $doc;
            }
        }
        foreach ($mapping['properties'] as $key => $current) {
            if (array_key_exists('properties', $current)) {
                foreach ($doc as $key2 => $current2) {
                    $doc[$key2] = $this->testMappingNestedDataField($current, $current2[$key], $error);
                }
            }
        }
        return $doc;
    }

    /*
     * @var $event DocumentValidator
     */
    public function checkForAdd(DocumentValidator $event) {
        $eventContent = $event->getContent();
        $mapping = $event->getMapping();
        $error = '';

        if (!is_array($eventContent) || !empty(array_diff_key($mapping, $eventContent))) {
            $error = 'Malformed request syntax : wrong array keys';
        }
        else if (!$this->is_id($eventContent['id'])) {
            $error = 'Malformed request syntax : id must be an positive integer and cannot equal to zero';
        }
        else {
            // Test of the nested objects 
            foreach ($mapping as $key => $current) {
                if (array_key_exists('properties', $current)) {
                    if (!is_array($eventContent[$key])) {
                        $error = $error.' - Malformed request syntax : '.$key.' nested field must be an array';
                    }
                    else {
                        $eventContent[$key] = $this->testMappingNestedDataField($current, $eventContent[$key], $error);
                    }
                }
            }
        }
        if (strcmp($error, '') != 0) {
            $eventContent['error'] = $error;
            $event->setContent($eventContent);
        }
    }

    /*
     * @var $event DocumentValidator
     */
    public function checkForUpdate(DocumentValidator $event) {
        $eventContent = $event->getContent();
        $mapping = $event->getMapping();
        $error = '';

        if (!is_array($eventContent)) {
            $error = 'Malformed request syntax';
        }
        else {
            $keys = array_keys($eventContent);
            foreach ($keys as $key) {
                if (strcmp($key, 'id') == 0) {
                    $error = $error." - 'id' field must not appear.";
                    break;
                }
                if (array_key_exists($key, $mapping)) {
                    if (array_key_exists('properties', $mapping[$key])) {
                        $eventContent[$key] = $this->testMappingNestedDataField($mapping[$key], $eventContent[$key], $error);
                    }
                }
                else {
                    $error = $error." - Malformed request syntax : '$key' field not present ";
                }
            }
        }
            
        if (strcmp($error, '') != 0) {
            $eventContent['error'] = $error;
            $event->setContent($eventContent);
        }
    }

    public function checkListId($doc) {
        $eventContent = $doc->getContent();

        if (!is_array($eventContent)) {
            $eventContent['error'] = 'Malformed request syntax : request must be an array';
            $doc->setContent($eventContent);
        }
        else {
            foreach ($eventContent as $id) {
                if (!is_int($id)) {
                    $eventContent['error'] = 'Malformed request syntax : id must be an positive integer';
                    $doc->setContent($eventContent);
                    break;
                }
            }
        }
    }

    public static function getSubscribedEvents() {
        // le nom de l'event et le nom de la fonction qui sera déclenché
        return [
            'doc.pre.add' => 'checkForAdd',
            'doc.pre.update' => 'checkForUpdate',
            'doc.list.exams' => 'checkListId'
        ];
    }
}