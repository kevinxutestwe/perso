<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Exception;

use Elastica\Query;
use Elastica\Aggregation;

use App\Entity\Users;

/**
 * @Route("/profs")
 */
class ProfsStatsController extends AbstractController
{
    /**
     * @Route("/{prof_id}/exams/years/{year_id}", defaults={"year_id"=-1}, requirements={"prof_id" = "\d+", "year_id" = "\d+"}, name="prof_exams_stats_year")
     * @ParamConverter("prof", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function profExamsStatsByYear(Users $prof, $year_id) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByYear($examsInfoIndex, 'author', $prof->getId(), $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{prof_id}/exams/stats", requirements={"prof_id" = "\d+"}, name="prof_exams_stats")
     * @ParamConverter("prof", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function profExamsStats(Users $prof) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info'); 

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByField($examsInfoIndex, 'author', $prof->getId());
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{prof_id}/exams/by_field/years/{year_id}", defaults={"year_id"=-1}, requirements={"prof_id" = "\d+", "year_id" = "\d+"}, name="prof_stats_field_year")
     * @ParamConverter("prof", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function profStatsByFieldByYear(Request $request, Users $prof, $year_id) {
        /* http://localhost:8000/profs/305/exams/by_field/years?filter_exam_category=2           */
        /* http://localhost:8000/profs/305/exams/by_field/years/2?filter_subject=all             */
        /* http://localhost:8000/profs/3/exams/by_field/years?filter_education_year=all */

        // Get params
        $keys = $request->query->keys();

        if (count($keys) != 1) {
            return $this->createJsonBadResquestResponse('filter must be specified');
        }

        $params = [
            'year_id'  => $year_id,
            'field'    => substr($keys[0], 7),
            'field_id' => $request->query->get($keys[0]),
            'prof_id'  => $prof->getId()
        ];

        if (intval($params['field_id']) == 0 && strcmp($params['field_id'], 'all') != 0) {
            return $this->createJsonBadResquestResponse('field_id must be an positive integer or \'all\'');
        }

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('profs.info.es')
                         ->getProfStatsByFieldByYear($examsInfoIndex, $params);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{prof_id}/exams_to_correct", requirements={"prof_id" = "\d+"}, name="prof_exams_to_correct")
     * @ParamConverter("prof", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function profExamsToCorrect(Users $prof) {
        $prof_id = $prof->getId();
      
        $boolQuery = new Query\BoolQuery();

        $match = new Query\Match();
        $match->setField('author_id', $prof_id);
        $boolQuery->addMust($match);

        $match = new Query\Match();
        $match->setField('exam_validate', false);
        $boolQuery->addMust($match);

        $query = new Query();
        $query
             ->setSource(['id', 'name', 'planned_date'])
             ->setSize(10000)
             ->setQuery($boolQuery);

        $results = $this->container
                                  ->get('fos_elastica.index.exams_info')
                                  ->search($query)
                                  ->getResults();

        $exams = ['nb_exams' => count($results)];
        foreach ($results as $exam) {
            $exam = $exam->getHit();
            $exams['exams'][] = [
                'id' => $exam['_source']['id'],
                'name' => $exam['_source']['name'],
                'planned_date' => $exam['_source']['planned_date']
            ];
        } 

        return $this->createJsonOkResponse($exams);
    }
}