<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Exception;

use App\Entity\Users;

use Elastica\Query;
use Elastica\Aggregation;

/**
 * @Route("/students")
 */
class StudentsStatsController extends AbstractController
{
    /**
     * @Route("/{student_id}/exams/stats", requirements={"student_id" = "\d+"}, name="student_stats")
     * @ParamConverter("student", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function studentStats(Users $student) {
        $student_id = $student->getId();

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('students.info.es')
                         ->getStudentStats($examsInfoIndex, $student_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        $stats['student_username'] = $student->getUsername();
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{student_id}/exams/years/{year_id}", defaults={"year_id"=-1}, requirements={"student_id" = "\d+", "year_id" = "\d+"}, name="student_stats_year")
     * @ParamConverter("student", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function studentStatsByYear(Users $student, $year_id) {
        $student_id = $student->getId();

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('students.info.es')
                         ->getStudentStatsByYear($examsInfoIndex, $student_id, $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        $stats['student_username'] = $student->getUsername();
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{student_id}/exams/by_field/years/{year_id}", defaults={"year_id"=-1}, requirements={"student_id" = "\d+", "year_id" = "\d+"}, name="student_stats_field_year")
     * @ParamConverter("student", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function studentStatsByFieldByYear(Request $request, Users $student, $year_id) {
    /* http://localhost:8000/students/305/exams/by_field/year?filter_exam_category=2 */
    /* http://localhost:8000/students/305/exams/by_field/year/2?filter_subject=all */

        // Get params
        $keys = $request->query->keys();

        if (count($keys) != 1) {
            return $this->createJsonBadResquestResponse('filter must be specified');
        }

        $params = [
            'year_id'    => $year_id,
            'field'      => substr($keys[0], 7),
            'field_id'   => $request->query->get($keys[0]),
            'student_id' => $student->getId()
        ];

        if (intval($params['field_id']) == 0 && strcmp($params['field_id'], 'all') != 0) {
            return $this->createJsonBadResquestResponse('field_id must be an positive integer or \'all\'');
        }

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                     ->container
                     ->get('students.info.es')
                     ->getStudentStatsByFieldByYear($examsInfoIndex, $params);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{student_id}/exams/subjects/years/{year_id}", defaults={"year_id"=-1}, requirements={"student_id" = "\d+", "year_id" = "\d+"}, name="student_stats_subject_year")
     * @ParamConverter("student", class="App:Users", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function studentStatsBySubjectByYear(Request $request, Users $student, $year_id) {
        /* http://localhost:8000/students/7522/exams/subject/year?field_subject=all */

        // Get subject_id = all|int
        $keys = $request->query->keys();
        if (count($keys) != 1) {
            return $this->createJsonBadResquestResponse('filter must be specified');
        }

        $subject_id = $request->query->get($keys[0]);
        if (intval($subject_id) == 0 && strcmp($subject_id, 'all') != 0) {
            return $this->createJsonBadResquestResponse('subject_id must be an positive integer');
        }
        $student_id = $student->getId();

        // Creation of the query
        $nested = new Query\Nested();
        $nested->setPath('students');
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('students.id', $student_id);
        $boolQuery->addMust($match);
        $nested->setQuery($boolQuery);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg->setField('year_id');
        if ($year_id != -1) {
            $yearAgg
                   ->setInclude([strval($year_id)])
                   ->setSize(1);
        }
        else {
            $yearAgg
                   ->setSize(10000)
                   ->setOrder('_key', 'asc');
        }

        $subjectAgg = new Aggregation\Terms('subjectAgg');
        $subjectAgg->setField('subject_id');
        if (strcmp($subject_id, 'all') != 0) {
            $subjectAgg
               ->setInclude([strval($subject_id)])
               ->setSize(1);
        }
        else {
            $subjectAgg
               ->setSize(10000)
               ->setOrder('_key', 'asc');
        }

        $examCategoryAgg = new Aggregation\Terms('examCategoryAgg');
        $examCategoryAgg
                       ->setField('exam_category_id')
                       ->setSize(10000);

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');
        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setInclude([strval($student_id)]);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');
        $examGrade = new Aggregation\Sum('examGrade');
        $examGrade->setField('students.parts.part_grade');

        $scale = new Aggregation\Sum('maxGrade');
        $scale->setField('students.parts.scale');

        $grade20Script = new Aggregation\BucketScript('grade20');
        $grade20Script
        ->setBucketsPath([
            'max' => 'partsNested.maxGrade',
            'grade' => 'partsNested.examGrade'
        ])
        ->setScript('params.grade/params.max * 20');

        $partsNested
                   ->addAggregation($examGrade)
                   ->addAggregation($scale);
        $studentAgg
                  ->addAggregation($partsNested)
                  ->addAggregation($grade20Script);
        $studentsNested->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $examCategoryAgg->addAggregation($examAgg);

        $subjectAgg->addAggregation($examCategoryAgg);
        $yearAgg->addAggregation($subjectAgg);
        $examValidateAgg->addAggregation($yearAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->setQuery($nested)
             ->addAggregation($examValidateAgg);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.exams_info')
                       ->search($query)
                       ->getAggregations();
               
        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            return $this->createJsonNotFoundResponse('No exam validated');
        }
        if (empty($results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'])) {
            return $this->createJsonNotFoundResponse('year not found');
        }
        $results = $results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'];

        $years = [];

        $studentsInfoES = $this
                           ->container
                           ->get('students.info.es');

        foreach ($results as $year) {
            if (empty($year['subjectAgg']['buckets'])) {
                return $this->createJsonNotFoundResponse('subject not found');
            } 
            $sumAvg = 0;
            $minAvg = PHP_INT_MAX;
            $maxAvg = PHP_INT_MIN;

            // Processing of the data for one year
            $stats = [];
            foreach ($year['subjectAgg']['buckets'] as $subject) {
                $examCategories = [];
                $exams = [];
                $minGrade = PHP_INT_MAX;
                $maxGrade = PHP_INT_MIN;

                // Processing of the exam_categories for one year
                foreach ($subject['examCategoryAgg']['buckets'] as $examCategory) {
                    $sumGradeExamCategory = 0;
                    
                    // Processing of the exams for one exam category
                    foreach ($examCategory['examAgg']['buckets'] as $exam) {
                        $coef = $exam['coefAgg']['value'];
                        $grade20 = $exam['studentsNested']['studentAgg']['buckets'][0]['grade20']['value'];
                        $grade = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['examGrade']['value'];
                        $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];
    
                        $grade = $studentsInfoES->checkGrade20($grade20, $grade, $scale);   
    
                        $exams[] = [
                            'exam_id'          => $exam['key'],
                            'exam_category_id' => $examCategory['key'],
                            'coefficient'      => $coef,
                            'grade'            => $grade,
                            'scale'            => 20
                        ];
    
                        $sumGradeExamCategory += $grade; 
                        if ($grade !== NULL) {
                            $minGrade = $grade < $minGrade ? $grade : $minGrade;
                            $maxGrade = $grade > $maxGrade ? $grade : $maxGrade;
                        }
                    }

                    $examCategories[$examCategory['key']] = [
                        'nb_exams' => $examCategory['doc_count'],
                        'avg'      => $sumGradeExamCategory/$examCategory['doc_count'],
                        'coef'     => isset($coef) ? $coef : NULL 
                    ];
                }

                // Stats for one subject
                $sumSubject = 0;
                $sumCoef = 0;
                foreach ($examCategories as $current) {
                    $sumSubject += $current['avg']*$current['coef'];
                    $sumCoef += $current['coef'];
                }
                 
                $avg = $sumSubject == 0 ? NULL : round($sumSubject/$sumCoef, 2);
                if ($avg !== NULL) {
                    $sumAvg += $avg;
                    $minAvg = $avg < $minAvg ? $avg : $minAvg;
                    $maxAvg = $avg > $maxAvg ? $avg : $maxAvg;
                }

                $stats[] = [
                    'subject_id' => $subject['key'],
                    'nb_exams'   => count($exams),
                    'min'        => $minGrade == PHP_INT_MAX ? NULL : $minGrade,
                    'max'        => $maxGrade == PHP_INT_MIN ? NULL : $maxGrade,
                    'avg'        => $avg,
                    'exams'      => $exams
                ];
            }

            $nb_stats = count($stats);
            $years[] = [
                'year_id'     => $year['key'],
                'min'         => $minAvg,
                'max'         => $maxAvg,
                'avg'         => round($sumAvg/$nb_stats, 2),
                //'nb_exams'    => $['doc_count'],
                'nb_subjects' => $nb_stats,
                'subjects'    => $stats
            ];
        }

        return $this->createJsonOkResponse(
            [
                'student_id'       => $student_id,
                // 'student_username' => $student->getUsername(),
                'nb_years'         => count($years),
                'years'            => $years
            ]
        );
    }
}