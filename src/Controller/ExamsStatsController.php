<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Exception;

// For Events
use App\Event\DocumentValidator;
use Symfony\Component\EventDispatcher\EventDispatcher;

use Elastica\Query;
use Elastica\Aggregation;
use Elastica\Client;
use Elastica\Document;

use App\Entity\Exams;
use App\Entity\SchoolSubjects;
use App\Entity\SchoolExamCategories;
use App\Entity\EducationYears;

use Elastica\Exception\ResponseException;

/**
 * @Route("/exams")
 */
class ExamsStatsController extends AbstractController
{
    /**
     * @Route("/add", name="add_exam")
     * @Method({"POST"})
     */
    public function addExam(Request $request) {
        $json = $request->getContent();
        $content = json_decode($json, TRUE);

        // Middleware : Mapping checking
        $mapping = $this
                       ->container
                       ->get('mapping.infos')
                       ->getExamsInfoMapping();
        $event = new DocumentValidator($content, $mapping);
        $this
            ->container
            ->get('event_dispatcher')
            ->dispatch('doc.pre.add', $event);
        $content = $event->getContent();
        if (array_key_exists('error', $content)) {
            return $this->createJsonBadResquestResponse($content['error']);
        }

        // Creation and adding of the new document
        $document = new Document();
        $document
                ->setData($content)
                ->setId($content['id']);

        $examsInfoIndex = $this
                              ->container
                              ->get('fos_elastica.index.exams_info');
        $examsInfoType = $examsInfoIndex->getType('exams_info');
        $examsInfoType->addDocument($document);
        $response = $examsInfoIndex->refresh();

        if ($response->hasError()) {
            return $this->createJsonServerErrorResponse('Errors occurred when adding the data to Elasticsearch : '.$response->getErrorMessage());
        }
        return $this->createJsonCreatedResponse('Data have been successfully added to Elasticsearch !');
    }

    /**
     * @Route("/{exam_id}", name="delete_exam")
     * @Method({"DELETE"})
     */
    public function deleteExam($exam_id) { 
        $examsInfoType = $this
                             ->container
                             ->get('fos_elastica.index.exams_info')
                             ->getType('exams_info');

        $response = $examsInfoType->deleteById($exam_id);

        if ($response->hasError()) {
            return $this->createNotFoundException('exam does not exist in Elasticsearch.');
        }

        return $this->createJsonNoContentResponse();
    }

    /**
     * @Route("/{exam_id}", name="update_exam")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"PUT"})
     */
    public function updateExam(Request $request, Exams $exam) { 
        $json = $request->getContent();
        $content = json_decode($json, true);

        // Middleware : Mapping checking
        $mapping = $this
                       ->container
                       ->get('mapping.infos')
                       ->getExamsInfoMapping();
        $event = new DocumentValidator($content, $mapping);
        $this
            ->container
            ->get('event_dispatcher')
            ->dispatch('doc.pre.update', $event);
        $content = $event->getContent();
        if (array_key_exists('error', $content)) {
            return $this->createJsonBadResquestResponse($content['error']);
        }

        // Updating of the document
        $doc = new Document();
        $doc
           ->setData($content)
           ->setId($exam->getId());

        try {
            $response = $this
                            ->container
                            ->get('fos_elastica.index.exams_info')
                            ->getType('exams_info')
                            ->updateDocument($doc);
        }
        catch (ResponseException $exception) { 
            if (strpos($exception->getMessage(), 'document missing') !== FALSE) {
                return $this->createJsonNotFoundResponse('exam does not exist in Elasticsearch.');
            }

            throw $exception;
        }
            
        if ($response->hasError()) { 
            return $this->createJsonServerErrorResponse('Errors occurred when updating the data in Elasticsearch : '.$response->getErrorMessage());
        }
        return $this->createJsonNoContentResponse();
    }

    /**
     * @Route("/", name="exams_info")
     * @Method({"GET"})
     */
    public function getExams() {
        $maxExams = 10000;

        // Fetching the documents
        $query = new Query();
        $query
             ->setQuery(new Query\MatchAll())
             ->setSize($maxExams)
             ->setSort(['id' => 'asc']);
        $examsInfos = $this
                          ->container
                          ->get('fos_elastica.index.exams_info')
                          ->search($query)
                          ->getResults();

        // Processing of the resultss
        $results = [];
        $size = count($examsInfos);
        for ($i=0; $i<$size; $i++) {
            $results[$i] = $examsInfos[$i]->getData();
        }
        return $this->createJsonOkResponse($results);
    }

    /**
     * @Route("/{exam_id}", requirements={"exam_id" = "\d+"}, name="exam_info")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function getExam(Exams $exam) {
        //$exam = $request->attributes->get('exam');
        // Creation of the query
        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam->getId());
        $boolQuery->addMust($match);
        $query->setQuery($boolQuery);

        $examInfos = $this
                         ->container
                         ->get('fos_elastica.index.exams_info')
                         ->search($query)
                         ->getResults();

        if (empty($examInfos)) {
            throw $this->createNotFoundException('exam does not exist in Elasticsearch.');
        }      
        return $this->createJsonOkResponse($examInfos[0]->getData());
    }


    /**
     * @Route("/{exam_id}/stats", requirements={"exam_id" = "\d+"}, name="exam_stats")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examStats(Exams $exam) {
        //$exam = $request->attributes->get('exam');
        $exam_id = $exam->getId();

        // Creation of the query
        $query = new Query();
        $query->setSize(1);

        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam_id);

        $boolQuery->addMust($match);
        $query->setQuery($boolQuery);

        $examAgg = new Aggregation\Terms('exam');
        $examAgg
               ->setField('id')
               ->setInclude([strval($exam_id)]);
        
        $partNested = new Aggregation\Nested('studentsNested', 'students');

        $examStats = new Aggregation\Stats('examStats');
        $examStats->setField('students.exam_grade');

        $partNested->addAggregation($examStats);
        $examAgg->addAggregation($partNested);
        $query->addAggregation($examAgg);

        $response = $this
                        ->container
                        ->get('fos_elastica.index.exams_info')
                        ->search($query);
        $aggRes = $response->getAggregations('partStats');
        $results = $response->getResults();

        // Processing of the results
        if (empty($results)) {
            return $this->createJsonNotFoundResponse('Document not found');
        }
        $data = $results[0]->getData();
        if (empty($data['students'])) {
            return $this->createJsonNotFoundResponse('No student');   
        }
        $parts = $data['students'][0]['parts'];

        $maxGradeExam = 0;
        $partsId = [];
        foreach ($parts as $part) {
            $partsId[] = $part['id'];
            $maxGradeExam += $part['scale'];
        }

        $maxGradeExam = $maxGradeExam == 0 ? 1 : $maxGradeExam;
        $nbParts = count($parts);
        $stats = $aggRes['exam']['buckets'][0]['studentsNested']['examStats'];

        return $this->createJsonOkResponse(
            [
                'exam_id'       => $exam_id,
                'exam_name'     => $exam->getName(),
                'exam_validate' => $exam->getExamValidate(),
                'nb_students'   => $stats['count'],
                'avg'           => round($stats['avg'], 2),
                'min'           => round($stats['min'], 2),
                'max'           => round($stats['max'], 2),
                'scale'         => $maxGradeExam,
                'nb_parts'      => $nbParts,
                'parts'         => $partsId
            ]
        );
    }

    /**
     * @Route("/{exam_id}/parts/stats", requirements={"exam_id" = "\d+"}, name="exam_parts_stats")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examParts(Exams $exam) {
        //$exam = $request->attributes->get('exam');
        $exam_id = $exam->getId();

        // Creation of the query
        $query = new Query();
        $query->setSize(0);
        
        $examAgg = new Aggregation\Terms('exam');
        $examAgg
               ->setField('id')
               ->setInclude([strval($exam_id)]);

        $partNested = new Aggregation\Nested('partNested', 'students.parts');
        $partAgg = new Aggregation\Terms('partAgg');
        $partAgg
               ->setField('students.parts.id')
               ->setSize(10000);

        $partStats = new Aggregation\Stats('partStats');
        $partStats->setField('students.parts.part_grade');

        $partScale = new Aggregation\Max('partScale');
        $partScale->setField('students.parts.scale');

        $partAgg
               ->addAggregation($partStats)
               ->addAggregation($partScale);
        $partNested->addAggregation($partAgg);
        $examAgg->addAggregation($partNested);
        $query->addAggregation($examAgg);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.exams_info')
                       ->search($query)
                       ->getAggregations();

        // Processing of the results
        $parts = $results['exam']['buckets'][0]['partNested']['partAgg']['buckets'];

        $partsStats = [
            'exam_id' => $exam_id,
            'exam_name' => $exam->getName(),
            'nb_students' => isset($parts[0]) ? $parts[0]['doc_count'] : 0,
            'nb_parts' => count($parts)
        ];

        foreach ($parts as $part) {
            $partsStats['parts'][] = [
                'part_id' => $part['key'],
                'avg'     => round($part['partStats']['avg'], 2),
                'min'     => $part['partStats']['min'],
                'max'     => $part['partStats']['max'],
                'scale'   => $part['partScale']['value']
            ];
        }
        
        return $this->createJsonOkResponse($partsStats);
    }

    /**
     * @Route("/{exam_id}/parts/{part_id}", requirements={"exam_id" = "\d+", "part_id" = "\d+"}, name="exam_part")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examPart(Exams $exam, $part_id) {
        //$exam = $request->attributes->get('exam');
        $exam_id = $exam->getId();

        // Creation of the query
        $query = new Query();
        $query
             ->setSource(['students.parts'])
             ->setSize(1);

        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam_id);

        $nested = new Query\Nested();
        $nested->setPath('students.parts');
        $boolQuery2 = new Query\BoolQuery();
        $match2 = new Query\Match();
        $match2->setField('students.parts.id', $part_id);
        $boolQuery2->addMust($match2);
        $nested->setQuery($boolQuery2);

        $boolQuery
                 ->addMust($match)
                 ->addMust($nested);
        $query->setQuery($boolQuery);

        $examAgg = new Aggregation\Terms('exam');
        $examAgg
               ->setField('id')
               ->setInclude([strval($exam_id)]);
        
        $partNested = new Aggregation\Nested('partNested', 'students.parts');
        $partAgg = new Aggregation\Terms('part');
        $partAgg
               ->setField('students.parts.id')
               ->setInclude([strval($part_id)]);

        $partStats = new Aggregation\Stats('partStats');
        $partStats->setField('students.parts.part_grade');

        $partAgg->addAggregation($partStats);
        $partNested->addAggregation($partAgg);
        $examAgg->addAggregation($partNested);
        $query->addAggregation($examAgg);

        $response = $this
                        ->container
                        ->get('fos_elastica.index.exams_info')
                        ->search($query);

        // Processing of the results
        $aggRes = $response->getAggregations('partStats');
        $results = $response->getResults();

        if (empty($results)) {
            return $this->createJsonNotFoundResponse('Document not found');
        }        
        $data = $results[0]->getData();
        $part = $data['students'][0]['parts'];

        // Search of the part's index
        $i = 0;
        while ($part[$i]['id'] != $part_id) {
            $i++;
        } 
        $part = $part[$i];
        
        $stats = $aggRes['exam']['buckets'][0]['partNested']['part']['buckets'][0]['partStats'];

        return $this->createJsonOkResponse(
            [
                'exam_id' => $exam_id,
                'exam_name' => $exam->getName(),
                'part_id' => $part_id,
                'type' => $part['type_part'],
                'nb_students' => $stats['count'],
                'avg' => round($stats['avg'], 2),
                'min' => $stats['min'],
                'max' => $stats['max'],
                'scale' => $part['scale']
            ]
        );
    }
    
    /**
     * @Route("/{exam_id}/parts/{part_id}/questions/stats", requirements={"exam_id" = "\d+", "part_id" = "\d+"}, name="exam_part_questions")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examPartQuestions(Exams $exam, $part_id) {
        //$exam = $request->attributes->get('exam');
        $exam_id = $exam->getId();

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        // Creation of the query
        $query = new Query();
        $query
             ->setSource(['students.parts'])
             ->setSize(1);

        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam_id);

        $nested = new Query\Nested();
        $nested->setPath('students.parts');
        $boolQuery2 = new Query\BoolQuery();
        $match2 = new Query\Match();
        $match2->setField('students.parts.id', $part_id);
        $boolQuery2->addMust($match2);
        $nested->setQuery($boolQuery2);

        $boolQuery
                 ->addMust($match)
                 ->addMust($nested);
        $query->setQuery($boolQuery);

        $response = $examsInfoIndex->search($query);
        $results = $response->getResults();

        // Processing of the results
        if (empty($results)) {
            return $this->createJsonNotFoundResponse('Document not found');
        }        
        $data = $results[0]->getData();

        // Search of the part's index
        $i = 0;
        while ($data['students'][0]['parts'][$i]['id'] != $part_id) {
            $i++;
        } 
        $part = $data['students'][0]['parts'][$i];

        // Stats des questions 
        $questionsStats = [
            'exam_id' => $exam_id,
            'exam_name' => $exam->getName(),
            'part_id' => $part_id,
            'type' => $part['type_part'],
            'nb_students' => count($data['students']),
            'nb_questions' => count($part['questions'])
        ];

        // Creation of the query to fetch stats
        $query = new Query();
        $query->setSize(0);
        
        $examAgg = new Aggregation\Terms('exam');
        $examAgg
               ->setField('id')
               ->setInclude([strval($exam_id)]);

        $partNested = new Aggregation\Nested('partNested', 'students.parts');
        $partAgg = new Aggregation\Terms('part');
        $partAgg
               ->setField('students.parts.id')
               ->setInclude([strval($part_id)]);

        $questionsNested = new Aggregation\Nested('questionsNested', 'students.parts.questions');

        $questionAgg = new Aggregation\Terms('question');
        $questionAgg->setField('students.parts.questions.id')
                    ->setSize(10000);

        $questionStats = new Aggregation\Stats('questionStats');
        $questionStats->setField('students.parts.questions.question_grade');

        $questionAgg->addAggregation($questionStats);
        $questionsNested->addAggregation($questionAgg);
        $partAgg->addAggregation($questionsNested);
        $partNested->addAggregation($partAgg);
        $examAgg->addAggregation($partNested);
        $query->addAggregation($examAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations('questionStats');
        $results = $results['exam']['buckets'][0]['partNested']['part']['buckets'][0]['questionsNested']['question']['buckets'];

        // Processing of the results
        $i = 0;
        foreach ($part['questions'] as $question) {
            // Search of the question's index in the results
            while ($results[$i]['key'] != $question['id']) {
                $i++;
                if ($i >= $questionsStats['nb_questions']) {
                    $i = 0;
                }
            } 
            $stats = $results[$i]['questionStats'];

            $questionsStats['questions'][] = [
                'question_id' => $question['id'],
                'avg' => round($stats['avg'], 2) + $question['other_points'], // Ajouter otherPoints ??
                'min' => $stats['min'],
                'max' => $stats['max'],
                'scale' => $question['scale']
            ];
        }
        
        return $this->createJsonOkResponse($questionsStats);
    }

    /**
     * @Route("/{exam_id}/students/{student_id}", requirements={"exam_id" = "\d+", "student_id" = "\d+"}, name="exam_student_stats")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})z
     */
    public function examStudentStats(Exams $exam, $student_id) {
        $exam_id = $exam->getId();

        // Creation of the query
        $query = new Query();
        $query->setSize(1);

        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam_id);

        $nested = new Query\Nested();
        $nested->setPath('students');

        $boolQuery2 = new Query\BoolQuery();
        $match2 = new Query\Match();
        $match2->setField('students.id', $student_id);
        $boolQuery2->addMust($match2);
        $nested->setQuery($boolQuery2);

        $boolQuery
                 ->addMust($match)
                 ->addMust($nested);
        $query->setQuery($boolQuery);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.exams_info')
                       ->search($query)
                       ->getResults();

        // Processing of the results
        if (empty($results)) {
            return $this->createJsonBadResquestResponse('No student found');
        }
        $hit = $results[0]->getHit();
        $result = $hit['_source'];

        // Search of the student's index in the array
        $i = 0;
        while ($result['students'][$i]['id'] != $student_id) {
            $i++;
        }    
        $student = $result['students'][$i];

        // Calculation of the exam's scale
        $scaleExam = 0;
        foreach ($student['parts'] as $key => $part) {
            $scaleExam += $part['scale'];
            $student['parts'][$key]['nb_questions'] = count($part['questions']);
            unset($student['parts'][$key]['questions']);
        }
        $scaleExam = $scaleExam == 0 ? 1 : $scaleExam/20;

        $student['exam_grade'] = $student['exam_grade'] == NULL ? NULL : round($student['exam_grade']/$scaleExam, 2);
        $student['nb_parts'] = count($student['parts']);

        return $this->createJsonOkResponse($student);
    }

    /**
     * @Route("/{exam_id}/students/{student_id}/parts/{part_id}", requirements={"exam_id" = "\d+", "student_id" = "\d+", "part_id" = "\d+"}, name="exam_part_student_stats")
     * @ParamConverter("exam", class="App:Exams", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examPartStudentStats(Exams $exam, $student_id, $part_id) {
        $exam_id = $exam->getId();

        // Creation of the query
        $query = new Query();
        $query->setSize(1);

        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $exam_id);

        $nested = new Query\Nested();
        $nested->setPath('students');
        $boolQuery2 = new Query\BoolQuery();
        $match2 = new Query\Match();
        $match2->setField('students.id', $student_id);
        $boolQuery2->addMust($match2);
        $nested->setQuery($boolQuery2);

        $nested2 = new Query\Nested();
        $nested2->setPath('students.parts');
        $boolQuery3 = new Query\BoolQuery();
        $match3 = new Query\Match();
        $match3->setField('students.parts.id', $part_id);
        $boolQuery3->addMust($match3);
        $nested2->setQuery($boolQuery3);

        $boolQuery
                 ->addMust($match)
                 ->addMust($nested)
                 ->addMust($nested2);
        $query->setQuery($boolQuery);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.exams_info')
                       ->search($query)
                       ->getResults();

        // Processing of the results
        if (empty($results)) {
            return $this->createJsonBadResquestResponse('No student or part found');
        }
        $hit = $results[0]->getHit();
        $result = $hit['_source'];

        // Search of the student's index in the array
        $i = 0;
        while ($result['students'][$i]['id'] != $student_id) {
            $i++;
        }
        $student = $result['students'][$i];

        // Search of the part's index in they array
        $i = 0;
        while ($student['parts'][$i]['id'] != $part_id) {
            $i++;
        }
        $part = $student['parts'][$i];
        $part['nb_questions'] = count($part['questions']);

        return $this->createJsonOkResponse($part);
    }

    /**
     * @Route("/avg_exams", name="list_exams_stats")
     * @Method({"POST"})
     */
    public function listExamsStats(Request $request) {
        $json = $request->getContent();
        $content = json_decode($json, TRUE);

        // Middleware : Mapping checking
        $mapping = $this
                       ->container
                       ->get('mapping.infos')
                       ->getExamsInfoMapping();
        $event = new DocumentValidator($content, $mapping);
        $this
            ->container
            ->get('event_dispatcher')
            ->dispatch('doc.list.exams', $event);

        $examsId = $event->getContent();
        if (array_key_exists('error', $examsId)) {
            return $this->createJsonBadResquestResponse($examsId['error']);
        }

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setInclude($examsId);

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');

        $examStats = new Aggregation\Avg('examAvg');
        $examStats->setField('students.exam_grade');

        $studentAgg = new Aggregation\Terms('studentsAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setSize(1);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');
        $examScale = new Aggregation\Sum('examScale');
        $examScale->setField('students.parts.scale');

        $partsNested->addAggregation($examScale);
        $studentAgg->addAggregation($partsNested);
        $studentsNested
                      ->addAggregation($examStats)
                      ->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->addAggregation($examAgg);

        $results = $this->container
                                 ->get('fos_elastica.index.exams_info')
                                 ->search($query)
                                 ->getAggregations();
        // Processing of the results 
        if (empty($results['examAgg']['buckets'])) {
            return $this->createJsonNotFoundResponse('exam(s) not found');
        }

        $exams = [];
        $avgs = [];
        $sumAvg = 0;
        $sumCoef = 0;
        foreach ($results['examAgg']['buckets'] as $exam) {
            if (empty($exam['studentsNested']['studentsAgg']['buckets'])) {
                $scale = 1;
            }
            else {
                $scale = $exam['studentsNested']['studentsAgg']['buckets'][0]['partsNested']['examScale']['value'];
            }
            $scale = $scale == 0 ? 1 : $scale/20;

            if ($exam['studentsNested']['examAvg']['value'] === NULL) {
                $avg = NULL;
            }
            else {
                $avg = round($exam['studentsNested']['examAvg']['value']/$scale, 2);
            }
            $coef = $exam['coefAgg']['value'];

            $sumCoef += $coef;
            $sumAvg += $coef*$avg;
            $avgs[] = $avg;

            $exams[] = [
                'exam_id'     => $exam['key'],
                'coefficient' => $coef,
                'avg'         => $avg
            ];
        }

        return $this->createJsonOkResponse(
            [
                'avg' => round($sumAvg/$sumCoef, 2),
                'min' => min($avgs),
                'max' => max($avgs),
                'exams' => $exams
            ] 
        );
    }  


    /**
     * @Route("/subjects/{subject_id}/years/{year_id}", defaults={"year_id"=-1}, requirements={"subject_id" = "\d+", "year_id" = "\d+"}, name="subject_exams_stats_year")
     * @ParamConverter("subject", class="App:SchoolSubjects", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function subjectExamsStatsByYear(SchoolSubjects $subject, $year_id) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByYear($examsInfoIndex, 'subject', $subject->getId(), $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/exam_categories/{exam_category_id}/years/{year_id}", defaults={"year_id"=-1}, requirements={"exam_category_id" = "\d+", "year_id" = "\d+"}, name="category_exams_stats_year")
     * @ParamConverter("exam_category", class="App:SchoolExamCategories", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function categoryExamsStatsByYear(SchoolExamCategories $exam_category, $year_id) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByYear($examsInfoIndex, 'exam_category', $exam_category->getId(), $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/education_years/{education_year_id}/years/{year_id}", defaults={"year_id"=-1}, requirements={"education_year_id" = "\d+", "year_id" = "\d+"}, name="education_year_exams_stats_year")
     * @ParamConverter("education_year", class="App:EducationYears", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function educationYearExamsStatsByYear(EducationYears $education_year, $year_id) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByYear($examsInfoIndex, 'education_year', $education_year->getId(), $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }


    /**
     * @Route("/subjects/{subject_id}/stats", requirements={"subject_id" = "\d+"}, name="subject_exams_stats")
     * @ParamConverter("subject", class="App:SchoolSubjects", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function subjectExamsStats(SchoolSubjects $subject) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByField($examsInfoIndex, 'subject', $subject->getId());
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/exam_categories/{exam_category_id}/stats", requirements={"exam_category_id" = "\d+"}, name="category_exams_stats")
     * @ParamConverter("exam_category", class="App:SchoolExamCategories", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function examCategoryStats(SchoolExamCategories $exam_category) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByField($examsInfoIndex, 'exam_category', $exam_category->getId());
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/education_years/{education_year_id}/stats", requirements={"education_year_id" = "\d+"}, name="education_year_exams_stats")
     * @ParamConverter("education_year", class="App:EducationYears", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function educationYearExamsStats(EducationYears $education_year) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByField($examsInfoIndex, 'education_year', $education_year->getId());
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        
        return $this->createJsonOkResponse($stats);
    }
}