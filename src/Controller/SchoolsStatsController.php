<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Exception;
use Elastica\Exception\ResponseException;

// For Events
use App\Event\DocumentValidator;
use Symfony\Component\EventDispatcher\EventDispatcher;

use Elastica\Query;
use Elastica\Search;
use Elastica\Client;
use Elastica\Document;
use Elastica\Type\Mapping;

use App\Entity\Schools;

/**
 * @Route("/schools")
 */
class SchoolsStatsController extends AbstractController 
{
    /**
     * @Route("/stats_tableau", name="schools_stats_table")
     */
    public function schoolsStatsTable() {
        $maxSchools = 1000;
        // Récupération des écoles
        /*$client = new Client();
        $schoolsInfoIndex = $client->getIndex('schools_info');
        $schoolsInfoIndex = $this->container->get('fos_elastica.index.schools_info');*/

        $query = new Query();
        $query
             ->setQuery(new Query\MatchAll())
             ->setSize($maxSchools)
             ->setSort(['id' => 'asc']);
        $schoolInfos = $this
                           ->container
                           ->get('fos_elastica.index.schools_info')
                           ->search($query)
                           ->getResults();
        $results = [];
        $size = count($schoolInfos);
        for ($i=0; $i<$size; $i++) {
            $hit = $schoolInfos[$i]->getHit();
            $source = $hit['_source'];

            $results[$i] = [
                'id' => $source['id'],
                'name' => $source['name'],
                'nb_exams' => count($source['exams']),
                'nb_students' => count($source['students']),
                'nb_profs' => count($source['profs']),
                'nb_classes' => count($source['classes']),
                'nb_educationYears' => count($source['education_years']),
                'nb_subjects' => count($source['subjects'])
            ];
        }
        
        return $this->render('schoolsStats/schoolsStats.html.twig', [
            'results' => $results,
            'nbResults' => count($results),
        ]);
    }

    /**
     * @Route("/add", name="add_school")
     * @Method({"POST"})
     */
    public function addSchool(Request $request) {
        $json = $request->getContent();
        $content = json_decode($json, TRUE);

        // Middleware : Vérification du mapping
        $mapping = $this
                       ->container
                       ->get('mapping.infos')
                       ->getSchoolsInfoMapping();
        $event = new DocumentValidator($content, $mapping);
        $this
            ->container
            ->get('event_dispatcher')
            ->dispatch('doc.pre.add', $event);
        $content = $event->getContent();
        if (array_key_exists('error', $content)) {
            return $this->createJsonBadResquestResponse($content['error']);
        }

        // Creation and adding of the new document
        $document = new Document();
        $document
                ->setData($content)
                ->setId($content['id']);
        
        $schoolsInfoIndex = $this
                                ->container
                                ->get('fos_elastica.index.schools_info');
        $schoolsInfoType = $schoolsInfoIndex->getType('schools_info');
        $schoolsInfoType->addDocument($document);
        $response = $schoolsInfoIndex->refresh();

        if ($response->hasError()) {
            return $this->createJsonServerErrorResponse('Errors occurred when adding the data to Elasticsearch : '.$response->getErrorMessage());
        }
        return $this->createJsonCreatedResponse('Data have been successfully added to Elasticsearch !');
    }

    /**
     * @Route("/{school_id}", name="delete_school")
     * @Method({"DELETE"})
     */
    public function deleteSchool($school_id) { 
        //$school = $request->attributes->get('school');
        $schoolsInfoType = $this
                               ->container
                               ->get('fos_elastica.index.schools_info')
                               ->getType('schools_info');

        $response = $schoolsInfoType->deleteById($school_id);

        if ($response->hasError()) {
            return $this->createNotFoundException('school does not exist in Elasticsearch.');
        }
        return $this->createJsonNoContentResponse();
    }

    /**
     * @Route("/{school_id}", name="update_school")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"PUT"})
     */
    public function updateSchool(Request $request, Schools $school) { 
        $json = $request->getContent();
        $content = json_decode($json, true);

        // Middleware : Vérification du mapping
        $mapping = $this
                       ->container
                       ->get('mapping.infos')
                       ->getSchoolsInfoMapping();
        $event = new DocumentValidator($content, $mapping);
        $this
            ->container
            ->get('event_dispatcher')
            ->dispatch('doc.pre.update', $event);
        $content = $event->getContent();
        if (array_key_exists('error', $content)) {
            return $this->createJsonBadResquestResponse($content['error']);
        }

       // Updating of the document
        $doc = new Document();
        $doc
           ->setData($content)
           ->setId($school->getId());

        try {
            $response = $this
                            ->container
                            ->get('fos_elastica.index.schools_info')
                            ->getType('schools_info')
                            ->updateDocument($doc);
        }
        catch (ResponseException $exception){
            if (strpos($exception->getMessage(), 'document missing') !== FALSE) {
                return $this->createJsonNotFoundResponse('school does not exist in Elasticsearch.');
            }

            throw $exception;
        }

        if ($response->hasError()) { 
            return $this->createJsonServerErrorResponse('Errors occurred when updating the data in Elasticsearch : '.$response->getErrorMessage());
        }
        return $this->createJsonNoContentResponse();
    }

    /**
     * @Route("/", name="schools_info")
     * @Method({"GET"})
     */
    public function schools() {
        $maxSchools = 1000;

        // Récupération des écoles
        $query = new Query();
        $query
             ->setQuery(new Query\MatchAll())
             ->setSize($maxSchools)
             ->setSort(['id' => 'asc']);
        $schoolInfos = $this
                           ->container
                           ->get('fos_elastica.index.schools_info')
                           ->search($query)
                           ->getResults();
        $results = [];
        $size = count($schoolInfos);
        for ($i=0; $i<$size; $i++) {
            $hit = $schoolInfos[$i]->getHit();
            $results[$i] = $hit['_source'];
        }
        
        return $this->createJsonOkResponse($results);
    }

    /**
     * @Route("/{school_id}", requirements={"school_id" = "\d+"}, name="school")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function school(Schools $school) {
        //$school = $request->attributes->get('school');
        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $school->getId());
        $boolQuery->addMust($match);
        $query->setQuery($boolQuery);
        $schoolInfos = $this
                           ->container
                           ->get('fos_elastica.index.schools_info')
                           ->search($query)
                           ->getResults();

        if (empty($schoolInfos)) {
            throw $this->createNotFoundException('school does not exist in Elasticsearch.');
        }
        return $this->createJsonOkResponse($schoolInfos[0]->getData());
    }

    /**
     * @Route("/stats", name="schools_stats")
     * @Method({"GET"})
     */
    public function schoolsStats() {
        $maxSchools = 1000;

        // Récupération des écoles
        $query = new Query();
        $query
             ->setQuery(new Query\MatchAll())
             ->setSize($maxSchools)
             ->setSort(['id' => 'asc']);
        $results = $this
                       ->container
                       ->get('fos_elastica.index.schools_info')
                       ->search($query)
                       ->getResults();
        $stats = [];
        $size = count($results);
        for ($i=0; $i<$size; $i++) {
            $schoolInfo = $results[$i]->getData();

            $stats[$i] = [
                'id' => $schoolInfo['id'],
                'name' => $schoolInfo['name'],
                'nb_exams' => count($schoolInfo['exams']),
                'nb_students' => count($schoolInfo['students']),
                'nb_profs' => count($schoolInfo['profs']),
                'nb_classes' => count($schoolInfo['classes']),
                'nb_educationYears' => count($schoolInfo['education_years']),
                'nb_subjects' => count($schoolInfo['subjects'])
            ];
        }
        
        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{school_id}/stats", requirements={"school_id" = "\d+"}, name="school_stats")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function schoolStats(Schools $school) {
        //$school = $request->attributes->get('school');
        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $school->getId());
        $boolQuery->addMust($match);
        $query->setQuery($boolQuery);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.schools_info')
                       ->search($query)
                       ->getResults();
        if (empty($results)) {
            return $this->createJsonNotFoundResponse('errors occurred');
        }

        $stats = $results[0]->getData();
        return $this->createJsonOkResponse(
            [
                'id' => $stats['id'],
                'name' => $stats['name'],
                'nb_exams' => count($stats['exams']),
                'nb_students' => count($stats['students']),
                'nb_profs' => count($stats['profs']),
                'nb_classes' => count($stats['classes']),
                'nb_educationYears' => count($stats['education_years']),
                'nb_subjects' => count($stats['subjects'])
            ]
        );
    }

    /**
     * @Route("/{school_id}/{info}", requirements={"school_id" = "\d+", "info" = "exams|profs|students|classes|education_years|subjects"}, name="school_info")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function schoolInfo(Schools $school, $info) {

        $schoolsInfoIndex = $this->container->get('fos_elastica.index.schools_info');
        
        return $this->createJsonOkResponse(    
            $this
                ->container
                ->get('schools.info.es')
                ->getSchoolsInfo($schoolsInfoIndex, $school->getId(), $info)
        );
    }

    /**
     * @Route("/{school_id}/students/stats", requirements={"school_id" = "\d+"}, name="school_students_exams_stats")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function schoolStudentsExamsStats(Schools $school) {
        $school_id = $school->getId();

        $schoolsInfoIndex = $this->container->get('fos_elastica.index.schools_info');
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        // Fetching of the school's students
        $students = $this
                        ->container
                        ->get('schools.info.es')
                        ->getSchoolsInfo($schoolsInfoIndex, $school_id, 'students');
        // Fetching student's stats
        $sum = 0;
        $min = PHP_INT_MAX;
        $max = PHP_INT_MIN;
        $studentsStats = [];
        $studentSkipped = 0;
        foreach ($students as $student) {
            try {
                $stats = $this
                             ->container
                             ->get('exams.info.es')
                             ->getStudentStats($examsInfoIndex, $student['id']);
            }
            catch (Exception $e) {
                $studentSkipped++;
                continue;
            }
            
            if (array_key_exists('error', $stats)) {
                $stats['avg'] = NULL;
            } 
            
            $studentsStats[] = [
                'student_id' => $student['id'],
                'student_username' => $student['username'],
                'avg_grade' => $stats['avg']
            ];

            $sum += $stats['avg']; 
            if ($stats['avg'] !== NULL) {
                $min = $stats['avg'] < $min ? $stats['avg'] : $min;
                $max = $stats['avg'] > $max ? $stats['avg'] : $max;
            }
        }

        $nb_students = count($students);
        $schoolExamsStudentsStats = [
            'school_id'                          => $school_id,
            'school_name'                        => $school->getName(),
            'nb_students'                        => $nb_students,
            'nb_students_with_no_exam_validated' => $studentSkipped,
            'avg'                                => round($sum/$nb_students, 2),
            'min'                                => $min == PHP_INT_MAX ? NULL : $min,
            'max'                                => $max == PHP_INT_MIN ? NULL : $max,
            'students'                           => $studentsStats
        ];
        return $this->createJsonOkResponse($schoolExamsStudentsStats);
    }

    /**
     * @Route("/{school_id}/exams/years/{year_id}", defaults={"year_id"=-1}, requirements={"school_id" = "\d+", "year_id" = "\d+"}, name="school_exams_stats_year")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function schoolExamsStatsByYear(Schools $school, $year_id) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                     ->container
                     ->get('exams.info.es')
                     ->getExamsByYear($examsInfoIndex, 'school', $school->getId(), $year_id);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        return $this->createJsonOkResponse($stats);
    }

    /**
     * @Route("/{school_id}/exams/stats", requirements={"school_id" = "\d+"}, name="school_exams_stats")
     * @ParamConverter("school", class="App:Schools", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function schoolExamsStats(Schools $school) {
        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('exams.info.es')
                         ->getExamsByField($examsInfoIndex, 'school', $school->getId());
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }
        return $this->createJsonOkResponse($stats);
    }
}

