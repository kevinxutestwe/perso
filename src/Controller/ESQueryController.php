<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Elastica\Client;
use Elastica\Document;
use Elastica\Request as ESRequest;

/**
 * @Route("/query")
 */
class ESQueryController extends AbstractController
{
	/**
     * @Route("/{type_doc}", requirements={"type_doc" = "schools_info|exams_info"}, name="es_query")
     * @Method({"POST"})
     */
	public function ESQuery(Request $request, $type_doc) {
		$client = new Client();
		$index = $client->getIndex($type_doc);
		$type = $index->getType($type_doc);
		$query = $request->getContent();

		$path = $index->getName().'/'.$type->getName().'/_search';
		return $this->createJsonOkResponse(
			$client
			      ->request($path, ESRequest::GET, $query)
			      ->getData()
		);
	}
}