<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Exception;

use Elastica\Query;
use Elastica\Aggregation;
use Elastica\Client;

use App\Entity\SchoolClasses;

/**
 * @Route("/classes")
 */
class ClassesStatsController extends AbstractController
{
    /**
     * @Route("/{class_id}/exams/years/{year_id}", defaults={"year_id"=-1}, requirements={"class_id" = "\d+", "year_id" = "\d+"}, name="class_exams_stats_year")
     * @ParamConverter("class", class="App:SchoolClasses", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function classExamsStatsByYear(SchoolClasses $class, $year_id) {
        $class_id = $class->getId();
        // Creation of the query
        $nested = new Query\Nested();
        $nested->setPath('classes');
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('classes.id', $class_id);
        $boolQuery->addMust($match);
        $nested->setQuery($boolQuery);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg->setField('year_id');

        if ($year_id != -1) {
            $yearAgg
                   ->setInclude([strval($year_id)])
                   ->setSize(1);
        }
        else {
            $yearAgg
                   ->setSize(10000)
                   ->setOrder('_key', 'asc');
        }

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');
        
        $studentsNested = new Aggregation\Nested('studentsNested', 'students');
        
        $studentClass = new Aggregation\Terms('studentClass');
        $studentClass
                    ->setField('students.class_id')
                    ->setInclude([strval($class_id)]);

        $examStats = new Aggregation\Stats('examStats');
        $examStats->setField('students.exam_grade');
        
        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
             ->setField('students.id')
             ->setSize(1);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');

        $scale = new Aggregation\Sum('scale');
        $scale->setField('students.parts.scale');

        $partsNested->addAggregation($scale);
        $studentAgg->addAggregation($partsNested);
        $studentClass
                     ->addAggregation($studentAgg)
                     ->addAggregation($examStats);
        $studentsNested->addAggregation($studentClass);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $yearAgg->addAggregation($examAgg);
        $examValidateAgg->addAggregation($yearAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->setQuery($nested)
             ->addAggregation($examValidateAgg);

        $results = $this
                       ->container
                       ->get('fos_elastica.index.exams_info')
                       ->search($query)
                       ->getAggregations();
        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            return $this->createJsonNotFoundResponse('No exam validated');
        }
        $years = [];
        // foreach loop on years
        foreach ($results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'] as $result) {
            $exams = [];
            $sumCoef = 0;
            $sumYear = 0;
            $minYear = PHP_INT_MAX;
            $maxYear = PHP_INT_MIN;
           
            // foreach loop on exams
            foreach ($result['examAgg']['buckets'] as $exam) {
                if (empty($exam['studentsNested']['studentClass']['buckets'])) {
                    $stats = [
                        'min'   => NULL,
                        'max'   => NULL,
                        'avg'   => NULL,
                        'count' => 0
                    ];
                    $scale = 0;
                }
                else {
                    $stats = $exam['studentsNested']['studentClass']['buckets'][0]['examStats'];
                
                    if (empty($exam['studentsNested']['studentClass']['buckets'][0]['studentAgg']['buckets'])) {
                        $scale = 0;
                    }
                    else {
                        $scale = $exam['studentsNested']['studentClass']['buckets'][0]['studentAgg']['buckets'][0]['partsNested']['scale']['value'];
                    }
                }
                $coef = $exam['coefAgg']['value'];
                $scale = $scale == 0 ? 1 : $scale/20;
                $min = $stats['min'] === NULL ? NULL : round($stats['min']/$scale, 2);
                $max = $stats['max'] === NULL ? NULL : round($stats['max']/$scale, 2);
                $avg = $stats['avg'] === NULL ? NULL : round($stats['avg']/$scale, 2);

                $sumCoef += $coef;
                $sumYear += $avg*$coef; 
                if ($avg !== NULL) {
                    $minYear = $avg < $minYear ? $avg : $minYear;
                    $maxYear = $avg > $maxYear ? $avg : $maxYear;
                }

                $exams[] = [
                    'exam_id'     => $exam['key'],
                    'coefficient' => $coef,
                    'nb_students' => $stats['count'],
                    'min'         => $min, 
                    'max'         => $max,
                    'avg'         => $avg,
                ];
            }

            $years[] = [
                'year_id'  => $result['key'],
                'min'      => $minYear == PHP_INT_MAX ? NULL : $minYear, 
                'max'      => $maxYear == PHP_INT_MIN ? NULL : $maxYear,
                'avg'      => $sumYear == 0 ? NULL : round($sumYear/$sumCoef, 2),
                'nb_exams' => $result['doc_count'],
                'exams'    => $exams
            ];
        }

        return $this->createJsonOkResponse(
            [   
                'class_id'   => $class_id,
                'class_name' => $class->getName(),
                'nb_years'   => count($years),
                'years'      => $years
            ]
        );
    }

    /**
     * @Route("/{class_id}/exams/by_field/years/{year_id}", defaults={"year_id"=-1}, requirements={"class_id" = "\d+", "year_id" = "\d+"}, name="class_stats_field_year")
     * @ParamConverter("class", class="App:SchoolClasses", converter="ElasticWe")
     * @Method({"GET"})
     */
    public function classStatsByFieldByYear(Request $request, SchoolClasses $class, $year_id) {
        /* http://localhost:8000/classes/305/exams/by_field/year?filter_exam_category=2 */
        /* http://localhost:8000/classes/305/exams/by_field/year/2?filter_subject=all */
        /* http://localhost:8000/classes/140/exams/by_field/year?filter_author=all */
       
        // Get params
        $keys = $request->query->keys();

        if (count($keys) != 1) {
            return $this->createJsonBadResquestResponse('filter must be specified');
        }

        $params = [
            'year_id'  => $year_id,
            'field'    => substr($keys[0], 7),
            'field_id' => $request->query->get($keys[0]),
            'class_id' => $class->getId()
        ];

        if (intval($params['field_id']) == 0 && strcmp($params['field_id'], 'all') != 0) {
            return $this->createJsonBadResquestResponse('field_id must be an positive integer or \'all\'');
        }

        $examsInfoIndex = $this->container->get('fos_elastica.index.exams_info');

        try {
            $stats = $this
                         ->container
                         ->get('classes.info.es')
                         ->getClassStatsByFieldByYear($examsInfoIndex, $params);
        }
        catch (Exception $e) {
            return $this->createJsonNotFoundResponse($e->getMessage());
        }

        return $this->createJsonOkResponse($stats);
    }
}