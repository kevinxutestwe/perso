<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController extends Controller
{
	public function createJsonOkResponse($data) {
		return new JsonResponse(
			$data, 
			JsonResponse::HTTP_OK);
	}

	public function createJsonCreatedResponse($msg) {
		return new JsonResponse(
			[
				'info'   => $msg,
				'status' => JsonResponse::HTTP_CREATED
			], 
			JsonResponse::HTTP_CREATED);
	}

	public function createJsonNoContentResponse() {
		return new JsonResponse(
			[
				'info'   => '',
				'status' => JsonResponse::HTTP_NO_CONTENT
			], 
			JsonResponse::HTTP_NO_CONTENT);
	}

	public function createJsonBadResquestResponse($msg) {
		return new JsonResponse(
			[
				'error'  => $msg,
				'status' => JsonResponse::HTTP_BAD_REQUEST
			], 
			JsonResponse::HTTP_BAD_REQUEST);
	}

	public function createJsonServerErrorResponse($msg) {
		return new JsonResponse(
			[
				'error'  => $msg,
				'status' => JsonResponse::HTTP_INTERNAL_SERVER_ERROR
			], 
			JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
	}

	public function createJsonNotFoundResponse($msg) {
		return new JsonResponse(
			[
				'error'  => $msg,
				'status' => JsonResponse::HTTP_NOT_FOUND
			], 
			JsonResponse::HTTP_NOT_FOUND);
	}
}