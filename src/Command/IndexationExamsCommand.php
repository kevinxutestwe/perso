<?php

namespace App\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Elastica\Client;
use Elastica\Document;
use Elastica\Type\Mapping;

use App\Entity\Exams;
use App\Entity\Users;

class IndexationExamsCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('indexation:exams_info')
            ->setDescription('Indexation des exams sur Elasticsearch');
    }

    protected function getStudentClass($schoolClasses, $classesDoc) {
        foreach ($schoolClasses as $sc) {
            $id = $sc->getId();
            if (in_array(['id' => $sc->getId(), 'name' => $sc->getName()], $classesDoc)) {
                return $id;          
            }
        }
        return null;
    }


    protected function execute(InputInterface $input, OutputInterface $output) {
        // Creation of index and mapping        
        $client = new Client();
        $examsInfoIndex = $client->getIndex('exams_info');
        $examsInfoIndex->create(
            array(
                'number_of_shards' => 5,
                'number_of_replicas' => 1
            ),
            true
        );
        $examsInfoType = $examsInfoIndex->getType('exams_info');
        $mapping = new Mapping();
        $properties = $this
                          ->getContainer()
                          ->get('mapping.infos')
                          ->getExamsInfoMapping();
        $mapping
               ->setType($examsInfoType)
               ->setProperties($properties);
        $mapping->send();
        $index = $examsInfoType->getIndex();

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $examsRepository = $em->getRepository(Exams::class);
        $usersRepository = $em->getRepository(Users::class);

        $exams = $examsRepository->findAll();

        $examsSize = count($exams); // à enlever
        $i = 0;

        foreach ($exams as $exam) {
            $i++;

            if (!$exam->getIsValidated()) {
                continue;
            }
            $output->writeln($i."/".$examsSize); /// à enlever
            
            $exam_id = $exam->getId();

            // Retrieving all students and grades of the current exam
            $students = $examsRepository->getStudentsByExam($exam_id);

            // Retrieving all classes of the current exam
            $classes = $examsRepository->getClassesByExam($exam_id);
            $classesDoc = [];
            foreach ($classes as $class) {
                if ($class['id'] !== null) {
                    $classesDoc[] = [
                        'id' => $class['id'],
                        'name' => $class['name']
                    ];
                }
            }

            $studentsDoc = [];
            foreach ($students as $student) {
                // Retrieving current student's parts and grades
                $parts = $examsRepository->getGradesByParts($exam_id, $student['student_id']);
                $partsDoc = [];
                foreach ($parts as $part) {
                    // Retrieving current part's questions 
                    $questions = $examsRepository->getGradesByQuestions($exam_id, $student['student_id'], $part['id']);
                    $questionsDoc = [];
                    $partScale = 0;
                    foreach ($questions as $question) {
                        $partScale += $question['maxGrade'];
                        $questionsDoc[] = [
                            'id' => $question['id'],
                            'name' => $question['name'],
                            'question_grade' => floatval($question['questionGrade']),
                            'scale' => floatval($question['maxGrade']),
                            'other_points' => $question['otherPoints']
                        ];
                    }
                    $partScale = $partScale == 0 ? NULL : $partScale;
                    $partsDoc[] = [
                        'id' => $part['id'],
                        'type_part' => $part['type'],
                        'part_grade' => floatval($part['partGrade']),
                        'scale' => $part['maxGrade'] === NULL ? $partScale : $part['maxGrade'],
                        'questions' => $questionsDoc
                    ];
                }

                // Retrieving current student's class 
                $schoolClasses = $usersRepository
                                                ->findOneBy(array('id' => $student['student_id']))
                                                ->getSchoolclass();
                if (empty($classesDoc)) {
                    $class_id = null;
                } 
                else {
                    $class_id = $this->getStudentClass($schoolClasses, $classesDoc);
                }

                $studentsDoc[] = [
                    'id' => $student['student_id'],
                    'username' => $student['student_username'],
                    'class_id' => $class_id,
                    'exam_grade' => floatval($student['grade']),
                    'parts' => $partsDoc
                ];
            }

            /*if ($exam->getSubject() !== NULL) {
                $ey = $exam->getSubject()->getEducationyear()->toArray();
                if (!empty($ey)) {
                    $educationYear = $ey[0]->getId();
                }
                else {
                    $educationYear = 0;
                }
            }
            else {
                $educationYear = 0;
            }*/

            $year = $exam->getYear() === NULL ? NULL : $exam->getYear()->getId();
            $subjectId = $exam->getSubject() === NULL ? NULL : $exam->getSubject()->getId();
            $author = $exam->getAuthor() === NULL ? NULL : $exam->getAuthor()->getId();
            $examCategoryId = $exam->getCategory() === NULL ? NULL : $exam->getCategory()->getId();
            $educationYear = $exam->getSubject() === NULL ? NULL : $exam->getSubject()->getId();
            $coef = $examsRepository->getExamCoef($examCategoryId, $subjectId);

            // Creation of a document exams_info
            $examInfo = [
                'id'                => $exam_id,
                'name'              => $exam->getName(),
                'exam_validate'     => $exam->getExamValidate(),
                'exam_category_id'  => $examCategoryId,
                'coefficient'       => $coef,
                'author_id'         => $author,
                'school_id'         => $exam->getSchool()->getId(),
                'planned_date'      => $exam->getPlannedDate(),
                'year_id'           => $year,
                'education_year_id' => $educationYear,
                'subject_id'        => $subjectId,
                'students'          => $studentsDoc,
                'classes'           => $classesDoc
            //  'year_name' => $exam->getYear()->getName(),
            //  'subject_name' => $exam->getSubject()->getName(),
            ];
            $document = new Document();
            $document->setData($examInfo)
                     ->setId($exam_id);

            // Indexing the document in Elasticsearch
            $examsInfoType->addDocument($document);
            $response = $index->refresh();
        }

        if (isset($response) && $response->hasError()) {
            $output->writeln("Errors occured");
        }
    }
}