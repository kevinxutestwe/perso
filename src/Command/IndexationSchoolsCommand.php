<?php

namespace App\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Elastica\Client;
use Elastica\Document;
use Elastica\Type\Mapping;
use App\Entity\Schools;
use App\Entity\Exams;
use App\Entity\Users;
use App\Entity\SchoolClasses;
use App\Entity\EducationYears;
use App\Entity\SchoolSubjects;

class IndexationSchoolsCommand extends ContainerAwareCommand
{

    protected function configure() {
        $this
            ->setName('indexation:schools_info')
            ->setDescription('Indexation des écoles sur Elasticsearch');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        // Creation of index and mapping        
        $client = new Client();
        $schoolsInfoIndex = $client->getIndex('schools_info');
        $schoolsInfoIndex->create(
            array(
                'number_of_shards' => 5,
                'number_of_replicas' => 1
            ),
            true
        );
        $schoolsInfoType = $schoolsInfoIndex->getType('schools_info');
        $mapping = new Mapping();
        $properties = $this
                          ->getContainer()
                          ->get('mapping.infos')
                          ->getSchoolsInfoMapping();
        $mapping
               ->setType($schoolsInfoType)
               ->setProperties($properties);
        $mapping->send();
        $index = $schoolsInfoType->getIndex();
    
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $schoolsRepository = $em->getRepository(Schools::class);
        $examsRepository = $em->getRepository(Exams::class);
        $usersRepository = $em->getRepository(Users::class);
        $schoolClassesRepository = $em->getRepository(SchoolClasses::class);
        $educationYearsRepository = $em->getRepository(EducationYears::class);
        $subjectsRepository = $em->getRepository(SchoolSubjects::class);

        $schools = $schoolsRepository->findAll();

        $schoolsSize = count($schools); // à enlever

        foreach ($schools as $school) {
            $id = $school->getId();

            $output->writeln($id."/".$schoolsSize); /// à enlever

            // Retrieving current school's exams
            $exams = $examsRepository->findBy(
                ['school' => $id]
            );
            $examsDoc = [];
            foreach ($exams as $exam) {
                if (!$exam->getIsValidated()) {
                    continue;
                }

                $examsDoc[] = [
                    'id' => $exam->getId(),
                    'name' => $exam->getName()
                ];
            }

            // Retrieving current school's profs
            $profs = $usersRepository->getProfsBySchool($id);
            $profsDoc = [];
            foreach ($profs as $prof) {
                $profsDoc[] = [
                    'id' => $prof['id'],
                    'username' => $prof['username']
                ];
            }

            // Retrieving current school's students
            $students = $usersRepository->getStudentsBySchool($id);
            $studentsDoc = [];
            foreach ($students as $student) {
                $studentsDoc[] = [
                    'id' => $student['id'],
                    'username' => $student['username']
                ];
            }

            // Retrieving current school's classes
            $classes = $schoolClassesRepository->findBy(
                ['school' => $id]
            );
            $classesDoc = [];
            foreach ($classes as $class) {
                $classesDoc[] = [
                    'id' => $class->getId(),
                    'name' => $class->getName()
                ];
            }

            // Retrieving current school's years
            $educationYears = $educationYearsRepository->findBy(
                ['school' => $id]
            );
            $educationYearsDoc = [];
            foreach ($educationYears as $educationYear) {
                $educationYearsDoc[] = [
                    'id' => $educationYear->getId(),
                    'name' => $educationYear->getName()
                ];
            }

            // Retrieving current school's subjects
            $subjects = $subjectsRepository->findBy(
                ['school' => $id]
            );
            $subjectsDoc = [];
            foreach ($subjects as $subject) {
                $subjectsDoc[] = [
                    'id' => $subject->getId(),
                    'name' => $subject->getName()
                ];
            }

            // Creation of a document schools_info
            $schoolInfo = [
                'id' => $id,
                'name' => $school->getName(),
                'exams' => $examsDoc,
                'profs' => $profsDoc,
                'students' => $studentsDoc,
                'classes' => $classesDoc,
                'education_years' => $educationYearsDoc,
                'subjects' => $subjectsDoc
            ];
            $document = new Document();
            $document
                    ->setData($schoolInfo)
                    ->setId($id);
                    
            // Indexing the document in Elasticsearch
            $schoolsInfoType->addDocument($document);
            $response = $index->refresh();
        }

        if (isset($response) && $response->hasError()) {
            $output->writeln("Errors occured");
        }
    }
}