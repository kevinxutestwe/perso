<?php

namespace App\Service;

use Elastica\Query;
use Elastica\Search;
use Elastica\Aggregation;
use Elastica\Index;
use \Exception;

class ExamsInfo
{
    /*
     * @var $examsInfoIndex Index
     * @var $field 'author'|'school'|'subject'|'category'|'education_year'
     * @var $field_id int
     * @var $year_id int 
     * @return array 
     */
    public function getExamsByYear(Index $examsInfoIndex, $field, $field_id, $year_id) {
        // Creation of the query
        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $agg = new Aggregation\Terms('agg');
        $agg
           ->setField($field.'_id')
           ->setInclude([strval($field_id)]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg
               ->setField('year_id')
               ->setSize(10000);

        if ($year_id != -1) {
            $yearAgg->setInclude([strval($year_id)]);
        }
        else {
            $yearAgg
                   ->setOrder('_key', 'asc')
                   ->setSize(10000);
        }

        $examCategoryAgg = new Aggregation\Terms('examCategoryAgg');
        $examCategoryAgg
                       ->setField('exam_category_id')
                       ->setSize(10000);
        
        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');

        $examStats = new Aggregation\Stats('examStats');
        $examStats->setField('students.exam_grade');

        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setSize(1);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');

        $maxGrade = new Aggregation\Sum('maxGrade');
        $maxGrade->setField('students.parts.scale');

        $partsNested->addAggregation($maxGrade);
        $studentAgg->addAggregation($partsNested);
        $studentsNested
                      ->addAggregation($examStats)
                      ->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg); 
        $examCategoryAgg->addAggregation($examAgg);
        $yearAgg->addAggregation($examCategoryAgg);
        $agg->addAggregation($yearAgg);
        $examValidateAgg->addAggregation($agg);

        $query = new Query();
        $query
             ->setSize(0)
             ->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('no exam validated');
        }

        if (empty($results['examValidateAgg']['buckets'][0]['agg']['buckets'])) {
            throw new Exception('no result by '.$field);
        }

        $results = $results['examValidateAgg']['buckets'][0]['agg']['buckets'][0]['yearAgg']['buckets'];
        if (empty($results)) {
            throw new Exception('year not found');
        }

        $years = [];
        foreach ($results as $year) {
            $exams = [];
            $minYear = PHP_INT_MAX;
            $maxYear = PHP_INT_MIN;
            $examCategories = [];

            // Processing of the exam categories for one year
            foreach ($year['examCategoryAgg']['buckets'] as $examCategory) {
                $sumGradeExamCategory = 0;

                // Processing of the exams for one exam category
                foreach ($examCategory['examAgg']['buckets'] as $exam) {    
                    $examStats = $exam['studentsNested']['examStats'];

                    if (empty($exam['studentsNested']['studentAgg']['buckets'])) {
                        $scale = 1;
                    }
                    else {
                        $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];
                    }
                    $coef = $exam['coefAgg']['value'];

                    // TODO
                    //print_r($scale."\n");
                    $scale = $scale == 0 ? 1 : $scale/20;
                    //$scale = $scale == 0 ? $examStats['max']/20 : $scale/20;

                    $min = $examStats['min'] === null ? null : round($examStats['min']/$scale, 2);
                    $max = $examStats['max'] === null ? null : round($examStats['max']/$scale, 2);
                    $avg = $examStats['avg'] === null ? null : round($examStats['avg']/$scale, 2);

                    $sumGradeExamCategory += $avg; 

                    if ($avg !== null) {
                        $minYear = $avg < $minYear ? $avg : $minYear;
                        $maxYear = $avg > $maxYear ? $avg : $maxYear;
                    }

                    $exams[] = [
                        'exam_id'     => $exam['key'],
                        'exam_category_id' => $examCategory['key'],
                        'coefficient' => $coef,
                        'nb_students' => $examStats['count'],
                        'min'         => $min,
                        'max'         => $max,
                        'avg'         => $avg,
                        'scale'       => 20
                    ];    
                }
                $examCategories[$examCategory['key']] = [
                    'nb_exams' => $examCategory['doc_count'],
                    'avg'      => $sumGradeExamCategory/$examCategory['doc_count'],
                    'coef'     => isset($coef) ? $coef : NULL 
                ];
            }

            // Average for one year
            $sumAvg = 0;
            $sumCoef = 0;
            foreach ($examCategories as $current) {
                $sumAvg += $current['avg']*$current['coef'];
                $sumCoef += $current['coef'];   
            }

            $years[] = [
                'year_id'  => $year['key'],
                'min'      => $minYear == PHP_INT_MAX ? NULL : $minYear, 
                'max'      => $maxYear == PHP_INT_MIN ? NULL : $maxYear,
                'avg'      => $sumAvg == 0 ? NULL : round($sumAvg/$sumCoef, 2),
                'nb_exams' => $year['doc_count'],
                'exams'    => $exams
            ];
        }

        return [
            $field.'_id'   => $field_id,
            'nb_years'     => count($years),
            'years'        => $years
        ]; 
    }

    /*
     * @var $examsInfoIndex Index
     * @var $field 'author'|'school'|'subject'|'category'|'education_year'
     * @var $field_id int
     * @var $year_id int 
     * @return array 
     */
    public function getExamsByField(Index $examsInfoIndex, $field, $field_id) {
        // Creation of the query
        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $agg = new Aggregation\Terms('agg');
        $agg
           ->setField($field.'_id')
           ->setInclude([strval($field_id)]);

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');

        $examStats = new Aggregation\Stats('examStats');
        $examStats->setField('students.exam_grade');

        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setSize(1);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');

        $maxGrade = new Aggregation\Sum('maxGrade');
        $maxGrade->setField('students.parts.scale');

        $partsNested->addAggregation($maxGrade);
        $studentAgg->addAggregation($partsNested);
        $studentsNested
                      ->addAggregation($examStats)
                      ->addAggregation($studentAgg);
        $examAgg->addAggregation($studentsNested); 
        $agg->addAggregation($examAgg);
        $examValidateAgg->addAggregation($agg);

        $query = new Query();
        $query
             ->setSize(0)
             ->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('no exam validated');
        }
        if (empty($results['examValidateAgg']['buckets'][0]['agg']['buckets'])) {
            throw new Exception('no result');
        }
        $results = $results['examValidateAgg']['buckets'][0]['agg']['buckets'][0];

        $exams = [];
        $sumAll = 0;
        $minAll = PHP_INT_MAX;
        $maxAll = PHP_INT_MIN;

        foreach ($results['examAgg']['buckets'] as $exam) {    
            $examStats = $exam['studentsNested']['examStats'];

            // Scale
            if (empty($exam['studentsNested']['studentAgg']['buckets'])) {
                $scale = 0;
            }
            else {
                $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];
            }

            // TODO
            //$scale = $scale == 0 ? $examStats['max']/20 : $scale/20;
            $scale = $scale == 0 ? 1 : $scale/20;

            $min = $examStats['min'] === null ? null : round($examStats['min']/$scale, 2);
            $max = $examStats['max'] === null ? null : round($examStats['max']/$scale, 2);
            $avg = $examStats['avg'] === null ? null : round($examStats['avg']/$scale, 2);

            $exams[] = [
                'exam_id'     => $exam['key'],
                'nb_students' => $examStats['count'],
                'min'         => $min,
                'max'         => $max,
                'avg'         => $avg,
                'scale'       => 20
            ];

            $sumAll += $avg; 
            if ($avg !== null) {
                $minAll = $avg < $minAll ? $avg : $minAll;
                $maxAll = $avg > $maxAll ? $avg : $maxAll;
            }
        }

        $stats = [
            $field.'_id'   => $field_id,
            'min'          => $minAll == PHP_INT_MAX ? NULL : $minAll, 
            'max'          => $maxAll == PHP_INT_MIN ? NULL : $maxAll, 
            'avg'          => round($sumAll/$results['doc_count'], 2),
            'nb_exams'     => $results['doc_count'],
            'exams'        => $exams
        ];
        return $stats;
    }
    
    /*
     * @var $grade20 int|NULL
     * @var $grade int
     * @var $scale int
     * @return double|NULL 
     */
    public function checkGrade20($grade20, $grade, $scale) {
        if ($grade20 === NULL && $scale == 0 && $grade !== NULL) {
            return round($grade, 2);
        }
        else if ($grade20 !== NULL) {
            return round($grade20, 2);
        }
        else {
            return NULL;
        }
    }

}