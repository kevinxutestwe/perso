<?php

namespace App\Service;

use Elastica\Query;
use Elastica\Search;
use Elastica\Aggregation;
use Elastica\Index;
use \Exception;

class ProfsInfo
{
    /*
     * @var $examsInfoIndex Index
     * @var $params = [$prof_id, $field_id, $field, $year_id]
     * @return array 
     */
    public function getProfStatsByFieldByYear(Index $examsInfoIndex, $params) {
        // Creation of the query
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('author_id', $params['prof_id']);
        $boolQuery->addMust($match);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg->setField('year_id');
        if ($params['year_id'] != -1) {
            $yearAgg
                   ->setInclude([strval($params['year_id'])])
                   ->setSize(1);
        }
        else {
            $yearAgg
                   ->setSize(10000)
                   ->setOrder('_key', 'asc');
        }

        $agg = new Aggregation\Terms('agg');
        $agg->setField($params['field'].'_id');
        if (strcmp($params['field_id'], 'all') != 0) {
            $agg
               ->setInclude([strval($params['field_id'])])
               ->setSize(1);
        }
        else {
            $agg
               ->setSize(10000)
               ->setOrder('_key', 'asc');
        }

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');
        
        $studentsNested = new Aggregation\Nested('studentsNested', 'students');

        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setSize(1);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');

        $scale = new Aggregation\Sum('scale');
        $scale->setField('students.parts.scale');

        $examStats = new Aggregation\Stats('examStats');
        $examStats->setField('students.exam_grade');

        $partsNested->addAggregation($scale);
        $studentAgg->addAggregation($partsNested);
        $studentsNested
                      ->addAggregation($studentAgg)
                      ->addAggregation($examStats);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $agg->addAggregation($examAgg);
        $yearAgg->addAggregation($agg);
        $examValidateAgg->addAggregation($yearAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->setQuery($boolQuery)
             ->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('No exam validated');
        }

        $results = $results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'];
        if (empty($results)) {
            throw new Exception('year not found');
        }

        $years = [];
        foreach ($results as $year) {
            if (empty($year['agg']['buckets'])) {
                continue;
            } 
            $sumYear = 0;
            $minYear = PHP_INT_MAX;
            $maxYear = PHP_INT_MIN;
            // Processing of the data for one year
            $stats = [];
            foreach ($year['agg']['buckets'] as $current) {
                $exams = [];
                $sumExam = 0;
                $minExam = PHP_INT_MAX;
                $maxExam = PHP_INT_MIN;
                $sumCoef = 0;
                // Processing of the exams for one current
                foreach ($current['examAgg']['buckets'] as $exam){
                    if (empty($exam['studentsNested']['studentAgg']['buckets'])) {
                        $scale = 0;
                        $statsExam = [
                            'max' => NULL,
                            'avg' => NULL
                        ];
                    }
                    else {
                        $statsExam = $exam['studentsNested']['examStats'];
                        $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['scale']['value'];
                    }

                    $coef = $exam['coefAgg']['value'];
                    $scale = $scale == 0 ? $statsExam['max']/20 : $scale/20;
                    $avg = $statsExam['avg'] == NULL ? NULL : round($statsExam['avg']/$scale, 2);

                    $sumCoef += $coef;
                    $sumExam += $avg*$coef; 
                    if ($avg !== NULL) {
                        $minExam = $avg < $minExam ? $avg : $minExam;
                        $maxExam = $avg > $maxExam ? $avg : $maxExam;
                    }

                    $exams[] = [
                        'exam_id'     => $exam['key'],
                        'coefficient' => $coef,
                        'avg'         => $avg,
                        'scale'       => 20
                    ];
                }

                $avgExams = $sumExam == 0 ? NULL : round($sumExam/$sumCoef, 2);
                if ($avgExams !== NULL) {
                    $sumYear += $avgExams;
                    $minYear = $avgExams < $minYear ? $avgExams : $minYear;
                    $maxYear = $avgExams > $maxYear ? $avgExams : $maxYear;
                }
                $stats[] = [
                    $params['field'].'_id' => $current['key'],
                    'min'                  => $minExam == PHP_INT_MAX ? NULL : $minExam,
                    'max'                  => $maxExam == PHP_INT_MIN ? NULL : $maxExam,
                    'avg'                  => $avgExams,
                    'nb_exams'             => count($exams),
                    'exams'                => $exams
                ];
            }
            
            $nb_stats = count($stats);
            $years[] = [
                'year_id'               => $year['key'],
                'min'                   => $minYear == PHP_INT_MAX ? NULL : $minYear,
                'max'                   => $maxYear == PHP_INT_MIN ? NULL : $maxYear,
                'avg'                   => $sumYear == 0 ? NULL : round($sumYear/$nb_stats, 2),
                'nb_exams'              => $year['doc_count'],
                'nb_'.$params['field']  => $nb_stats,
                $params['field']        => $stats
            ];
        }

        return [
            'prof_id'  => $params['prof_id'],
            'nb_years' => count($years),
            'years'    => $years
        ];
    }
}