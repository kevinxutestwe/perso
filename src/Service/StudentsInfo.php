<?php

namespace App\Service;

use Elastica\Query;
use Elastica\Search;
use Elastica\Aggregation;
use Elastica\Index;
use \Exception;

class StudentsInfo
{
    /*
     * @var $examsInfoIndex Index
     * @var $student_id int
     * @return array
     */
    public function getStudentStats(Index $examsInfoIndex, $student_id) {
        // Search of the student's exams
        $query = new Query();

        $nested = new Query\Nested();
        $nested->setPath('students');
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('students.id', $student_id);
        $boolQuery->addMust($match);
        $nested->setQuery($boolQuery);
        $query
             ->setQuery($nested)
             ->setSize(0);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $examCategoryAgg = new Aggregation\Terms('examCategoryAgg');
        $examCategoryAgg
                       ->setField('exam_category_id')
                       ->setSize(10000);

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');
        
        $studentsNested = new Aggregation\Nested('studentsNested', 'students');

        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setInclude([strval($student_id)]);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');

        $examGrade = new Aggregation\Sum('examGrade');
        $examGrade->setField('students.parts.part_grade');

        $maxGrade = new Aggregation\Sum('maxGrade');
        $maxGrade->setField('students.parts.scale');

        $gradeES20Script = new Aggregation\BucketScript('gradeES20');
        $gradeES20Script
                     ->setBucketsPath([
                         'max' => 'partsNested.maxGrade',
                         'grade' => 'partsNested.examGrade'
                     ])
                     ->setScript('params.grade/params.max * 20');

        $partsNested
                   ->addAggregation($examGrade)
                   ->addAggregation($maxGrade);
        $studentAgg
                  ->addAggregation($partsNested)
                  ->addAggregation($gradeES20Script);
        $studentsNested->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $examCategoryAgg->addAggregation($examAgg);
        $examValidateAgg->addAggregation($examCategoryAgg);
        $query->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('no result');            
        }
        $results = $results['examValidateAgg']['buckets'][0];

        // Processing of the results
        $exams = [];
        $examCategories = [];
        $minGrade = PHP_INT_MAX;
        $maxGrade = PHP_INT_MIN;

        foreach ($results['examCategoryAgg']['buckets'] as $examCategory) {
            $sumGradeExamCategory = 0;
            foreach ($examCategory['examAgg']['buckets'] as $exam) {
                $coef = $exam['coefAgg']['value'];

                $gradeES20 = $exam['studentsNested']['studentAgg']['buckets'][0]['gradeES20']['value'];
                $grade = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['examGrade']['value'];
                $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];

                $grade = $this->checkGrade20($gradeES20, $grade, $scale);    

                $sumGradeExamCategory += $grade; 
                if ($grade !== null) {
                    $minGrade = $grade < $minGrade ? $grade : $minGrade;
                    $maxGrade = $grade > $maxGrade ? $grade : $maxGrade;
                }

                $exams[] = [
                    'exam_id'          => $exam['key'],
                    'exam_category_id' => $examCategory['key'],
                    'coefficient'      => $coef,
                    'grade'            => $grade,
                    'scale'            => 20
                ];
            }
            $examCategories[$examCategory['key']] = [
                'nb_exams' => $examCategory['doc_count'],
                'avg'      => $sumGradeExamCategory/$examCategory['doc_count'],
                'coef'     => isset($coef) ? $coef : NULL 
            ];
        }

        $sumAvg = 0;
        $sumCoef = 0;
        foreach ($examCategories as $current) {
            $sumAvg += $current['avg']*$current['coef'];
            $sumCoef += $current['coef'];
        }

        return [
            'student_id' => $student_id,
            'nb_exams'   => count($exams),
            'min'        => $minGrade == PHP_INT_MAX ? NULL : $minGrade,
            'max'        => $maxGrade == PHP_INT_MIN ? NULL : $maxGrade,
            'avg'        => $sumAvg == 0 ? NULL : round($sumAvg/$sumCoef, 2),
            'exams'      => $exams
        ];
    }

    /*
     * @var $examsInfoIndex Index
     * @var $student_id int
     * @var $year_id int 
     * @return array 
     */
    public function getStudentStatsByYear(Index $examsInfoIndex, $student_id, $year_id) {
        // Creation of the query
        $nested = new Query\Nested();
        $nested->setPath('students');
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('students.id', $student_id);
        $boolQuery->addMust($match);
        $nested->setQuery($boolQuery);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg->setField('year_id');

        if ($year_id != -1) {
            $yearAgg
                   ->setInclude([strval($year_id)])
                   ->setSize(1);
        }
        else {
            $yearAgg
                   ->setSize(10000)
                   ->setOrder('_key', 'asc');
        }

        $examCategoryAgg = new Aggregation\Terms('examCategoryAgg');
        $examCategoryAgg
                       ->setField('exam_category_id')
                       ->setSize(10000);

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');
        
        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');
        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setInclude([strval($student_id)]);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');
        $examGrade = new Aggregation\Sum('examGrade');
        $examGrade->setField('students.parts.part_grade');

        $maxGrade = new Aggregation\Sum('maxGrade');
        $maxGrade->setField('students.parts.scale');

        $gradeES20Script = new Aggregation\BucketScript('gradeES20');
        $gradeES20Script
                     ->setBucketsPath([
                         'max' => 'partsNested.maxGrade',
                         'grade' => 'partsNested.examGrade'
                     ])
                     ->setScript('params.grade/params.max * 20');

        $partsNested
                   ->addAggregation($examGrade)
                   ->addAggregation($maxGrade);
        $studentAgg
                  ->addAggregation($partsNested)
                  ->addAggregation($gradeES20Script);
        $studentsNested->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $examCategoryAgg->addAggregation($examAgg);
        $yearAgg->addAggregation($examCategoryAgg);
        $examValidateAgg->addAggregation($yearAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->setQuery($nested)
             ->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('no result');
        }
        $results = $results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'];
        if (empty($results)) {
            throw new Exception('year not found');
        }

        $years = [];
        foreach ($results as $year) {
            $exams = [];
            $minGrade = PHP_INT_MAX;
            $maxGrade = PHP_INT_MIN;
            $examCategories = [];

            // Processing of the exam_categories for one year
            foreach ($year['examCategoryAgg']['buckets'] as $examCategory) {
                $sumGradeExamCategory = 0;
                // Processing of the exams for one exam category
                foreach ($examCategory['examAgg']['buckets'] as $exam) {
                    $gradeES20 = $exam['studentsNested']['studentAgg']['buckets'][0]['gradeES20']['value'];
                    $grade = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['examGrade']['value'];
                    $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];

                    $grade = $this->checkGrade20($gradeES20, $grade, $scale);    

                    $coef = $exam['coefAgg']['value'];

                    $sumGradeExamCategory += $grade; 
                    if ($grade !== null) {
                        $minGrade = $grade < $minGrade ? $grade : $minGrade;
                        $maxGrade = $grade > $maxGrade ? $grade : $maxGrade;
                    }

                    $exams[] = [
                        'exam_id'          => $exam['key'],
                        'exam_category_id' => $examCategory['key'],
                        'coefficient'      => $coef,
                        'grade'            => $grade,
                        'scale'            => 20
                    ];
                }
                $examCategories[$examCategory['key']] = [
                    'nb_exams' => $examCategory['doc_count'],
                    'avg'      => $sumGradeExamCategory/$examCategory['doc_count'],
                    'coef'     => isset($coef) ? $coef : NULL 
                ];
            }

            // Average for one year
            $sumAvg = 0;
            $sumCoef = 0;
            foreach ($examCategories as $current) {
                $sumAvg += $current['avg']*$current['coef'];
                $sumCoef += $current['coef'];
            }

            $years[] = [
                'year_id'  => $year['key'],
                'min'      => $minGrade == PHP_INT_MAX ? NULL : $minGrade,
                'max'      => $maxGrade == PHP_INT_MIN ? NULL : $maxGrade,
                'avg'      => $sumAvg == 0 ? NULL : round($sumAvg/$sumCoef, 2),
                'nb_exams' => $year['doc_count'],
                'exams'    => $exams
            ];
        }

        return [
            'student_id' => $student_id,
            'nb_years'   => count($years),
            'years'      => $years
        ];
    }

    /*
     * @var $examsInfoIndex Index
     * @var $params = [$student_id, $field_id, $field, $year_id]
     * @throws Exception     
     * @return array 
     */
    public function getStudentStatsByFieldByYear(Index $examsInfoIndex, $params) {
        // Creation of the query
        $nested = new Query\Nested();
        $nested->setPath('students');
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('students.id', $params['student_id']);
        $boolQuery->addMust($match);
        $nested->setQuery($boolQuery);

        $examValidateAgg = new Aggregation\Terms('examValidateAgg');
        $examValidateAgg
                       ->setField('exam_validate')
                       ->setInclude([true]);

        $yearAgg = new Aggregation\Terms('yearAgg');
        $yearAgg->setField('year_id');
        if ($params['year_id'] != -1) {
            $yearAgg
                   ->setInclude([strval($params['year_id'])])
                   ->setSize(1);
        }
        else {
            $yearAgg
                   ->setSize(10000)
                   ->setOrder('_key', 'asc');
        }

        $agg = new Aggregation\Terms('agg');
        $agg->setField($params['field'].'_id');
        if (strcmp($params['field_id'], 'all') != 0) {
            $agg
               ->setInclude([strval($params['field_id'])])
               ->setSize(1);
        }
        else {
            $agg
               ->setSize(10000)
               ->setOrder('_key', 'asc');
        }

        $examAgg = new Aggregation\Terms('examAgg');
        $examAgg
               ->setField('id')
               ->setSize(10000)
               ->setOrder('_key', 'asc');

        $coefAgg = new Aggregation\Max('coefAgg');
        $coefAgg->setField('coefficient');

        $studentsNested = new Aggregation\Nested('studentsNested', 'students');
        $studentAgg = new Aggregation\Terms('studentAgg');
        $studentAgg
                  ->setField('students.id')
                  ->setInclude([strval($params['student_id'])]);

        $partsNested = new Aggregation\Nested('partsNested', 'students.parts');
        $examGrade = new Aggregation\Sum('examGrade');
        $examGrade->setField('students.parts.part_grade');

        $scale = new Aggregation\Sum('maxGrade');
        $scale->setField('students.parts.scale');

        $gradeES20Script = new Aggregation\BucketScript('gradeES20');
        $gradeES20Script
        ->setBucketsPath([
            'max' => 'partsNested.maxGrade',
            'grade' => 'partsNested.examGrade'
        ])
        ->setScript('params.grade/params.max * 20');

        $partsNested
                   ->addAggregation($examGrade)
                   ->addAggregation($scale);
        $studentAgg
                  ->addAggregation($partsNested)
                  ->addAggregation($gradeES20Script);
        $studentsNested->addAggregation($studentAgg);
        $examAgg
               ->addAggregation($studentsNested)
               ->addAggregation($coefAgg);
        $agg->addAggregation($examAgg);
        $yearAgg->addAggregation($agg);
        $examValidateAgg->addAggregation($yearAgg);

        $query = new Query();
        $query
             ->setSize(0)
             ->setQuery($nested)
             ->addAggregation($examValidateAgg);

        $results = $examsInfoIndex
                                 ->search($query)
                                 ->getAggregations();

        // Processing of the results
        if (empty($results['examValidateAgg']['buckets'])) {
            throw new Exception('no exam validated');
        }
        if (empty($results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'])) {
            throw new Exception('year not found');
        }
        $results = $results['examValidateAgg']['buckets'][0]['yearAgg']['buckets'];

        $years = [];
        foreach ($results as $year) {
            if (empty($year['agg']['buckets'])) {
                throw new Exception($params['field'].' not found');
            } 
            $sumAvg = 0;
            $minAvg = PHP_INT_MAX;
            $maxAvg = PHP_INT_MIN;
            // Processing of the data for one year
            $stats = [];
            foreach ($year['agg']['buckets'] as $current) {
                $exams = [];
                $sumGrade = 0;
                $sumCoef = 0;
                $minGrade = PHP_INT_MAX;
                $maxGrade = PHP_INT_MIN;
                // Processing of the exams for one current
                foreach ($current['examAgg']['buckets'] as $exam){
                    $coef = $exam['coefAgg']['value'];
                    $gradeES20 = $exam['studentsNested']['studentAgg']['buckets'][0]['gradeES20']['value'];
                    $grade = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['examGrade']['value'];
                    $scale = $exam['studentsNested']['studentAgg']['buckets'][0]['partsNested']['maxGrade']['value'];

                    $grade = $this->checkGrade20($gradeES20, $grade, $scale);   

                    $exams[] = [
                        'exam_id'     => $exam['key'],
                        'coefficient' => $coef,
                        'grade'       => $grade,
                        'scale'       => 20
                    ];

                    $sumCoef += $coef;
                    $sumGrade += $grade*$coef; 
                    if ($grade !== NULL) {
                        $minGrade = $grade < $minGrade ? $grade : $minGrade;
                        $maxGrade = $grade > $maxGrade ? $grade : $maxGrade;
                    }
                }

                $avg = $sumGrade == 0 ? NULL : round($sumGrade/$sumCoef, 2);
                if ($avg !== NULL) {
                    $sumAvg += $avg;
                    $minAvg = $avg < $minAvg ? $avg : $minAvg;
                    $maxAvg = $avg > $maxAvg ? $avg : $maxAvg;
                }
                $stats[] = [
                    $params['field'].'_id' => $current['key'],
                    'nb_exams'             => count($exams),
                    'min'                  => $minGrade == PHP_INT_MAX ? NULL : $minGrade,
                    'max'                  => $maxGrade == PHP_INT_MIN ? NULL : $maxGrade,
                    'avg'                  => $avg,
                    'exams'                => $exams
                ];
            }
            
            $nb_stats = count($stats);
            $years[] = [
                'year_id'               => $year['key'],
                'min'                   => $minAvg,
                'max'                   => $maxAvg,
                'avg'                   => round($sumAvg/$nb_stats, 2),
                'nb_exams'              => $year['doc_count'],
                'nb_'.$params['field']  => $nb_stats,
                $params['field']        => $stats
            ];
        }

        return [
            'student_id'       => $params['student_id'],
            'nb_years'         => count($years),
            'years'            => $years
        ];
    }

    /*
     * @var $gradeES20 int|NULL
     * @var $grade int
     * @var $scale int
     * @return double|NULL 
     */
    public function checkGrade20($gradeES20, $grade, $scale) {
        if ($gradeES20 === NULL && $scale == 0 && $grade !== NULL) {
            return round($grade, 2);
        }
        else if ($gradeES20 !== NULL) {
            return round($gradeES20, 2);
        }
        else {
            return NULL;
        }
    }

}