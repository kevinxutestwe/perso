<?php

namespace App\Service;

use Elastica\Query;
use Elastica\Search;
use Elastica\Index;

class SchoolsInfo
{
    /**
     * @var $examsInfoIndex Index
     * @var $school_id int
     * @var $info String
     * @return array 
     */
	public function getSchoolsInfo(Index $schools_info_index, $school_id, $info) {
        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $match = new Query\Match();
        $match->setField('id', $school_id);
        $boolQuery->addMust($match);
        $query->setQuery($boolQuery);
        $schoolInfos = $schools_info_index
                                         ->search($query)
                                         ->getResults();
        $data = $schoolInfos[0]->getData();
        return $data[$info];
    }
}