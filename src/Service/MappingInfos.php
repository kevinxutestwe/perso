<?php

namespace App\Service;

use Elastica\Type\Mapping as Mapping;

class MappingInfos
{
    /*
     * @return array 
     */
    public function getSchoolsInfoMapping() {
        return [
            'id'    => ['type' => 'integer'],
            'name'  => ['type' => 'text'],
            'exams' => [
                'type'       => 'nested',
                'properties' => [
                    'id'   => ['type' => 'integer'],
                    'name' => ['type' => 'text']
                ]
            ],
            'profs' => [
                'type'       => 'nested',
                'properties' => [
                    'id'       => ['type' => 'integer'],
                    'username' => ['type' => 'text']
                ]
            ],
            'students' => [
                'type' => 'nested',
                'properties' => [
                    'id'       => ['type' => 'integer'],
                    'username' => ['type' => 'text']
                ]
            ],
            'classes' => [
                'type'       => 'nested',
                'properties' => [
                    'id'   => ['type' => 'integer'],
                    'name' => ['type' => 'text']
                ]
            ],
            'education_years' => [
                'type'       => 'nested',
                'properties' => [
                    'id'   => ['type' => 'integer'],
                    'name' => ['type' => 'text']
                ]
            ],
            'subjects' => [
                'type'       => 'nested',
                'properties' => [
                    'id'   => ['type' => 'integer'],
                    'name' => ['type' => 'text']
                ]
            ]
        ];
    }

    /*
     * @return array 
     */
    public function getExamsInfoMapping() {
        return [
            'id'                => ['type' => 'integer'],
            'name'              => ['type' => 'text'],
            'exam_validate'     => ['type' => 'boolean'],
            'exam_category_id'  => ['type' => 'integer'],
            'coefficient'       => ['type' => 'float'],
            'author_id'         => ['type' => 'integer'],
            'school_id'         => ['type' => 'integer'],
            'planned_date'      => [
                'type'   => 'date',
                'format' => 'dateOptionalTime'
            ],
            'year_id'           => ['type' => 'integer'],
            'education_year_id' => ['type' => 'integer'],
            'subject_id'        => ['type' => 'integer'],
            'students'          => [
                'type'       => 'nested',
                'properties' => [
                    'id'         => ['type' => 'integer'],
                    'username'   => ['type' => 'text'],
                    'class_id'   => ['type' => 'integer'],
                    'exam_grade' => ['type' => 'integer'],
                    'parts'      => [
                        'type'       => 'nested',
                        'properties' => [
                            'id'         => ['type' => 'integer'],
                            'type_part'  => ['type' => 'text'],
                            'part_grade' => ['type' => 'integer'],
                            'scale'      => ['type' => 'integer'],
                            'questions'  => [
                                'type'       => 'nested',
                                'properties' => [
                                    'id'             => ['type' => 'integer'],
                                    'name'           => ['type' => 'text'],
                                    'question_grade' => ['type' => 'integer'],
                                    'scale'          => ['type' => 'integer'],
                                    'other_points'   => ['type' => 'integer']
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'classes'  => [
                'type'       => 'nested',
                'properties' => [
                    'id'   => ['type' => 'integer'],
                    'name' => ['type' => 'text']
                ]
            ]
        ];
    }
}