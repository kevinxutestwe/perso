<?php

namespace App\Converter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
/**
 * ElasticWeConverter.
 *
 * @author Kevin Xu
 */
class ElasticWeConverter implements ParamConverterInterface
{
    /**
     * @var ManagerRegistry
     */
    private $registry;
    
    /**
     * @var ExpressionLanguage
     */
    private $language;
    
    public function __construct(ManagerRegistry $registry = null, ExpressionLanguage $expressionLanguage = null) {
        $this->registry = $registry;
        $this->language = $expressionLanguage;
    }
    
    /**
     * {@inheritdoc}
     *
     * @throws \LogicException       When unable to guess how to get a Doctrine instance from the request information
     * @throws NotFoundHttpException When object not found
     */
    public function apply(Request $request, ParamConverter $configuration) {
        $name = $configuration->getName();
        $class = $configuration->getClass();

        $em = $this->getManager($class);
        $id = $request->attributes->get($name.'_id');

        $entity = $em->getRepository($class)->findOneBy(['id'=>$id]);
        if (!$entity) {
            $message = sprintf('%s object not found.', $name);
            throw new NotFoundHttpException($message);
        }
        $request->attributes->set($name, $entity);
        
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration) {
        return true;
    }

    private function getManager($class) {
        return $this->registry->getManagerForClass($class);
    }
}