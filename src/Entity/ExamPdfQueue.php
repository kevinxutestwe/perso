<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamPdfQueue
 *
 * @ORM\Table(name="exam_pdf_queue")
 * @ORM\Entity
 */
class ExamPdfQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var int
     *
     * @ORM\Column(name="exam_id", type="integer", nullable=false)
     */
    private $examId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getExamId(): ?int
    {
        return $this->examId;
    }

    public function setExamId(int $examId): self
    {
        $this->examId = $examId;

        return $this;
    }


}
