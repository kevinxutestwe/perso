<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamCompletions
 *
 * @ORM\Table(name="exam_completions", indexes={@ORM\Index(name="IDX_B798E806578D5E91", columns={"exam_id"}), @ORM\Index(name="IDX_B798E806CB944F1A", columns={"student_id"}), @ORM\Index(name="IDX_B798E8064AF38FD1", columns={"deleted_at"}), @ORM\Index(name="IDX_B798E80693F3C6CA", columns={"datetime"})})
 * @ORM\Entity
 */
class ExamCompletions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="completions_same_student_exam", type="integer", nullable=true)
     */
    private $completionsSameStudentExam;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="timezone", type="string", length=50, nullable=true)
     */
    private $timezone;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="post_datetime", type="datetime", nullable=true)
     */
    private $postDatetime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="exam_last_update", type="datetime", nullable=true)
     */
    private $examLastUpdate;

    /**
     * @var float
     *
     * @ORM\Column(name="jury_grade", type="float", precision=10, scale=0, nullable=false)
     */
    private $juryGrade;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="email_send", type="datetime", nullable=true)
     */
    private $emailSend;

    /**
     * @var Exams
     *
     * @ORM\ManyToOne(targetEntity="Exams")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $exam;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(?\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getCompletionsSameStudentExam(): ?int
    {
        return $this->completionsSameStudentExam;
    }

    public function setCompletionsSameStudentExam(?int $completionsSameStudentExam): self
    {
        $this->completionsSameStudentExam = $completionsSameStudentExam;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getPostDatetime(): ?\DateTimeInterface
    {
        return $this->postDatetime;
    }

    public function setPostDatetime(?\DateTimeInterface $postDatetime): self
    {
        $this->postDatetime = $postDatetime;

        return $this;
    }

    public function getExamLastUpdate(): ?\DateTimeInterface
    {
        return $this->examLastUpdate;
    }

    public function setExamLastUpdate(?\DateTimeInterface $examLastUpdate): self
    {
        $this->examLastUpdate = $examLastUpdate;

        return $this;
    }

    public function getJuryGrade(): ?float
    {
        return $this->juryGrade;
    }

    public function setJuryGrade(float $juryGrade): self
    {
        $this->juryGrade = $juryGrade;

        return $this;
    }

    public function getEmailSend(): ?\DateTimeInterface
    {
        return $this->emailSend;
    }

    public function setEmailSend(?\DateTimeInterface $emailSend): self
    {
        $this->emailSend = $emailSend;

        return $this;
    }

    public function getExam(): ?Exams
    {
        return $this->exam;
    }

    public function setExam(?Exams $exam): self
    {
        $this->exam = $exam;

        return $this;
    }

    public function getStudent(): ?Users
    {
        return $this->student;
    }

    public function setStudent(?Users $student): self
    {
        $this->student = $student;

        return $this;
    }


}
