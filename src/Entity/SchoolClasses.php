<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SchoolClasses
 *
 * @ORM\Table(name="school_classes", indexes={@ORM\Index(name="IDX_90C24C5DC32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_90C24C5DD2EECC3F", columns={"school_year_id"}), @ORM\Index(name="IDX_90C24C5D5E237E06", columns={"name"})})
 * @ORM\Entity
 */
class SchoolClasses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var int
     *
     * @ORM\Column(name="id_by_school", type="integer", nullable=false)
     */
    private $idBySchool;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var SchoolYears
     *
     * @ORM\ManyToOne(targetEntity="SchoolYears")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_year_id", referencedColumnName="id")
     * })
     */
    private $schoolYear;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Exams", mappedBy="schoolclass")
     */
    private $exam;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolSubjects", mappedBy="schoolclass")
     */
    private $schoolsubject;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", mappedBy="schoolclass")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exam = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolsubject = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getIdBySchool(): ?int
    {
        return $this->idBySchool;
    }

    public function setIdBySchool(int $idBySchool): self
    {
        $this->idBySchool = $idBySchool;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getSchoolYear(): ?SchoolYears
    {
        return $this->schoolYear;
    }

    public function setSchoolYear(?SchoolYears $schoolYear): self
    {
        $this->schoolYear = $schoolYear;

        return $this;
    }

    /**
     * @return Collection|Exams[]
     */
    public function getExam(): Collection
    {
        return $this->exam;
    }

    public function addExam(Exams $exam): self
    {
        if (!$this->exam->contains($exam)) {
            $this->exam[] = $exam;
            $exam->addSchoolclass($this);
        }

        return $this;
    }

    public function removeExam(Exams $exam): self
    {
        if ($this->exam->contains($exam)) {
            $this->exam->removeElement($exam);
            $exam->removeSchoolclass($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolSubjects[]
     */
    public function getSchoolsubject(): Collection
    {
        return $this->schoolsubject;
    }

    public function addSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if (!$this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject[] = $schoolsubject;
            $schoolsubject->addSchoolclass($this);
        }

        return $this;
    }

    public function removeSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if ($this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject->removeElement($schoolsubject);
            $schoolsubject->removeSchoolclass($this);
        }

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addSchoolclass($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeSchoolclass($this);
        }

        return $this;
    }

}
