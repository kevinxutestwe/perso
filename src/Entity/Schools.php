<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Schools
 *
 * @ORM\Table(name="schools", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_47443BD5EACC7B22", columns={"current_year_id"})}, indexes={@ORM\Index(name="IDX_47443BD55E237E06", columns={"name"}), @ORM\Index(name="IDX_47443BD54AF38FD1", columns={"deleted_at"})})
 * @ORM\Entity
 */
class Schools
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var array|null
     *
     * @ORM\Column(name="allowed_exam_fields", type="simple_array", length=0, nullable=true)
     */
    private $allowedExamFields;

    /**
     * @var array|null
     *
     * @ORM\Column(name="allowed_exam_options", type="simple_array", length=0, nullable=true)
     */
    private $allowedExamOptions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="learning_goals_management", type="boolean", nullable=false)
     */
    private $learningGoalsManagement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="excel_header", type="string", length=255, nullable=true)
     */
    private $excelHeader;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade_a", type="float", precision=10, scale=0, nullable=true)
     */
    private $gradeA;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade_b", type="float", precision=10, scale=0, nullable=true)
     */
    private $gradeB;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $gradeC;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $gradeD;

    /**
     * @var float|null
     *
     * @ORM\Column(name="coefficient", type="float", precision=10, scale=0, nullable=true)
     */
    private $coefficient;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade_e", type="float", precision=10, scale=0, nullable=true)
     */
    private $gradeE;

    /**
     * @var bool
     *
     * @ORM\Column(name="rating_management", type="boolean", nullable=false)
     */
    private $ratingManagement;

    /**
     * @var bool
     *
     * @ORM\Column(name="home_university", type="boolean", nullable=false)
     */
    private $homeUniversity;

    /**
     * @var int|null
     *
     * @ORM\Column(name="grade_max_exam", type="smallint", nullable=true)
     */
    private $gradeMaxExam;

    /**
     * @var string|null
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var array
     *
     * @ORM\Column(name="default_exam_options", type="array", length=0, nullable=false)
     */
    private $defaultExamOptions;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="can_see_correction_stats", type="boolean", nullable=true)
     */
    private $canSeeCorrectionStats;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="can_see_student_stats", type="boolean", nullable=true)
     */
    private $canSeeStudentStats;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="can_students_have_extra_time", type="boolean", nullable=true)
     */
    private $canStudentsHaveExtraTime;

    /**
     * @var SchoolYears
     *
     * @ORM\ManyToOne(targetEntity="SchoolYears")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="current_year_id", referencedColumnName="id")
     * })
     */
    private $currentYear;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAllowedExamFields(): ?array
    {
        return $this->allowedExamFields;
    }

    public function setAllowedExamFields(?array $allowedExamFields): self
    {
        $this->allowedExamFields = $allowedExamFields;

        return $this;
    }

    public function getAllowedExamOptions(): ?array
    {
        return $this->allowedExamOptions;
    }

    public function setAllowedExamOptions(?array $allowedExamOptions): self
    {
        $this->allowedExamOptions = $allowedExamOptions;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getLearningGoalsManagement(): ?bool
    {
        return $this->learningGoalsManagement;
    }

    public function setLearningGoalsManagement(bool $learningGoalsManagement): self
    {
        $this->learningGoalsManagement = $learningGoalsManagement;

        return $this;
    }

    public function getExcelHeader(): ?string
    {
        return $this->excelHeader;
    }

    public function setExcelHeader(?string $excelHeader): self
    {
        $this->excelHeader = $excelHeader;

        return $this;
    }

    public function getGradeA(): ?float
    {
        return $this->gradeA;
    }

    public function setGradeA(?float $gradeA): self
    {
        $this->gradeA = $gradeA;

        return $this;
    }

    public function getGradeB(): ?float
    {
        return $this->gradeB;
    }

    public function setGradeB(?float $gradeB): self
    {
        $this->gradeB = $gradeB;

        return $this;
    }

    public function getGradeC(): ?float
    {
        return $this->gradeC;
    }

    public function setGradeC(?float $gradeC): self
    {
        $this->gradeC = $gradeC;

        return $this;
    }

    public function getGradeD(): ?float
    {
        return $this->gradeD;
    }

    public function setGradeD(?float $gradeD): self
    {
        $this->gradeD = $gradeD;

        return $this;
    }

    public function getCoefficient(): ?float
    {
        return $this->coefficient;
    }

    public function setCoefficient(?float $coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getGradeE(): ?float
    {
        return $this->gradeE;
    }

    public function setGradeE(?float $gradeE): self
    {
        $this->gradeE = $gradeE;

        return $this;
    }

    public function getRatingManagement(): ?bool
    {
        return $this->ratingManagement;
    }

    public function setRatingManagement(bool $ratingManagement): self
    {
        $this->ratingManagement = $ratingManagement;

        return $this;
    }

    public function getHomeUniversity(): ?bool
    {
        return $this->homeUniversity;
    }

    public function setHomeUniversity(bool $homeUniversity): self
    {
        $this->homeUniversity = $homeUniversity;

        return $this;
    }

    public function getGradeMaxExam(): ?int
    {
        return $this->gradeMaxExam;
    }

    public function setGradeMaxExam(?int $gradeMaxExam): self
    {
        $this->gradeMaxExam = $gradeMaxExam;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getDefaultExamOptions(): ?array
    {
        return $this->defaultExamOptions;
    }

    public function setDefaultExamOptions(array $defaultExamOptions): self
    {
        $this->defaultExamOptions = $defaultExamOptions;

        return $this;
    }

    public function getCanSeeCorrectionStats(): ?bool
    {
        return $this->canSeeCorrectionStats;
    }

    public function setCanSeeCorrectionStats(?bool $canSeeCorrectionStats): self
    {
        $this->canSeeCorrectionStats = $canSeeCorrectionStats;

        return $this;
    }

    public function getCanSeeStudentStats(): ?bool
    {
        return $this->canSeeStudentStats;
    }

    public function setCanSeeStudentStats(?bool $canSeeStudentStats): self
    {
        $this->canSeeStudentStats = $canSeeStudentStats;

        return $this;
    }

    public function getCanStudentsHaveExtraTime(): ?bool
    {
        return $this->canStudentsHaveExtraTime;
    }

    public function setCanStudentsHaveExtraTime(?bool $canStudentsHaveExtraTime): self
    {
        $this->canStudentsHaveExtraTime = $canStudentsHaveExtraTime;

        return $this;
    }

    public function getCurrentYear(): ?SchoolYears
    {
        return $this->currentYear;
    }

    public function setCurrentYear(?SchoolYears $currentYear): self
    {
        $this->currentYear = $currentYear;

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
