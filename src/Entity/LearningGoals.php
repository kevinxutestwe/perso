<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LearningGoals
 *
 * @ORM\Table(name="learning_goals", indexes={@ORM\Index(name="IDX_39B65515C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_39B655155CE5917F", columns={"education_year_id"})})
 * @ORM\Entity
 */
class LearningGoals
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="order_value", type="integer", nullable=false)
     */
    private $orderValue;

    /**
     * @var EducationYears
     *
     * @ORM\ManyToOne(targetEntity="EducationYears")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="education_year_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $educationYear;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolSubjects", mappedBy="learninggoal")
     */
    private $schoolsubject;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schoolsubject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrderValue(): ?int
    {
        return $this->orderValue;
    }

    public function setOrderValue(int $orderValue): self
    {
        $this->orderValue = $orderValue;

        return $this;
    }

    public function getEducationYear(): ?EducationYears
    {
        return $this->educationYear;
    }

    public function setEducationYear(?EducationYears $educationYear): self
    {
        $this->educationYear = $educationYear;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|SchoolSubjects[]
     */
    public function getSchoolsubject(): Collection
    {
        return $this->schoolsubject;
    }

    public function addSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if (!$this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject[] = $schoolsubject;
            $schoolsubject->addLearninggoal($this);
        }

        return $this;
    }

    public function removeSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if ($this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject->removeElement($schoolsubject);
            $schoolsubject->removeLearninggoal($this);
        }

        return $this;
    }

}
