<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamCompletionQueue
 *
 * @ORM\Table(name="exam_completion_queue")
 * @ORM\Entity
 */
class ExamCompletionQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="exam_completion_id", type="integer", nullable=false)
     */
    private $examCompletionId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExamCompletionId(): ?int
    {
        return $this->examCompletionId;
    }

    public function setExamCompletionId(int $examCompletionId): self
    {
        $this->examCompletionId = $examCompletionId;

        return $this;
    }


}
