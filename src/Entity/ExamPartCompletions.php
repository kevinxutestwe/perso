<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamPartCompletions
 *
 * @ORM\Table(name="exam_part_completions", indexes={@ORM\Index(name="IDX_58008F076E5A280", columns={"exam_completion_id"}), @ORM\Index(name="IDX_58008F041451284", columns={"exam_part_id"}), @ORM\Index(name="IDX_58008F04AF38FD1", columns={"deleted_at"}), @ORM\Index(name="IDX_58008F093F3C6CA", columns={"datetime"})})
 * @ORM\Entity
 */
class ExamPartCompletions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_finished", type="boolean", nullable=false)
     */
    private $isFinished;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="started_datetime", type="datetime", nullable=true)
     */
    private $startedDatetime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="finished_datetime", type="datetime", nullable=true)
     */
    private $finishedDatetime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="timezone", type="string", length=50, nullable=true)
     */
    private $timezone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer_text", type="text", length=0, nullable=true)
     */
    private $answerText;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="post_datetime", type="datetime", nullable=true)
     */
    private $postDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var ExamParts
     *
     * @ORM\ManyToOne(targetEntity="ExamParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_part_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examPart;

    /**
     * @var ExamCompletions
     *
     * @ORM\ManyToOne(targetEntity="ExamCompletions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_completion_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examCompletion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getStartedDatetime(): ?\DateTimeInterface
    {
        return $this->startedDatetime;
    }

    public function setStartedDatetime(?\DateTimeInterface $startedDatetime): self
    {
        $this->startedDatetime = $startedDatetime;

        return $this;
    }

    public function getFinishedDatetime(): ?\DateTimeInterface
    {
        return $this->finishedDatetime;
    }

    public function setFinishedDatetime(?\DateTimeInterface $finishedDatetime): self
    {
        $this->finishedDatetime = $finishedDatetime;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getAnswerText(): ?string
    {
        return $this->answerText;
    }

    public function setAnswerText(?string $answerText): self
    {
        $this->answerText = $answerText;

        return $this;
    }

    public function getPostDatetime(): ?\DateTimeInterface
    {
        return $this->postDatetime;
    }

    public function setPostDatetime(?\DateTimeInterface $postDatetime): self
    {
        $this->postDatetime = $postDatetime;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getExamPart(): ?ExamParts
    {
        return $this->examPart;
    }

    public function setExamPart(?ExamParts $examPart): self
    {
        $this->examPart = $examPart;

        return $this;
    }

    public function getExamCompletion(): ?ExamCompletions
    {
        return $this->examCompletion;
    }

    public function setExamCompletion(?ExamCompletions $examCompletion): self
    {
        $this->examCompletion = $examCompletion;

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
