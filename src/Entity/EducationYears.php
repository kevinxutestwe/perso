<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EducationYears
 *
 * @ORM\Table(name="education_years", indexes={@ORM\Index(name="IDX_1F84F2D0C32A47EE", columns={"school_id"})})
 * @ORM\Entity
 */
class EducationYears
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolSubjects", inversedBy="educationyear")
     * @ORM\JoinTable(name="education_years_school_subjects",
     *   joinColumns={
     *     @ORM\JoinColumn(name="educationyear_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolsubject_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolsubject;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schoolsubject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|SchoolSubjects[]
     */
    public function getSchoolsubject(): Collection
    {
        return $this->schoolsubject;
    }

    public function addSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if (!$this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject[] = $schoolsubject;
        }

        return $this;
    }

    public function removeSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if ($this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject->removeElement($schoolsubject);
        }

        return $this;
    }

}
