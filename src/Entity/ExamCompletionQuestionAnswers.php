<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamCompletionQuestionAnswers
 *
 * @ORM\Table(name="exam_completion_question_answers", indexes={@ORM\Index(name="IDX_79EE5A534B56D5F0", columns={"completion_question_id"}), @ORM\Index(name="IDX_79EE5A53AA334807", columns={"answer_id"}), @ORM\Index(name="IDX_79EE5A534AF38FD1", columns={"deleted_at"})})
 * @ORM\Entity
 */
class ExamCompletionQuestionAnswers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var ExamCompletionQuestions
     *
     * @ORM\ManyToOne(targetEntity="ExamCompletionQuestions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="completion_question_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $completionQuestion;

    /**
     * @var ExamQuestionAnswers
     *
     * @ORM\ManyToOne(targetEntity="ExamQuestionAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     * })
     */
    private $answer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCompletionQuestion(): ?ExamCompletionQuestions
    {
        return $this->completionQuestion;
    }

    public function setCompletionQuestion(?ExamCompletionQuestions $completionQuestion): self
    {
        $this->completionQuestion = $completionQuestion;

        return $this;
    }

    public function getAnswer(): ?ExamQuestionAnswers
    {
        return $this->answer;
    }

    public function setAnswer(?ExamQuestionAnswers $answer): self
    {
        $this->answer = $answer;

        return $this;
    }


}
