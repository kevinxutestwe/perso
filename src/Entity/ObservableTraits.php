<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ObservableTraits
 *
 * @ORM\Table(name="observable_traits", indexes={@ORM\Index(name="IDX_2A025989C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_2A025989D7BB5C64", columns={"learning_objective_id"})})
 * @ORM\Entity
 */
class ObservableTraits
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="order_value", type="integer", nullable=false)
     */
    private $orderValue;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var LearningObjectives
     *
     * @ORM\ManyToOne(targetEntity="LearningObjectives")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="learning_objective_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $learningObjective;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamParts", mappedBy="observabletrait")
     */
    private $exampart;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamQuestions", mappedBy="observabletrait")
     */
    private $examquestion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exampart = new \Doctrine\Common\Collections\ArrayCollection();
        $this->examquestion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrderValue(): ?int
    {
        return $this->orderValue;
    }

    public function setOrderValue(int $orderValue): self
    {
        $this->orderValue = $orderValue;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getLearningObjective(): ?LearningObjectives
    {
        return $this->learningObjective;
    }

    public function setLearningObjective(?LearningObjectives $learningObjective): self
    {
        $this->learningObjective = $learningObjective;

        return $this;
    }

    /**
     * @return Collection|ExamParts[]
     */
    public function getExampart(): Collection
    {
        return $this->exampart;
    }

    public function addExampart(ExamParts $exampart): self
    {
        if (!$this->exampart->contains($exampart)) {
            $this->exampart[] = $exampart;
            $exampart->addObservabletrait($this);
        }

        return $this;
    }

    public function removeExampart(ExamParts $exampart): self
    {
        if ($this->exampart->contains($exampart)) {
            $this->exampart->removeElement($exampart);
            $exampart->removeObservabletrait($this);
        }

        return $this;
    }

    /**
     * @return Collection|ExamQuestions[]
     */
    public function getExamquestion(): Collection
    {
        return $this->examquestion;
    }

    public function addExamquestion(ExamQuestions $examquestion): self
    {
        if (!$this->examquestion->contains($examquestion)) {
            $this->examquestion[] = $examquestion;
            $examquestion->addObservabletrait($this);
        }

        return $this;
    }

    public function removeExamquestion(ExamQuestions $examquestion): self
    {
        if ($this->examquestion->contains($examquestion)) {
            $this->examquestion->removeElement($examquestion);
            $examquestion->removeObservabletrait($this);
        }

        return $this;
    }

}
