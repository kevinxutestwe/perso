<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamPartGradings
 *
 * @ORM\Table(name="exam_part_gradings", indexes={@ORM\Index(name="IDX_AA1849869409B4DB", columns={"exam_part_completion_id"}), @ORM\Index(name="IDX_AA184986495C6A8C", columns={"exam_grading_id"}), @ORM\Index(name="IDX_AA18498633E6DD38", columns={"is_finished"})})
 * @ORM\Entity
 */
class ExamPartGradings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="grade", type="string", length=10, nullable=true)
     */
    private $grade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer_text_comment", type="text", length=0, nullable=true)
     */
    private $answerTextComment;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_finished", type="boolean", nullable=false)
     */
    private $isFinished;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var ExamGradings
     *
     * @ORM\ManyToOne(targetEntity="ExamGradings")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_grading_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examGrading;

    /**
     * @var ExamPartCompletions
     *
     * @ORM\ManyToOne(targetEntity="ExamPartCompletions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_part_completion_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examPartCompletion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(?string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAnswerTextComment(): ?string
    {
        return $this->answerTextComment;
    }

    public function setAnswerTextComment(?string $answerTextComment): self
    {
        $this->answerTextComment = $answerTextComment;

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getExamGrading(): ?ExamGradings
    {
        return $this->examGrading;
    }

    public function setExamGrading(?ExamGradings $examGrading): self
    {
        $this->examGrading = $examGrading;

        return $this;
    }

    public function getExamPartCompletion(): ?ExamPartCompletions
    {
        return $this->examPartCompletion;
    }

    public function setExamPartCompletion(?ExamPartCompletions $examPartCompletion): self
    {
        $this->examPartCompletion = $examPartCompletion;

        return $this;
    }


}
