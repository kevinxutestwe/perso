<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LearningObjectives
 *
 * @ORM\Table(name="learning_objectives", indexes={@ORM\Index(name="IDX_BF9CB6DAC32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_BF9CB6DAA2DAA20D", columns={"learning_goal_id"})})
 * @ORM\Entity
 */
class LearningObjectives
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="order_value", type="integer", nullable=false)
     */
    private $orderValue;

    /**
     * @var LearningGoals
     *
     * @ORM\ManyToOne(targetEntity="LearningGoals")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="learning_goal_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $learningGoal;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamParts", mappedBy="learningobjective")
     */
    private $exampart;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamQuestions", mappedBy="learningobjective")
     */
    private $examquestion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolSubjects", mappedBy="learningobjective")
     */
    private $schoolsubject;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exampart = new \Doctrine\Common\Collections\ArrayCollection();
        $this->examquestion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolsubject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrderValue(): ?int
    {
        return $this->orderValue;
    }

    public function setOrderValue(int $orderValue): self
    {
        $this->orderValue = $orderValue;

        return $this;
    }

    public function getLearningGoal(): ?LearningGoals
    {
        return $this->learningGoal;
    }

    public function setLearningGoal(?LearningGoals $learningGoal): self
    {
        $this->learningGoal = $learningGoal;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|ExamParts[]
     */
    public function getExampart(): Collection
    {
        return $this->exampart;
    }

    public function addExampart(ExamParts $exampart): self
    {
        if (!$this->exampart->contains($exampart)) {
            $this->exampart[] = $exampart;
            $exampart->addLearningobjective($this);
        }

        return $this;
    }

    public function removeExampart(ExamParts $exampart): self
    {
        if ($this->exampart->contains($exampart)) {
            $this->exampart->removeElement($exampart);
            $exampart->removeLearningobjective($this);
        }

        return $this;
    }

    /**
     * @return Collection|ExamQuestions[]
     */
    public function getExamquestion(): Collection
    {
        return $this->examquestion;
    }

    public function addExamquestion(ExamQuestions $examquestion): self
    {
        if (!$this->examquestion->contains($examquestion)) {
            $this->examquestion[] = $examquestion;
            $examquestion->addLearningobjective($this);
        }

        return $this;
    }

    public function removeExamquestion(ExamQuestions $examquestion): self
    {
        if ($this->examquestion->contains($examquestion)) {
            $this->examquestion->removeElement($examquestion);
            $examquestion->removeLearningobjective($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolSubjects[]
     */
    public function getSchoolsubject(): Collection
    {
        return $this->schoolsubject;
    }

    public function addSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if (!$this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject[] = $schoolsubject;
            $schoolsubject->addLearningobjective($this);
        }

        return $this;
    }

    public function removeSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if ($this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject->removeElement($schoolsubject);
            $schoolsubject->removeLearningobjective($this);
        }

        return $this;
    }

}
