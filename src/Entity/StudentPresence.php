<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentPresence
 *
 * @ORM\Table(name="student_presence", indexes={@ORM\Index(name="IDX_DAFC8B3ECB944F1A", columns={"student_id"}), @ORM\Index(name="IDX_DAFC8B3E578D5E91", columns={"exam_id"})})
 * @ORM\Entity
 */
class StudentPresence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="absent", type="boolean", nullable=true)
     */
    private $absent;

    /**
     * @var bool
     *
     * @ORM\Column(name="paperCopy", type="boolean", nullable=false)
     */
    private $papercopy;

    /**
     * @var Exams
     *
     * @ORM\ManyToOne(targetEntity="Exams")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $exam;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getAbsent(): ?bool
    {
        return $this->absent;
    }

    public function setAbsent(?bool $absent): self
    {
        $this->absent = $absent;

        return $this;
    }

    public function getPapercopy(): ?bool
    {
        return $this->papercopy;
    }

    public function setPapercopy(bool $papercopy): self
    {
        $this->papercopy = $papercopy;

        return $this;
    }

    public function getExam(): ?Exams
    {
        return $this->exam;
    }

    public function setExam(?Exams $exam): self
    {
        $this->exam = $exam;

        return $this;
    }

    public function getStudent(): ?Users
    {
        return $this->student;
    }

    public function setStudent(?Users $student): self
    {
        $this->student = $student;

        return $this;
    }


}
