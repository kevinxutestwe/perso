<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SchoolParameters
 *
 * @ORM\Table(name="school_parameters", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_B332C80CC32A47EE", columns={"school_id"})})
 * @ORM\Entity
 */
class SchoolParameters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="allowed_tabs", type="array", length=0, nullable=false)
     */
    private $allowedTabs;

    /**
     * @var array
     *
     * @ORM\Column(name="tab_names", type="array", length=0, nullable=false)
     */
    private $tabNames;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     * })
     */
    private $school;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAllowedTabs(): ?array
    {
        return $this->allowedTabs;
    }

    public function setAllowedTabs(array $allowedTabs): self
    {
        $this->allowedTabs = $allowedTabs;

        return $this;
    }

    public function getTabNames(): ?array
    {
        return $this->tabNames;
    }

    public function setTabNames(array $tabNames): self
    {
        $this->tabNames = $tabNames;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }


}
