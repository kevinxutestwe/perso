<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamQuestionGradings
 *
 * @ORM\Table(name="exam_question_gradings", indexes={@ORM\Index(name="IDX_FE0AB5901E27F6BF", columns={"question_id"}), @ORM\Index(name="IDX_FE0AB590ECB7B435", columns={"exam_part_grading_id"})})
 * @ORM\Entity
 */
class ExamQuestionGradings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="grade", type="string", length=10, nullable=true)
     */
    private $grade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var ExamQuestions
     *
     * @ORM\ManyToOne(targetEntity="ExamQuestions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $question;

    /**
     * @var ExamPartGradings
     *
     * @ORM\ManyToOne(targetEntity="ExamPartGradings")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_part_grading_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examPartGrading;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(?string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getQuestion(): ?ExamQuestions
    {
        return $this->question;
    }

    public function setQuestion(?ExamQuestions $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getExamPartGrading(): ?ExamPartGradings
    {
        return $this->examPartGrading;
    }

    public function setExamPartGrading(?ExamPartGradings $examPartGrading): self
    {
        $this->examPartGrading = $examPartGrading;

        return $this;
    }


}
