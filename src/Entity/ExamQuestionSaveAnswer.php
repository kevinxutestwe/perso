<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamQuestionSaveAnswer
 *
 * @ORM\Table(name="exam_question_save_answer", indexes={@ORM\Index(name="IDX_6D42C19059A05DE0", columns={"examCompletion_id"}), @ORM\Index(name="IDX_6D42C190E9F8F345", columns={"examQuestion_id"})})
 * @ORM\Entity
 */
class ExamQuestionSaveAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var float|null
     *
     * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $grade;

    /**
     * @var ExamCompletions
     *
     * @ORM\ManyToOne(targetEntity="ExamCompletions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examCompletion_id", referencedColumnName="id")
     * })
     */
    private $examcompletion;

    /**
     * @var ExamQuestions
     *
     * @ORM\ManyToOne(targetEntity="ExamQuestions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examQuestion_id", referencedColumnName="id")
     * })
     */
    private $examquestion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getGrade(): ?float
    {
        return $this->grade;
    }

    public function setGrade(?float $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getExamcompletion(): ?ExamCompletions
    {
        return $this->examcompletion;
    }

    public function setExamcompletion(?ExamCompletions $examcompletion): self
    {
        $this->examcompletion = $examcompletion;

        return $this;
    }

    public function getExamquestion(): ?ExamQuestions
    {
        return $this->examquestion;
    }

    public function setExamquestion(?ExamQuestions $examquestion): self
    {
        $this->examquestion = $examquestion;

        return $this;
    }


}
