<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentExamDownloads
 *
 * @ORM\Table(name="student_exam_downloads", indexes={@ORM\Index(name="IDX_E3028357CB944F1A", columns={"student_id"}), @ORM\Index(name="IDX_E3028357578D5E91", columns={"exam_id"})})
 * @ORM\Entity
 */
class StudentExamDownloads
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var Exams
     *
     * @ORM\ManyToOne(targetEntity="Exams")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $exam;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getExam(): ?Exams
    {
        return $this->exam;
    }

    public function setExam(?Exams $exam): self
    {
        $this->exam = $exam;

        return $this;
    }

    public function getStudent(): ?Users
    {
        return $this->student;
    }

    public function setStudent(?Users $student): self
    {
        $this->student = $student;

        return $this;
    }


}
