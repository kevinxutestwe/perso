<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ExamQuestions
 *
 * @ORM\Table(name="exam_questions", indexes={@ORM\Index(name="IDX_354F518C41451284", columns={"exam_part_id"}), @ORM\Index(name="IDX_354F518C4AF38FD1", columns={"deleted_at"}), @ORM\Index(name="IDX_354F518C9EE92CC9", columns={"order_value"})})
 * @ORM\Entity
 */
class ExamQuestions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=0, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_commentable", type="boolean", nullable=false)
     */
    private $isCommentable;

    /**
     * @var int|null
     *
     * @ORM\Column(name="comment_max_words", type="integer", nullable=true)
     */
    private $commentMaxWords;

    /**
     * @var int|null
     *
     * @ORM\Column(name="comment_max_characters", type="integer", nullable=true)
     */
    private $commentMaxCharacters;

    /**
     * @var int|null
     *
     * @ORM\Column(name="order_value", type="integer", nullable=true)
     */
    private $orderValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="max_grade", type="string", length=255, nullable=true)
     */
    private $maxGrade;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_knowledge", type="boolean", nullable=false)
     */
    private $isKnowledge;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_skill", type="boolean", nullable=false)
     */
    private $isSkill;

    /**
     * @var float
     *
     * @ORM\Column(name="other_points", type="float", nullable=false, options={"default":0})
     */
    private $otherPoints;

    /**
     * @var ExamParts
     *
     * @ORM\ManyToOne(targetEntity="ExamParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_part_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examPart;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LearningObjectives", inversedBy="examquestion")
     * @ORM\JoinTable(name="exam_questions_learning_objectives",
     *   joinColumns={
     *     @ORM\JoinColumn(name="examquestion_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="learningobjective_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $learningobjective;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Medias", inversedBy="examquestion")
     * @ORM\JoinTable(name="exam_questions_medias",
     *   joinColumns={
     *     @ORM\JoinColumn(name="examquestion_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $media;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ObservableTraits", inversedBy="examquestion")
     * @ORM\JoinTable(name="exam_questions_observable_traits",
     *   joinColumns={
     *     @ORM\JoinColumn(name="examquestion_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="observabletrait_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $observabletrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->learningobjective = new \Doctrine\Common\Collections\ArrayCollection();
        $this->media = new \Doctrine\Common\Collections\ArrayCollection();
        $this->observabletrait = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsCommentable(): ?bool
    {
        return $this->isCommentable;
    }

    public function setIsCommentable(bool $isCommentable): self
    {
        $this->isCommentable = $isCommentable;

        return $this;
    }

    public function getCommentMaxWords(): ?int
    {
        return $this->commentMaxWords;
    }

    public function setCommentMaxWords(?int $commentMaxWords): self
    {
        $this->commentMaxWords = $commentMaxWords;

        return $this;
    }

    public function getCommentMaxCharacters(): ?int
    {
        return $this->commentMaxCharacters;
    }

    public function setCommentMaxCharacters(?int $commentMaxCharacters): self
    {
        $this->commentMaxCharacters = $commentMaxCharacters;

        return $this;
    }

    public function getOrderValue(): ?int
    {
        return $this->orderValue;
    }

    public function setOrderValue(?int $orderValue): self
    {
        $this->orderValue = $orderValue;

        return $this;
    }

    public function getMaxGrade(): ?string
    {
        return $this->maxGrade;
    }

    public function setMaxGrade(?string $maxGrade): self
    {
        $this->maxGrade = $maxGrade;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsKnowledge(): ?bool
    {
        return $this->isKnowledge;
    }

    public function setIsKnowledge(bool $isKnowledge): self
    {
        $this->isKnowledge = $isKnowledge;

        return $this;
    }

    public function getIsSkill(): ?bool
    {
        return $this->isSkill;
    }

    public function setIsSkill(bool $isSkill): self
    {
        $this->isSkill = $isSkill;

        return $this;
    }

    public function getOtherPoints(): ?float
    {
        return $this->otherPoints;
    }

    public function setOtherPoints(float $otherPoints): self
    {
        $this->otherPoints = $otherPoints;

        return $this;
    }

    public function getExamPart(): ?ExamParts
    {
        return $this->examPart;
    }

    public function setExamPart(?ExamParts $examPart): self
    {
        $this->examPart = $examPart;

        return $this;
    }

    /**
     * @return Collection|LearningObjectives[]
     */
    public function getLearningobjective(): Collection
    {
        return $this->learningobjective;
    }

    public function addLearningobjective(LearningObjectives $learningobjective): self
    {
        if (!$this->learningobjective->contains($learningobjective)) {
            $this->learningobjective[] = $learningobjective;
        }

        return $this;
    }

    public function removeLearningobjective(LearningObjectives $learningobjective): self
    {
        if ($this->learningobjective->contains($learningobjective)) {
            $this->learningobjective->removeElement($learningobjective);
        }

        return $this;
    }

    /**
     * @return Collection|Medias[]
     */
    public function getMedia(): Collection
    {
        return $this->media;
    }

    public function addMedium(Medias $medium): self
    {
        if (!$this->media->contains($medium)) {
            $this->media[] = $medium;
        }

        return $this;
    }

    public function removeMedium(Medias $medium): self
    {
        if ($this->media->contains($medium)) {
            $this->media->removeElement($medium);
        }

        return $this;
    }

    /**
     * @return Collection|ObservableTraits[]
     */
    public function getObservabletrait(): Collection
    {
        return $this->observabletrait;
    }

    public function addObservabletrait(ObservableTraits $observabletrait): self
    {
        if (!$this->observabletrait->contains($observabletrait)) {
            $this->observabletrait[] = $observabletrait;
        }

        return $this;
    }

    public function removeObservabletrait(ObservableTraits $observabletrait): self
    {
        if ($this->observabletrait->contains($observabletrait)) {
            $this->observabletrait->removeElement($observabletrait);
        }

        return $this;
    }

}
