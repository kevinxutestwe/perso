<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SchoolSubjects
 *
 * @ORM\Table(name="school_subjects", indexes={@ORM\Index(name="IDX_5563AD53C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_5563AD535E237E06", columns={"name"})})
 * @ORM\Entity
 */
class SchoolSubjects
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var int
     *
     * @ORM\Column(name="id_by_school", type="integer", nullable=false)
     */
    private $idBySchool;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="EducationYears", mappedBy="schoolsubject")
     */
    private $educationyear;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LearningGoals", inversedBy="schoolsubject")
     * @ORM\JoinTable(name="school_subjects_learning_goals",
     *   joinColumns={
     *     @ORM\JoinColumn(name="schoolsubject_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="learninggoal_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $learninggoal;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LearningObjectives", inversedBy="schoolsubject")
     * @ORM\JoinTable(name="school_subjects_learning_objectives",
     *   joinColumns={
     *     @ORM\JoinColumn(name="schoolsubject_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="learningobjective_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $learningobjective;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolClasses", inversedBy="schoolsubject")
     * @ORM\JoinTable(name="subjects_school_classes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="schoolsubject_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolclass_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolclass;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", mappedBy="schoolsubject")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->educationyear = new \Doctrine\Common\Collections\ArrayCollection();
        $this->learninggoal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->learningobjective = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolclass = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getIdBySchool(): ?int
    {
        return $this->idBySchool;
    }

    public function setIdBySchool(int $idBySchool): self
    {
        $this->idBySchool = $idBySchool;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|EducationYears[]
     */
    public function getEducationyear(): Collection
    {
        return $this->educationyear;
    }

    public function addEducationyear(EducationYears $educationyear): self
    {
        if (!$this->educationyear->contains($educationyear)) {
            $this->educationyear[] = $educationyear;
            $educationyear->addSchoolsubject($this);
        }

        return $this;
    }

    public function removeEducationyear(EducationYears $educationyear): self
    {
        if ($this->educationyear->contains($educationyear)) {
            $this->educationyear->removeElement($educationyear);
            $educationyear->removeSchoolsubject($this);
        }

        return $this;
    }

    /**
     * @return Collection|LearningGoals[]
     */
    public function getLearninggoal(): Collection
    {
        return $this->learninggoal;
    }

    public function addLearninggoal(LearningGoals $learninggoal): self
    {
        if (!$this->learninggoal->contains($learninggoal)) {
            $this->learninggoal[] = $learninggoal;
        }

        return $this;
    }

    public function removeLearninggoal(LearningGoals $learninggoal): self
    {
        if ($this->learninggoal->contains($learninggoal)) {
            $this->learninggoal->removeElement($learninggoal);
        }

        return $this;
    }

    /**
     * @return Collection|LearningObjectives[]
     */
    public function getLearningobjective(): Collection
    {
        return $this->learningobjective;
    }

    public function addLearningobjective(LearningObjectives $learningobjective): self
    {
        if (!$this->learningobjective->contains($learningobjective)) {
            $this->learningobjective[] = $learningobjective;
        }

        return $this;
    }

    public function removeLearningobjective(LearningObjectives $learningobjective): self
    {
        if ($this->learningobjective->contains($learningobjective)) {
            $this->learningobjective->removeElement($learningobjective);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolClasses[]
     */
    public function getSchoolclass(): Collection
    {
        return $this->schoolclass;
    }

    public function addSchoolclass(SchoolClasses $schoolclass): self
    {
        if (!$this->schoolclass->contains($schoolclass)) {
            $this->schoolclass[] = $schoolclass;
        }

        return $this;
    }

    public function removeSchoolclass(SchoolClasses $schoolclass): self
    {
        if ($this->schoolclass->contains($schoolclass)) {
            $this->schoolclass->removeElement($schoolclass);
        }

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addSchoolsubject($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeSchoolsubject($this);
        }

        return $this;
    }

}
