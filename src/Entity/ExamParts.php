<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ExamParts
 *
 * @ORM\Table(name="exam_parts", indexes={@ORM\Index(name="IDX_3BA5A14E1A663BB4", columns={"exam_theme_id"}), @ORM\Index(name="IDX_3BA5A14E4AF38FD1", columns={"deleted_at"}), @ORM\Index(name="IDX_3BA5A14E9EE92CC9", columns={"order_value"}), @ORM\Index(name="IDX_3BA5A14EF361A07C", columns={"is_validated"})})
 * @ORM\Entity
 */
class ExamParts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="order_value", type="integer", nullable=false)
     */
    private $orderValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="examiners_order", type="string", length=200, nullable=true)
     */
    private $examinersOrder;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options", type="simple_array", length=0, nullable=true)
     */
    private $options;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_families", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontFamilies;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_colors", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontColors;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_sizes", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontSizes;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_validated", type="boolean", nullable=false)
     */
    private $isValidated;

    /**
     * @var bool
     *
     * @ORM\Column(name="quiz_personalized_grading", type="boolean", nullable=false)
     */
    private $quizPersonalizedGrading;

    /**
     * @var bool
     *
     * @ORM\Column(name="quiz_personalized_duration", type="boolean", nullable=false)
     */
    private $quizPersonalizedDuration;

    /**
     * @var float|null
     *
     * @ORM\Column(name="quiz_max_grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $quizMaxGrade;

    /**
     * @var bool
     *
     * @ORM\Column(name="quiz_negative_grading", type="boolean", nullable=false)
     */
    private $quizNegativeGrading;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="quiz_partial_ok", type="boolean", nullable=false)
     */
    private $quizPartialOk;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quiz_take_points_on_empty", type="string", length=10, nullable=true)
     */
    private $quizTakePointsOnEmpty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quiz_take_points_on_error", type="string", length=10, nullable=true)
     */
    private $quizTakePointsOnError;

    /**
     * @var bool
     *
     * @ORM\Column(name="automatically_graded", type="boolean", nullable=false)
     */
    private $automaticallyGraded;

    /**
     * @var ExamThemes
     *
     * @ORM\ManyToOne(targetEntity="ExamThemes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_theme_id", referencedColumnName="id")
     * })
     */
    private $examTheme;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", inversedBy="exampart")
     * @ORM\JoinTable(name="exam_parts_examiners",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exampart_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LearningObjectives", inversedBy="exampart")
     * @ORM\JoinTable(name="exam_parts_learning_objectives",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exampart_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="learningobjective_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $learningobjective;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ObservableTraits", inversedBy="exampart")
     * @ORM\JoinTable(name="exam_parts_observable_traits",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exampart_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="observabletrait_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $observabletrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->learningobjective = new \Doctrine\Common\Collections\ArrayCollection();
        $this->observabletrait = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrderValue(): ?int
    {
        return $this->orderValue;
    }

    public function setOrderValue(int $orderValue): self
    {
        $this->orderValue = $orderValue;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getExaminersOrder(): ?string
    {
        return $this->examinersOrder;
    }

    public function setExaminersOrder(?string $examinersOrder): self
    {
        $this->examinersOrder = $examinersOrder;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getOptionsFontFamilies(): ?array
    {
        return $this->optionsFontFamilies;
    }

    public function setOptionsFontFamilies(?array $optionsFontFamilies): self
    {
        $this->optionsFontFamilies = $optionsFontFamilies;

        return $this;
    }

    public function getOptionsFontColors(): ?array
    {
        return $this->optionsFontColors;
    }

    public function setOptionsFontColors(?array $optionsFontColors): self
    {
        $this->optionsFontColors = $optionsFontColors;

        return $this;
    }

    public function getOptionsFontSizes(): ?array
    {
        return $this->optionsFontSizes;
    }

    public function setOptionsFontSizes(?array $optionsFontSizes): self
    {
        $this->optionsFontSizes = $optionsFontSizes;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getQuizPersonalizedGrading(): ?bool
    {
        return $this->quizPersonalizedGrading;
    }

    public function setQuizPersonalizedGrading(bool $quizPersonalizedGrading): self
    {
        $this->quizPersonalizedGrading = $quizPersonalizedGrading;

        return $this;
    }

    public function getQuizPersonalizedDuration(): ?bool
    {
        return $this->quizPersonalizedDuration;
    }

    public function setQuizPersonalizedDuration(bool $quizPersonalizedDuration): self
    {
        $this->quizPersonalizedDuration = $quizPersonalizedDuration;

        return $this;
    }

    public function getQuizMaxGrade(): ?float
    {
        return $this->quizMaxGrade;
    }

    public function setQuizMaxGrade(?float $quizMaxGrade): self
    {
        $this->quizMaxGrade = $quizMaxGrade;

        return $this;
    }

    public function getQuizNegativeGrading(): ?bool
    {
        return $this->quizNegativeGrading;
    }

    public function setQuizNegativeGrading(bool $quizNegativeGrading): self
    {
        $this->quizNegativeGrading = $quizNegativeGrading;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getQuizPartialOk(): ?bool
    {
        return $this->quizPartialOk;
    }

    public function setQuizPartialOk(bool $quizPartialOk): self
    {
        $this->quizPartialOk = $quizPartialOk;

        return $this;
    }

    public function getQuizTakePointsOnEmpty(): ?string
    {
        return $this->quizTakePointsOnEmpty;
    }

    public function setQuizTakePointsOnEmpty(?string $quizTakePointsOnEmpty): self
    {
        $this->quizTakePointsOnEmpty = $quizTakePointsOnEmpty;

        return $this;
    }

    public function getQuizTakePointsOnError(): ?string
    {
        return $this->quizTakePointsOnError;
    }

    public function setQuizTakePointsOnError(?string $quizTakePointsOnError): self
    {
        $this->quizTakePointsOnError = $quizTakePointsOnError;

        return $this;
    }

    public function getAutomaticallyGraded(): ?bool
    {
        return $this->automaticallyGraded;
    }

    public function setAutomaticallyGraded(bool $automaticallyGraded): self
    {
        $this->automaticallyGraded = $automaticallyGraded;

        return $this;
    }

    public function getExamTheme(): ?ExamThemes
    {
        return $this->examTheme;
    }

    public function setExamTheme(?ExamThemes $examTheme): self
    {
        $this->examTheme = $examTheme;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|LearningObjectives[]
     */
    public function getLearningobjective(): Collection
    {
        return $this->learningobjective;
    }

    public function addLearningobjective(LearningObjectives $learningobjective): self
    {
        if (!$this->learningobjective->contains($learningobjective)) {
            $this->learningobjective[] = $learningobjective;
        }

        return $this;
    }

    public function removeLearningobjective(LearningObjectives $learningobjective): self
    {
        if ($this->learningobjective->contains($learningobjective)) {
            $this->learningobjective->removeElement($learningobjective);
        }

        return $this;
    }

    /**
     * @return Collection|ObservableTraits[]
     */
    public function getObservabletrait(): Collection
    {
        return $this->observabletrait;
    }

    public function addObservabletrait(ObservableTraits $observabletrait): self
    {
        if (!$this->observabletrait->contains($observabletrait)) {
            $this->observabletrait[] = $observabletrait;
        }

        return $this;
    }

    public function removeObservabletrait(ObservableTraits $observabletrait): self
    {
        if ($this->observabletrait->contains($observabletrait)) {
            $this->observabletrait->removeElement($observabletrait);
        }

        return $this;
    }

}
