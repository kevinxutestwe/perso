<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Medias
 *
 * @ORM\Table(name="medias", indexes={@ORM\Index(name="IDX_12D2AF81C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_12D2AF818CDE5729", columns={"type"}), @ORM\Index(name="IDX_12D2AF814AF38FD1", columns={"deleted_at"})})
 * @ORM\Entity
 */
class Medias
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="upload_path", type="string", length=255, nullable=false)
     */
    private $uploadPath;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=false)
     */
    private $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="real_file_name", type="string", length=255, nullable=true)
     */
    private $realFileName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamQuestions", mappedBy="media")
     */
    private $examquestion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->examquestion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUploadPath(): ?string
    {
        return $this->uploadPath;
    }

    public function setUploadPath(string $uploadPath): self
    {
        $this->uploadPath = $uploadPath;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRealFileName(): ?string
    {
        return $this->realFileName;
    }

    public function setRealFileName(?string $realFileName): self
    {
        $this->realFileName = $realFileName;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|ExamQuestions[]
     */
    public function getExamquestion(): Collection
    {
        return $this->examquestion;
    }

    public function addExamquestion(ExamQuestions $examquestion): self
    {
        if (!$this->examquestion->contains($examquestion)) {
            $this->examquestion[] = $examquestion;
            $examquestion->addMedium($this);
        }

        return $this;
    }

    public function removeExamquestion(ExamQuestions $examquestion): self
    {
        if ($this->examquestion->contains($examquestion)) {
            $this->examquestion->removeElement($examquestion);
            $examquestion->removeMedium($this);
        }

        return $this;
    }

}
