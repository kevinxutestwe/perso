<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamGradings
 *
 * @ORM\Table(name="exam_gradings", indexes={@ORM\Index(name="IDX_CF5276CD76E5A280", columns={"exam_completion_id"}), @ORM\Index(name="IDX_CF5276CD8A588563", columns={"examiner_id"})})
 * @ORM\Entity
 */
class ExamGradings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="absent", type="boolean", nullable=true)
     */
    private $absent;

    /**
     * @var ExamCompletions
     *
     * @ORM\ManyToOne(targetEntity="ExamCompletions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_completion_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examCompletion;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examiner_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $examiner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAbsent(): ?bool
    {
        return $this->absent;
    }

    public function setAbsent(?bool $absent): self
    {
        $this->absent = $absent;

        return $this;
    }

    public function getExamCompletion(): ?ExamCompletions
    {
        return $this->examCompletion;
    }

    public function setExamCompletion(?ExamCompletions $examCompletion): self
    {
        $this->examCompletion = $examCompletion;

        return $this;
    }

    public function getExaminer(): ?Users
    {
        return $this->examiner;
    }

    public function setExaminer(?Users $examiner): self
    {
        $this->examiner = $examiner;

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
