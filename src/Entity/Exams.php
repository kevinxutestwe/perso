<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Exams
 *
 * @ORM\Table(name="exams", indexes={@ORM\Index(name="IDX_69311328C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_6931132823EDC87", columns={"subject_id"}), @ORM\Index(name="IDX_6931132812469DE2", columns={"category_id"}), @ORM\Index(name="IDX_6931132840C1FEA7", columns={"year_id"}), @ORM\Index(name="IDX_69311328F675F31B", columns={"author_id"}), @ORM\Index(name="IDX_69311328E252B6A5", columns={"medium_id"}), @ORM\Index(name="IDX_693113285E237E06", columns={"name"}), @ORM\Index(name="IDX_6931132811C65AE7", columns={"planned_date"}), @ORM\Index(name="IDX_693113284AF38FD1", columns={"deleted_at"}), @ORM\Index(name="IDX_69311328F361A07C", columns={"is_validated"})})
 * @ORM\Entity(repositoryClass="App\Repository\ExamsRepository")
 */
class Exams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="planned_date", type="date", nullable=true)
     */
    private $plannedDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="planned_hour", type="string", length=10, nullable=true)
     */
    private $plannedHour;

    /**
     * @var bool
     *
     * @ORM\Column(name="anonymous_grading", type="boolean", nullable=false)
     */
    private $anonymousGrading;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options", type="simple_array", length=0, nullable=true)
     */
    private $options;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_families", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontFamilies;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_colors", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontColors;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options_font_sizes", type="simple_array", length=0, nullable=true)
     */
    private $optionsFontSizes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="public_password", type="string", length=30, nullable=true)
     */
    private $publicPassword;

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin_password", type="string", length=30, nullable=true)
     */
    private $adminPassword;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_validated", type="boolean", nullable=false)
     */
    private $isValidated;

    /**
     * @var bool
     *
     * @ORM\Column(name="emails_sent", type="boolean", nullable=false)
     */
    private $emailsSent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="examiners_order", type="string", length=200, nullable=true)
     */
    private $examinersOrder;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var bool
     *
     * @ORM\Column(name="part_personalized_duration", type="boolean", nullable=false)
     */
    private $partPersonalizedDuration;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_validate", type="boolean", nullable=false)
     */
    private $examValidate;

    /**
     * @var int
     *
     * @ORM\Column(name="id_by_school", type="integer", nullable=false)
     */
    private $idBySchool;

    /**
     * @var bool
     *
     * @ORM\Column(name="extra_time", type="boolean", nullable=false)
     */
    private $extraTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="exam_available_at", type="datetime", nullable=true)
     */
    private $examAvailableAt;

    /**
     * @var int
     *
     * @ORM\Column(name="mail_counter", type="integer", nullable=false)
     */
    private $mailCounter;

    /**
     * @var SchoolExamCategories
     *
     * @ORM\ManyToOne(targetEntity="SchoolExamCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var SchoolSubjects
     *
     * @ORM\ManyToOne(targetEntity="SchoolSubjects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subject_id", referencedColumnName="id")
     * })
     */
    private $subject;

    /**
     * @var SchoolYears
     *
     * @ORM\ManyToOne(targetEntity="SchoolYears")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="year_id", referencedColumnName="id")
     * })
     */
    private $year;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     * })
     */
    private $school;

    /**
     * @var ExamMediums
     *
     * @ORM\ManyToOne(targetEntity="ExamMediums")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medium_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $medium;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", inversedBy="exam")
     * @ORM\JoinTable(name="exams_examiners",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exam_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", inversedBy="examProf")
     * @ORM\JoinTable(name="exams_professors",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exam_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $userProf;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolClasses", inversedBy="exam")
     * @ORM\JoinTable(name="exams_school_classes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="exam_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolclass_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolclass;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userProf = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolclass = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPlannedDate(): ?string
    {
        if ($this->plannedDate == NULL) return NULL;
        return $this->plannedDate->format('Y-m-d');
    }

    public function setPlannedDate(?\DateTimeInterface $plannedDate): self
    {
        $this->plannedDate = $plannedDate;
        return $this;
    }

    public function getPlannedHour(): ?string
    {
        return $this->plannedHour;
    }

    public function setPlannedHour(?string $plannedHour): self
    {
        $this->plannedHour = $plannedHour;

        return $this;
    }

    public function getAnonymousGrading(): ?bool
    {
        return $this->anonymousGrading;
    }

    public function setAnonymousGrading(bool $anonymousGrading): self
    {
        $this->anonymousGrading = $anonymousGrading;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getOptionsFontFamilies(): ?array
    {
        return $this->optionsFontFamilies;
    }

    public function setOptionsFontFamilies(?array $optionsFontFamilies): self
    {
        $this->optionsFontFamilies = $optionsFontFamilies;

        return $this;
    }

    public function getOptionsFontColors(): ?array
    {
        return $this->optionsFontColors;
    }

    public function setOptionsFontColors(?array $optionsFontColors): self
    {
        $this->optionsFontColors = $optionsFontColors;

        return $this;
    }

    public function getOptionsFontSizes(): ?array
    {
        return $this->optionsFontSizes;
    }

    public function setOptionsFontSizes(?array $optionsFontSizes): self
    {
        $this->optionsFontSizes = $optionsFontSizes;

        return $this;
    }

    public function getPublicPassword(): ?string
    {
        return $this->publicPassword;
    }

    public function setPublicPassword(?string $publicPassword): self
    {
        $this->publicPassword = $publicPassword;

        return $this;
    }

    public function getAdminPassword(): ?string
    {
        return $this->adminPassword;
    }

    public function setAdminPassword(?string $adminPassword): self
    {
        $this->adminPassword = $adminPassword;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getEmailsSent(): ?bool
    {
        return $this->emailsSent;
    }

    public function setEmailsSent(bool $emailsSent): self
    {
        $this->emailsSent = $emailsSent;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getExaminersOrder(): ?string
    {
        return $this->examinersOrder;
    }

    public function setExaminersOrder(?string $examinersOrder): self
    {
        $this->examinersOrder = $examinersOrder;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPartPersonalizedDuration(): ?bool
    {
        return $this->partPersonalizedDuration;
    }

    public function setPartPersonalizedDuration(bool $partPersonalizedDuration): self
    {
        $this->partPersonalizedDuration = $partPersonalizedDuration;

        return $this;
    }

    public function getExamValidate(): ?bool
    {
        return $this->examValidate;
    }

    public function setExamValidate(bool $examValidate): self
    {
        $this->examValidate = $examValidate;

        return $this;
    }

    public function getIdBySchool(): ?int
    {
        return $this->idBySchool;
    }

    public function setIdBySchool(int $idBySchool): self
    {
        $this->idBySchool = $idBySchool;

        return $this;
    }

    public function getExtraTime(): ?bool
    {
        return $this->extraTime;
    }

    public function setExtraTime(bool $extraTime): self
    {
        $this->extraTime = $extraTime;

        return $this;
    }

    public function getExamAvailableAt(): ?\DateTimeInterface
    {
        return $this->examAvailableAt;
    }

    public function setExamAvailableAt(?\DateTimeInterface $examAvailableAt): self
    {
        $this->examAvailableAt = $examAvailableAt;

        return $this;
    }

    public function getMailCounter(): ?int
    {
        return $this->mailCounter;
    }

    public function setMailCounter(int $mailCounter): self
    {
        $this->mailCounter = $mailCounter;

        return $this;
    }

    public function getCategory(): ?SchoolExamCategories
    {
        return $this->category;
    }

    public function setCategory(?SchoolExamCategories $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSubject(): ?SchoolSubjects
    {
        return $this->subject;
    }

    public function setSubject(?SchoolSubjects $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getYear(): ?SchoolYears
    {
        return $this->year;
    }

    public function setYear(?SchoolYears $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getMedium(): ?ExamMediums
    {
        return $this->medium;
    }

    public function setMedium(?ExamMediums $medium): self
    {
        $this->medium = $medium;

        return $this;
    }

    public function getAuthor(): ?Users
    {
        return $this->author;
    }

    public function setAuthor(?Users $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUserProf(): Collection
    {
        return $this->userProf;
    }

    public function addUserProf(Users $userProf): self
    {
        if (!$this->userProf->contains($userProf)) {
            $this->userProf[] = $userProf;
        }

        return $this;
    }

    public function removeUserProf(Users $userProf): self
    {
        if ($this->userProf->contains($userProf)) {
            $this->userProf->removeElement($userProf);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolClasses[]
     */
    public function getSchoolclass(): Collection
    {
        return $this->schoolclass;
    }

    public function addSchoolclass(SchoolClasses $schoolclass): self
    {
        if (!$this->schoolclass->contains($schoolclass)) {
            $this->schoolclass[] = $schoolclass;
        }

        return $this;
    }

    public function removeSchoolclass(SchoolClasses $schoolclass): self
    {
        if ($this->schoolclass->contains($schoolclass)) {
            $this->schoolclass->removeElement($schoolclass);
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
