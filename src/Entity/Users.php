<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_1483A5E992FC23A8", columns={"username_canonical"}), @ORM\UniqueConstraint(name="UNIQ_1483A5E9A0D96FBF", columns={"email_canonical"})}, indexes={@ORM\Index(name="IDX_1483A5E9C32A47EE", columns={"school_id"}), @ORM\Index(name="IDX_1483A5E9F85E0677", columns={"username"}), @ORM\Index(name="IDX_1483A5E9E7927C74", columns={"email"}), @ORM\Index(name="IDX_1483A5E94AD26064", columns={"discr"})})
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="username_canonical", type="string", length=255, nullable=false)
     */
    private $usernameCanonical;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_canonical", type="string", length=255, nullable=false)
     */
    private $emailCanonical;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked;

    /**
     * @var bool
     *
     * @ORM\Column(name="expired", type="boolean", nullable=false)
     */
    private $expired;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="confirmation_token", type="string", length=255, nullable=true)
     */
    private $confirmationToken;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    private $passwordRequestedAt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array", length=0, nullable=false)
     */
    private $roles;

    /**
     * @var bool
     *
     * @ORM\Column(name="credentials_expired", type="boolean", nullable=false)
     */
    private $credentialsExpired;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="credentials_expire_at", type="datetime", nullable=true)
     */
    private $credentialsExpireAt;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var string
     *
     * @ORM\Column(name="discr", type="string", length=255, nullable=false)
     */
    private $discr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="student_id", type="string", length=255, nullable=true)
     */
    private $studentId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_student", type="boolean", nullable=true)
     */
    private $isStudent;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_examiner", type="boolean", nullable=true)
     */
    private $isExaminer;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_professor", type="boolean", nullable=true)
     */
    private $isProfessor;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_manager", type="boolean", nullable=true)
     */
    private $isManager;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="simulation_date", type="datetime", nullable=true)
     */
    private $simulationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="eula_date", type="datetime", nullable=true)
     */
    private $eulaDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eula_version", type="string", length=20, nullable=true)
     */
    private $eulaVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="home_university", type="string", length=255, nullable=true)
     */
    private $homeUniversity;

    /**
     * @var string
     *
     * @ORM\Column(name="mails_lang", type="string", length=255, nullable=false)
     */
    private $mailsLang;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_proctor", type="boolean", nullable=true)
     */
    private $isProctor;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_admin", type="boolean", nullable=true)
     */
    private $isAdmin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="roles_id", type="string", length=200, nullable=true)
     */
    private $rolesId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="extra_time", type="boolean", nullable=true)
     */
    private $extraTime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="testwe_date", type="datetime", nullable=true)
     */
    private $testweDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="testwe_version", type="string", length=20, nullable=true)
     */
    private $testweVersion;

    /**
     * @var Schools
     *
     * @ORM\ManyToOne(targetEntity="Schools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     * })
     */
    private $school;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ExamParts", mappedBy="user")
     */
    private $exampart;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Exams", mappedBy="user")
     */
    private $exam;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Exams", mappedBy="userProf")
     */
    private $examProf;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolExamCategories", inversedBy="user")
     * @ORM\JoinTable(name="users_authorized_exam_categories",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolexamcategory_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolexamcategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolClasses", inversedBy="user")
     * @ORM\JoinTable(name="users_classes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolclass_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolclass;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="user")
     * @ORM\JoinTable(name="users_roles",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     *   }
     * )
     */
    private $role;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SchoolSubjects", inversedBy="user")
     * @ORM\JoinTable(name="users_school_subjects",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="schoolsubject_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $schoolsubject;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exampart = new \Doctrine\Common\Collections\ArrayCollection();
        $this->exam = new \Doctrine\Common\Collections\ArrayCollection();
        $this->examProf = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolexamcategory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolclass = new \Doctrine\Common\Collections\ArrayCollection();
        $this->role = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolsubject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical(string $usernameCanonical): self
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    public function setEmailCanonical(string $emailCanonical): self
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getLocked(): ?bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getExpired(): ?bool
    {
        return $this->expired;
    }

    public function setExpired(bool $expired): self
    {
        $this->expired = $expired;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(?\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getCredentialsExpired(): ?bool
    {
        return $this->credentialsExpired;
    }

    public function setCredentialsExpired(bool $credentialsExpired): self
    {
        $this->credentialsExpired = $credentialsExpired;

        return $this;
    }

    public function getCredentialsExpireAt(): ?\DateTimeInterface
    {
        return $this->credentialsExpireAt;
    }

    public function setCredentialsExpireAt(?\DateTimeInterface $credentialsExpireAt): self
    {
        $this->credentialsExpireAt = $credentialsExpireAt;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDiscr(): ?string
    {
        return $this->discr;
    }

    public function setDiscr(string $discr): self
    {
        $this->discr = $discr;

        return $this;
    }

    public function getStudentId(): ?string
    {
        return $this->studentId;
    }

    public function setStudentId(?string $studentId): self
    {
        $this->studentId = $studentId;

        return $this;
    }

    public function getIsStudent(): ?bool
    {
        return $this->isStudent;
    }

    public function setIsStudent(?bool $isStudent): self
    {
        $this->isStudent = $isStudent;

        return $this;
    }

    public function getIsExaminer(): ?bool
    {
        return $this->isExaminer;
    }

    public function setIsExaminer(?bool $isExaminer): self
    {
        $this->isExaminer = $isExaminer;

        return $this;
    }

    public function getIsProfessor(): ?bool
    {
        return $this->isProfessor;
    }

    public function setIsProfessor(?bool $isProfessor): self
    {
        $this->isProfessor = $isProfessor;

        return $this;
    }

    public function getIsManager(): ?bool
    {
        return $this->isManager;
    }

    public function setIsManager(?bool $isManager): self
    {
        $this->isManager = $isManager;

        return $this;
    }

    public function getSimulationDate(): ?\DateTimeInterface
    {
        return $this->simulationDate;
    }

    public function setSimulationDate(?\DateTimeInterface $simulationDate): self
    {
        $this->simulationDate = $simulationDate;

        return $this;
    }

    public function getEulaDate(): ?\DateTimeInterface
    {
        return $this->eulaDate;
    }

    public function setEulaDate(?\DateTimeInterface $eulaDate): self
    {
        $this->eulaDate = $eulaDate;

        return $this;
    }

    public function getEulaVersion(): ?string
    {
        return $this->eulaVersion;
    }

    public function setEulaVersion(?string $eulaVersion): self
    {
        $this->eulaVersion = $eulaVersion;

        return $this;
    }

    public function getHomeUniversity(): ?string
    {
        return $this->homeUniversity;
    }

    public function setHomeUniversity(?string $homeUniversity): self
    {
        $this->homeUniversity = $homeUniversity;

        return $this;
    }

    public function getMailsLang(): ?string
    {
        return $this->mailsLang;
    }

    public function setMailsLang(string $mailsLang): self
    {
        $this->mailsLang = $mailsLang;

        return $this;
    }

    public function getIsProctor(): ?bool
    {
        return $this->isProctor;
    }

    public function setIsProctor(?bool $isProctor): self
    {
        $this->isProctor = $isProctor;

        return $this;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(?bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function getRolesId(): ?string
    {
        return $this->rolesId;
    }

    public function setRolesId(?string $rolesId): self
    {
        $this->rolesId = $rolesId;

        return $this;
    }

    public function getExtraTime(): ?bool
    {
        return $this->extraTime;
    }

    public function setExtraTime(?bool $extraTime): self
    {
        $this->extraTime = $extraTime;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getTestweDate(): ?\DateTimeInterface
    {
        return $this->testweDate;
    }

    public function setTestweDate(?\DateTimeInterface $testweDate): self
    {
        $this->testweDate = $testweDate;

        return $this;
    }

    public function getTestweVersion(): ?string
    {
        return $this->testweVersion;
    }

    public function setTestweVersion(?string $testweVersion): self
    {
        $this->testweVersion = $testweVersion;

        return $this;
    }

    public function getSchool(): ?Schools
    {
        return $this->school;
    }

    public function setSchool(?Schools $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|ExamParts[]
     */
    public function getExampart(): Collection
    {
        return $this->exampart;
    }

    public function addExampart(ExamParts $exampart): self
    {
        if (!$this->exampart->contains($exampart)) {
            $this->exampart[] = $exampart;
            $exampart->addUser($this);
        }

        return $this;
    }

    public function removeExampart(ExamParts $exampart): self
    {
        if ($this->exampart->contains($exampart)) {
            $this->exampart->removeElement($exampart);
            $exampart->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Exams[]
     */
    public function getExam(): Collection
    {
        return $this->exam;
    }

    public function addExam(Exams $exam): self
    {
        if (!$this->exam->contains($exam)) {
            $this->exam[] = $exam;
            $exam->addUser($this);
        }

        return $this;
    }

    public function removeExam(Exams $exam): self
    {
        if ($this->exam->contains($exam)) {
            $this->exam->removeElement($exam);
            $exam->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Exams[]
     */
    public function getExamProf(): Collection
    {
        return $this->examProf;
    }

    public function addExamProf(Exams $examProf): self
    {
        if (!$this->examProf->contains($examProf)) {
            $this->examProf[] = $examProf;
            $examProf->addUserProf($this);
        }

        return $this;
    }

    public function removeExamProf(Exams $examProf): self
    {
        if ($this->examProf->contains($examProf)) {
            $this->examProf->removeElement($examProf);
            $examProf->removeUserProf($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolExamCategories[]
     */
    public function getSchoolexamcategory(): Collection
    {
        return $this->schoolexamcategory;
    }

    public function addSchoolexamcategory(SchoolExamCategories $schoolexamcategory): self
    {
        if (!$this->schoolexamcategory->contains($schoolexamcategory)) {
            $this->schoolexamcategory[] = $schoolexamcategory;
        }

        return $this;
    }

    public function removeSchoolexamcategory(SchoolExamCategories $schoolexamcategory): self
    {
        if ($this->schoolexamcategory->contains($schoolexamcategory)) {
            $this->schoolexamcategory->removeElement($schoolexamcategory);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolClasses[]
     */
    public function getSchoolclass(): Collection
    {
        return $this->schoolclass;
    }

    public function addSchoolclass(SchoolClasses $schoolclass): self
    {
        if (!$this->schoolclass->contains($schoolclass)) {
            $this->schoolclass[] = $schoolclass;
        }

        return $this;
    }

    public function removeSchoolclass(SchoolClasses $schoolclass): self
    {
        if ($this->schoolclass->contains($schoolclass)) {
            $this->schoolclass->removeElement($schoolclass);
        }

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getRole(): Collection
    {
        return $this->role;
    }

    public function addRole(Role $role): self
    {
        if (!$this->role->contains($role)) {
            $this->role[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->role->contains($role)) {
            $this->role->removeElement($role);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolSubjects[]
     */
    public function getSchoolsubject(): Collection
    {
        return $this->schoolsubject;
    }

    public function addSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if (!$this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject[] = $schoolsubject;
        }

        return $this;
    }

    public function removeSchoolsubject(SchoolSubjects $schoolsubject): self
    {
        if ($this->schoolsubject->contains($schoolsubject)) {
            $this->schoolsubject->removeElement($schoolsubject);
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
