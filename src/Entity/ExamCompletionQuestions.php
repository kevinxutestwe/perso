<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamCompletionQuestions
 *
 * @ORM\Table(name="exam_completion_questions", indexes={@ORM\Index(name="IDX_277AEB43C7995787", columns={"completion_id"}), @ORM\Index(name="IDX_277AEB431E27F6BF", columns={"question_id"}), @ORM\Index(name="IDX_277AEB434AF38FD1", columns={"deleted_at"})})
 * @ORM\Entity
 */
class ExamCompletionQuestions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer_text", type="text", length=0, nullable=true)
     */
    private $answerText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var ExamQuestions
     *
     * @ORM\ManyToOne(targetEntity="ExamQuestions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $question;

    /**
     * @var ExamPartCompletions
     *
     * @ORM\ManyToOne(targetEntity="ExamPartCompletions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="completion_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $completion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswerText(): ?string
    {
        return $this->answerText;
    }

    public function setAnswerText(?string $answerText): self
    {
        $this->answerText = $answerText;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getQuestion(): ?ExamQuestions
    {
        return $this->question;
    }

    public function setQuestion(?ExamQuestions $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getCompletion(): ?ExamPartCompletions
    {
        return $this->completion;
    }

    public function setCompletion(?ExamPartCompletions $completion): self
    {
        $this->completion = $completion;

        return $this;
    }


}
