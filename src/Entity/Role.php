<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="role", indexes={@ORM\Index(name="IDX_57698A6A6A8ABCDE", columns={"parent_role"}), @ORM\Index(name="IDX_57698A6A5E237E06", columns={"name"})})
 * @ORM\Entity
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="id_by_school", type="integer", nullable=false, options={"comment"="If value = 0, all schools can access this role"})
     */
    private $idBySchool;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_student", type="boolean", nullable=false)
     */
    private $isStudent;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_set_grading_deadline", type="boolean", nullable=false)
     */
    private $examSetGradingDeadline;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_set_exam_deadline", type="boolean", nullable=false)
     */
    private $examSetExamDeadline;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_modify_exam_postvalidation", type="boolean", nullable=false)
     */
    private $examModifyExamPostvalidation;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_user", type="boolean", nullable=false)
     */
    private $platformCreateUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_user", type="boolean", nullable=false)
     */
    private $platformModifyUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_user", type="boolean", nullable=false)
     */
    private $platformDeleteUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_disable_user", type="boolean", nullable=false)
     */
    private $platformDisableUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_enable_user", type="boolean", nullable=false)
     */
    private $platformEnableUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_import_user", type="boolean", nullable=false)
     */
    private $platformImportUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_class", type="boolean", nullable=false)
     */
    private $platformCreateClass;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_class", type="boolean", nullable=false)
     */
    private $platformModifyClass;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_class", type="boolean", nullable=false)
     */
    private $platformDeleteClass;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_schoolyear", type="boolean", nullable=false)
     */
    private $platformCreateSchoolyear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_schoolyear", type="boolean", nullable=false)
     */
    private $platformModifySchoolyear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_schoolyear", type="boolean", nullable=false)
     */
    private $platformDeleteSchoolyear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_transfer_next_year", type="boolean", nullable=false)
     */
    private $platformTransferNextYear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_education_year", type="boolean", nullable=false)
     */
    private $platformCreateEducationYear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_education_year", type="boolean", nullable=false)
     */
    private $platformModifyEducationYear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_education_year", type="boolean", nullable=false)
     */
    private $platformDeleteEducationYear;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_school_subject", type="boolean", nullable=false)
     */
    private $platformCreateSchoolSubject;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_school_subject", type="boolean", nullable=false)
     */
    private $platformModifySchoolSubject;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_school_subject", type="boolean", nullable=false)
     */
    private $platformDeleteSchoolSubject;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_create_exam_category", type="boolean", nullable=false)
     */
    private $platformCreateExamCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_modify_exam_category", type="boolean", nullable=false)
     */
    private $platformModifyExamCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="platform_delete_exam_category", type="boolean", nullable=false)
     */
    private $platformDeleteExamCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_create_exam", type="boolean", nullable=false)
     */
    private $examCreateExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_name", type="boolean", nullable=false)
     */
    private $examManageExamName;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_description", type="boolean", nullable=false)
     */
    private $examManageExamDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_subject", type="boolean", nullable=false)
     */
    private $examManageExamSubject;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_planned_date", type="boolean", nullable=false)
     */
    private $examManageExamPlannedDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_end_date", type="boolean", nullable=false)
     */
    private $examManageExamEndDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_hour", type="boolean", nullable=false)
     */
    private $examManageExamHour;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_duration", type="boolean", nullable=false)
     */
    private $examManageExamDuration;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_professors", type="boolean", nullable=false)
     */
    private $examManageExamProfessors;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_examiners", type="boolean", nullable=false)
     */
    private $examManageExamExaminers;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_classes", type="boolean", nullable=false)
     */
    private $examManageExamClasses;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_medium", type="boolean", nullable=false)
     */
    private $examManageExamMedium;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_security", type="boolean", nullable=false)
     */
    private $examManageExamSecurity;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_category", type="boolean", nullable=false)
     */
    private $examManageExamCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_manage_exam_questions", type="boolean", nullable=false)
     */
    private $examManageExamQuestions;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_validate_exam", type="boolean", nullable=false)
     */
    private $examValidateExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_delete_exam", type="boolean", nullable=false)
     */
    private $examDeleteExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_send_reminder", type="boolean", nullable=false)
     */
    private $examSendReminder;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_copy_exam", type="boolean", nullable=false)
     */
    private $examCopyExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_access_not_corrected_exams", type="boolean", nullable=false)
     */
    private $examAccessNotCorrectedExams;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_correct_exam", type="boolean", nullable=false)
     */
    private $examCorrectExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_modify_exam_grades", type="boolean", nullable=false)
     */
    private $examModifyExamGrades;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_modify_exam_scale", type="boolean", nullable=false)
     */
    private $examModifyExamScale;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_print_exam", type="boolean", nullable=false)
     */
    private $examPrintExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="exam_proctor_exam", type="boolean", nullable=false)
     */
    private $examProctorExam;

    /**
     * @var bool
     *
     * @ORM\Column(name="user_can_see_exam_password", type="boolean", nullable=false)
     */
    private $userCanSeeExamPassword;

    /**
     * @var bool
     *
     * @ORM\Column(name="user_can_see_exam", type="boolean", nullable=false)
     */
    private $userCanSeeExam;

    /**
     * @var RoleCollection
     *
     * @ORM\ManyToOne(targetEntity="RoleCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_role", referencedColumnName="id")
     * })
     */
    private $parentRole;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", mappedBy="role")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdBySchool(): ?int
    {
        return $this->idBySchool;
    }

    public function setIdBySchool(int $idBySchool): self
    {
        $this->idBySchool = $idBySchool;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getIsStudent(): ?bool
    {
        return $this->isStudent;
    }

    public function setIsStudent(bool $isStudent): self
    {
        $this->isStudent = $isStudent;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getExamSetGradingDeadline(): ?bool
    {
        return $this->examSetGradingDeadline;
    }

    public function setExamSetGradingDeadline(bool $examSetGradingDeadline): self
    {
        $this->examSetGradingDeadline = $examSetGradingDeadline;

        return $this;
    }

    public function getExamSetExamDeadline(): ?bool
    {
        return $this->examSetExamDeadline;
    }

    public function setExamSetExamDeadline(bool $examSetExamDeadline): self
    {
        $this->examSetExamDeadline = $examSetExamDeadline;

        return $this;
    }

    public function getExamModifyExamPostvalidation(): ?bool
    {
        return $this->examModifyExamPostvalidation;
    }

    public function setExamModifyExamPostvalidation(bool $examModifyExamPostvalidation): self
    {
        $this->examModifyExamPostvalidation = $examModifyExamPostvalidation;

        return $this;
    }

    public function getPlatformCreateUser(): ?bool
    {
        return $this->platformCreateUser;
    }

    public function setPlatformCreateUser(bool $platformCreateUser): self
    {
        $this->platformCreateUser = $platformCreateUser;

        return $this;
    }

    public function getPlatformModifyUser(): ?bool
    {
        return $this->platformModifyUser;
    }

    public function setPlatformModifyUser(bool $platformModifyUser): self
    {
        $this->platformModifyUser = $platformModifyUser;

        return $this;
    }

    public function getPlatformDeleteUser(): ?bool
    {
        return $this->platformDeleteUser;
    }

    public function setPlatformDeleteUser(bool $platformDeleteUser): self
    {
        $this->platformDeleteUser = $platformDeleteUser;

        return $this;
    }

    public function getPlatformDisableUser(): ?bool
    {
        return $this->platformDisableUser;
    }

    public function setPlatformDisableUser(bool $platformDisableUser): self
    {
        $this->platformDisableUser = $platformDisableUser;

        return $this;
    }

    public function getPlatformEnableUser(): ?bool
    {
        return $this->platformEnableUser;
    }

    public function setPlatformEnableUser(bool $platformEnableUser): self
    {
        $this->platformEnableUser = $platformEnableUser;

        return $this;
    }

    public function getPlatformImportUser(): ?bool
    {
        return $this->platformImportUser;
    }

    public function setPlatformImportUser(bool $platformImportUser): self
    {
        $this->platformImportUser = $platformImportUser;

        return $this;
    }

    public function getPlatformCreateClass(): ?bool
    {
        return $this->platformCreateClass;
    }

    public function setPlatformCreateClass(bool $platformCreateClass): self
    {
        $this->platformCreateClass = $platformCreateClass;

        return $this;
    }

    public function getPlatformModifyClass(): ?bool
    {
        return $this->platformModifyClass;
    }

    public function setPlatformModifyClass(bool $platformModifyClass): self
    {
        $this->platformModifyClass = $platformModifyClass;

        return $this;
    }

    public function getPlatformDeleteClass(): ?bool
    {
        return $this->platformDeleteClass;
    }

    public function setPlatformDeleteClass(bool $platformDeleteClass): self
    {
        $this->platformDeleteClass = $platformDeleteClass;

        return $this;
    }

    public function getPlatformCreateSchoolyear(): ?bool
    {
        return $this->platformCreateSchoolyear;
    }

    public function setPlatformCreateSchoolyear(bool $platformCreateSchoolyear): self
    {
        $this->platformCreateSchoolyear = $platformCreateSchoolyear;

        return $this;
    }

    public function getPlatformModifySchoolyear(): ?bool
    {
        return $this->platformModifySchoolyear;
    }

    public function setPlatformModifySchoolyear(bool $platformModifySchoolyear): self
    {
        $this->platformModifySchoolyear = $platformModifySchoolyear;

        return $this;
    }

    public function getPlatformDeleteSchoolyear(): ?bool
    {
        return $this->platformDeleteSchoolyear;
    }

    public function setPlatformDeleteSchoolyear(bool $platformDeleteSchoolyear): self
    {
        $this->platformDeleteSchoolyear = $platformDeleteSchoolyear;

        return $this;
    }

    public function getPlatformTransferNextYear(): ?bool
    {
        return $this->platformTransferNextYear;
    }

    public function setPlatformTransferNextYear(bool $platformTransferNextYear): self
    {
        $this->platformTransferNextYear = $platformTransferNextYear;

        return $this;
    }

    public function getPlatformCreateEducationYear(): ?bool
    {
        return $this->platformCreateEducationYear;
    }

    public function setPlatformCreateEducationYear(bool $platformCreateEducationYear): self
    {
        $this->platformCreateEducationYear = $platformCreateEducationYear;

        return $this;
    }

    public function getPlatformModifyEducationYear(): ?bool
    {
        return $this->platformModifyEducationYear;
    }

    public function setPlatformModifyEducationYear(bool $platformModifyEducationYear): self
    {
        $this->platformModifyEducationYear = $platformModifyEducationYear;

        return $this;
    }

    public function getPlatformDeleteEducationYear(): ?bool
    {
        return $this->platformDeleteEducationYear;
    }

    public function setPlatformDeleteEducationYear(bool $platformDeleteEducationYear): self
    {
        $this->platformDeleteEducationYear = $platformDeleteEducationYear;

        return $this;
    }

    public function getPlatformCreateSchoolSubject(): ?bool
    {
        return $this->platformCreateSchoolSubject;
    }

    public function setPlatformCreateSchoolSubject(bool $platformCreateSchoolSubject): self
    {
        $this->platformCreateSchoolSubject = $platformCreateSchoolSubject;

        return $this;
    }

    public function getPlatformModifySchoolSubject(): ?bool
    {
        return $this->platformModifySchoolSubject;
    }

    public function setPlatformModifySchoolSubject(bool $platformModifySchoolSubject): self
    {
        $this->platformModifySchoolSubject = $platformModifySchoolSubject;

        return $this;
    }

    public function getPlatformDeleteSchoolSubject(): ?bool
    {
        return $this->platformDeleteSchoolSubject;
    }

    public function setPlatformDeleteSchoolSubject(bool $platformDeleteSchoolSubject): self
    {
        $this->platformDeleteSchoolSubject = $platformDeleteSchoolSubject;

        return $this;
    }

    public function getPlatformCreateExamCategory(): ?bool
    {
        return $this->platformCreateExamCategory;
    }

    public function setPlatformCreateExamCategory(bool $platformCreateExamCategory): self
    {
        $this->platformCreateExamCategory = $platformCreateExamCategory;

        return $this;
    }

    public function getPlatformModifyExamCategory(): ?bool
    {
        return $this->platformModifyExamCategory;
    }

    public function setPlatformModifyExamCategory(bool $platformModifyExamCategory): self
    {
        $this->platformModifyExamCategory = $platformModifyExamCategory;

        return $this;
    }

    public function getPlatformDeleteExamCategory(): ?bool
    {
        return $this->platformDeleteExamCategory;
    }

    public function setPlatformDeleteExamCategory(bool $platformDeleteExamCategory): self
    {
        $this->platformDeleteExamCategory = $platformDeleteExamCategory;

        return $this;
    }

    public function getExamCreateExam(): ?bool
    {
        return $this->examCreateExam;
    }

    public function setExamCreateExam(bool $examCreateExam): self
    {
        $this->examCreateExam = $examCreateExam;

        return $this;
    }

    public function getExamManageExamName(): ?bool
    {
        return $this->examManageExamName;
    }

    public function setExamManageExamName(bool $examManageExamName): self
    {
        $this->examManageExamName = $examManageExamName;

        return $this;
    }

    public function getExamManageExamDescription(): ?bool
    {
        return $this->examManageExamDescription;
    }

    public function setExamManageExamDescription(bool $examManageExamDescription): self
    {
        $this->examManageExamDescription = $examManageExamDescription;

        return $this;
    }

    public function getExamManageExamSubject(): ?bool
    {
        return $this->examManageExamSubject;
    }

    public function setExamManageExamSubject(bool $examManageExamSubject): self
    {
        $this->examManageExamSubject = $examManageExamSubject;

        return $this;
    }

    public function getExamManageExamPlannedDate(): ?bool
    {
        return $this->examManageExamPlannedDate;
    }

    public function setExamManageExamPlannedDate(bool $examManageExamPlannedDate): self
    {
        $this->examManageExamPlannedDate = $examManageExamPlannedDate;

        return $this;
    }

    public function getExamManageExamEndDate(): ?bool
    {
        return $this->examManageExamEndDate;
    }

    public function setExamManageExamEndDate(bool $examManageExamEndDate): self
    {
        $this->examManageExamEndDate = $examManageExamEndDate;

        return $this;
    }

    public function getExamManageExamHour(): ?bool
    {
        return $this->examManageExamHour;
    }

    public function setExamManageExamHour(bool $examManageExamHour): self
    {
        $this->examManageExamHour = $examManageExamHour;

        return $this;
    }

    public function getExamManageExamDuration(): ?bool
    {
        return $this->examManageExamDuration;
    }

    public function setExamManageExamDuration(bool $examManageExamDuration): self
    {
        $this->examManageExamDuration = $examManageExamDuration;

        return $this;
    }

    public function getExamManageExamProfessors(): ?bool
    {
        return $this->examManageExamProfessors;
    }

    public function setExamManageExamProfessors(bool $examManageExamProfessors): self
    {
        $this->examManageExamProfessors = $examManageExamProfessors;

        return $this;
    }

    public function getExamManageExamExaminers(): ?bool
    {
        return $this->examManageExamExaminers;
    }

    public function setExamManageExamExaminers(bool $examManageExamExaminers): self
    {
        $this->examManageExamExaminers = $examManageExamExaminers;

        return $this;
    }

    public function getExamManageExamClasses(): ?bool
    {
        return $this->examManageExamClasses;
    }

    public function setExamManageExamClasses(bool $examManageExamClasses): self
    {
        $this->examManageExamClasses = $examManageExamClasses;

        return $this;
    }

    public function getExamManageExamMedium(): ?bool
    {
        return $this->examManageExamMedium;
    }

    public function setExamManageExamMedium(bool $examManageExamMedium): self
    {
        $this->examManageExamMedium = $examManageExamMedium;

        return $this;
    }

    public function getExamManageExamSecurity(): ?bool
    {
        return $this->examManageExamSecurity;
    }

    public function setExamManageExamSecurity(bool $examManageExamSecurity): self
    {
        $this->examManageExamSecurity = $examManageExamSecurity;

        return $this;
    }

    public function getExamManageExamCategory(): ?bool
    {
        return $this->examManageExamCategory;
    }

    public function setExamManageExamCategory(bool $examManageExamCategory): self
    {
        $this->examManageExamCategory = $examManageExamCategory;

        return $this;
    }

    public function getExamManageExamQuestions(): ?bool
    {
        return $this->examManageExamQuestions;
    }

    public function setExamManageExamQuestions(bool $examManageExamQuestions): self
    {
        $this->examManageExamQuestions = $examManageExamQuestions;

        return $this;
    }

    public function getExamValidateExam(): ?bool
    {
        return $this->examValidateExam;
    }

    public function setExamValidateExam(bool $examValidateExam): self
    {
        $this->examValidateExam = $examValidateExam;

        return $this;
    }

    public function getExamDeleteExam(): ?bool
    {
        return $this->examDeleteExam;
    }

    public function setExamDeleteExam(bool $examDeleteExam): self
    {
        $this->examDeleteExam = $examDeleteExam;

        return $this;
    }

    public function getExamSendReminder(): ?bool
    {
        return $this->examSendReminder;
    }

    public function setExamSendReminder(bool $examSendReminder): self
    {
        $this->examSendReminder = $examSendReminder;

        return $this;
    }

    public function getExamCopyExam(): ?bool
    {
        return $this->examCopyExam;
    }

    public function setExamCopyExam(bool $examCopyExam): self
    {
        $this->examCopyExam = $examCopyExam;

        return $this;
    }

    public function getExamAccessNotCorrectedExams(): ?bool
    {
        return $this->examAccessNotCorrectedExams;
    }

    public function setExamAccessNotCorrectedExams(bool $examAccessNotCorrectedExams): self
    {
        $this->examAccessNotCorrectedExams = $examAccessNotCorrectedExams;

        return $this;
    }

    public function getExamCorrectExam(): ?bool
    {
        return $this->examCorrectExam;
    }

    public function setExamCorrectExam(bool $examCorrectExam): self
    {
        $this->examCorrectExam = $examCorrectExam;

        return $this;
    }

    public function getExamModifyExamGrades(): ?bool
    {
        return $this->examModifyExamGrades;
    }

    public function setExamModifyExamGrades(bool $examModifyExamGrades): self
    {
        $this->examModifyExamGrades = $examModifyExamGrades;

        return $this;
    }

    public function getExamModifyExamScale(): ?bool
    {
        return $this->examModifyExamScale;
    }

    public function setExamModifyExamScale(bool $examModifyExamScale): self
    {
        $this->examModifyExamScale = $examModifyExamScale;

        return $this;
    }

    public function getExamPrintExam(): ?bool
    {
        return $this->examPrintExam;
    }

    public function setExamPrintExam(bool $examPrintExam): self
    {
        $this->examPrintExam = $examPrintExam;

        return $this;
    }

    public function getExamProctorExam(): ?bool
    {
        return $this->examProctorExam;
    }

    public function setExamProctorExam(bool $examProctorExam): self
    {
        $this->examProctorExam = $examProctorExam;

        return $this;
    }

    public function getUserCanSeeExamPassword(): ?bool
    {
        return $this->userCanSeeExamPassword;
    }

    public function setUserCanSeeExamPassword(bool $userCanSeeExamPassword): self
    {
        $this->userCanSeeExamPassword = $userCanSeeExamPassword;

        return $this;
    }

    public function getUserCanSeeExam(): ?bool
    {
        return $this->userCanSeeExam;
    }

    public function setUserCanSeeExam(bool $userCanSeeExam): self
    {
        $this->userCanSeeExam = $userCanSeeExam;

        return $this;
    }

    public function getParentRole(): ?RoleCollection
    {
        return $this->parentRole;
    }

    public function setParentRole(?RoleCollection $parentRole): self
    {
        $this->parentRole = $parentRole;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addRole($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeRole($this);
        }

        return $this;
    }

}
