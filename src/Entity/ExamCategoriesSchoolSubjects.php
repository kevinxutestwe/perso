<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamCategoriesSchoolSubjects
 *
 * @ORM\Table(name="exam_categories_school_subjects", indexes={@ORM\Index(name="IDX_96E1BAE295530E3", columns={"exam_category_id"}), @ORM\Index(name="IDX_96E1BAE2B79F5C75", columns={"school_subject_id"})})
 * @ORM\Entity
 */
class ExamCategoriesSchoolSubjects
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficient", type="float", precision=10, scale=0, nullable=false)
     */
    private $coefficient;

    /**
     * @var SchoolExamCategories
     *
     * @ORM\ManyToOne(targetEntity="SchoolExamCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_category_id", referencedColumnName="id")
     * })
     */
    private $examCategory;

    /**
     * @var SchoolSubjects
     *
     * @ORM\ManyToOne(targetEntity="SchoolSubjects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_subject_id", referencedColumnName="id")
     * })
     */
    private $schoolSubject;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoefficient(): ?float
    {
        return $this->coefficient;
    }

    public function setCoefficient(float $coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getExamCategory(): ?SchoolExamCategories
    {
        return $this->examCategory;
    }

    public function setExamCategory(?SchoolExamCategories $examCategory): self
    {
        $this->examCategory = $examCategory;

        return $this;
    }

    public function getSchoolSubject(): ?SchoolSubjects
    {
        return $this->schoolSubject;
    }

    public function setSchoolSubject(?SchoolSubjects $schoolSubject): self
    {
        $this->schoolSubject = $schoolSubject;

        return $this;
    }


}
