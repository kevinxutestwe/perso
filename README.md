# Application Symfony 4 Pour Elasticsearch

### Requirements
+ Java 8
 
#### Windows
+ Elasticsearch server - Lien d'installation : https://www.elastic.co/fr/start?elektra=home&storm=banner

#### macOS
+ Dans un terminal : brew install elasticsearch
+ Puis pour lancer le serveur Elasticsearch : elasticsearch

### Guide d'installation

#### Symfony
+ Lancer la commande suivante dans un terminal à la source de l'application : composer install
+ Démarrez le serveur symfony : php bin/console server:run

#### Base de données
+ Configuer l'accès à la base dans le fichier .env situé à la source 
+ Ligne 23 : DATABASE_URL=mysql://[nom d'utilisateur]:[mot de passe]@127.0.0.1:3306/[nom de la BD]

#### Elasticsearch
+ Indexation des données à l'aide des commandes suivantes : 
	php -d memory_limit=-1 bin/console indexation:schools_info
et
	php -d memory_limit=-1 bin/console indexation:exams_info

+ 

#### Pour visualiser les données 
+ Pour utiliser Elasticsearch, il existe le plugin suivant : 
Elasticsearch Head : https://chrome.google.com/webstore/detail/elasticsearch-head/ffmkiejjmecolpfloofpjologoblkegm
+ Kibana : https://www.elastic.co/fr/downloads/kibana

##### Mapping
+ Dans src/Service/MappingInfos.php

### Listes des routes disponibles pour le moment 
+ Dans NotesPerso/Liste_Stats.md

### Auteur
+ XU Kevin - Développeur chez TestWe